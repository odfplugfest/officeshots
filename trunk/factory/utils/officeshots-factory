#! /bin/sh
### BEGIN INIT INFO
# Provides:          officeshots-factory
# Required-Start:    $local_fs $remote_fs oooserver
# Required-Stop:     $local_fs $remote_fs oooserver
# Default-Start:     2 3 4 5
# Default-Stop:      S 0 1 6
# X-Interactive:     true
# Short-Description: Initscript to start the Officeshots Factory client
# Description:       This file should be used to construct scripts to be
#                    placed in /etc/init.d.
### END INIT INFO

# Author: Sander Marechal

# PATH should only include /usr/* if it runs after the mountnfs.sh script
PATH=/usr/sbin:/usr/bin:/sbin:/bin
DESC="Officeshots factory"
NAME=factory.py
PYTHON=/usr/bin/python
DAEMON="/path/to/factory.py"
DAEMON_ARGS="-c /path/to/config.ini"
PIDFILE="/var/run/officeshots.pid"
SCRIPTNAME=/etc/init.d/officeshots-factory
USER=nobody

# Exit if the package is not installed
[ -x "$DAEMON" ] || exit 0

# Read configuration variable file if it is present
[ -r /etc/default/$NAME ] && . /etc/default/$NAME

# Load the VERBOSE setting and other rcS variables
[ -f /etc/default/rcS ] && . /etc/default/rcS

# Define LSB log_* functions.
# Depend on lsb-base (>= 3.0-6) to ensure that this file is present.
. /lib/lsb/init-functions

#
# Function that starts the daemon/service
#
do_start()
{
       # Return
       #   0 if daemon has been started
       #   1 if daemon was already running
       #   2 if daemon could not be started
       start-stop-daemon --start --pidfile $PIDFILE --chuid $USER --exec $PYTHON -- $DAEMON $DAEMON_ARGS start
}

#
# Function that stops the daemon/service
#
do_stop()
{
       # Return
       #   0 if daemon has been stopped
       #   1 if daemon was already stopped
       #   2 if daemon could not be stopped
       #   other if a failure occurred
       start-stop-daemon --stop --quiet --retry=TERM/30/KILL/5 --pidfile $PIDFILE --exec $PYTHON
       RETVAL="$?"
       [ "$RETVAL" = 2 ] && return 2
       # Wait for children to finish too if this is a daemon that forks
       # and if the daemon is only ever run from this initscript.
       # If the above conditions are not satisfied then add some other code
       # that waits for the process to drop all resources that could be
       # needed by services started subsequently.  A last resort is to
       # sleep for some time.
       start-stop-daemon --stop --quiet --oknodo --retry=0/30/KILL/5 --pidfile $PIDFILE --exec $PYTHON
       [ "$?" = 2 ] && return 2
       return "$RETVAL"
}

case "$1" in
 start)
       [ "$VERBOSE" != no ] && log_daemon_msg "Starting $DESC" "$NAME"
       do_start
       case "$?" in
               0|1) [ "$VERBOSE" != no ] && log_end_msg 0 ;;
               2) [ "$VERBOSE" != no ] && log_end_msg 1 ;;
       esac
       ;;
 stop)
       [ "$VERBOSE" != no ] && log_daemon_msg "Stopping $DESC" "$NAME"
       do_stop
       case "$?" in
               0|1) [ "$VERBOSE" != no ] && log_end_msg 0 ;;
               2) [ "$VERBOSE" != no ] && log_end_msg 1 ;;
       esac
       ;;
 restart|force-reload)
       log_daemon_msg "Restarting $DESC" "$NAME"
       do_stop
       case "$?" in
         0|1)
               do_start
               case "$?" in
                       0) log_end_msg 0 ;;
                       1) log_end_msg 1 ;; # Old process is still running
                       *) log_end_msg 1 ;; # Failed to start
               esac
               ;;
         *)
               # Failed to stop
               log_end_msg 1
               ;;
       esac
       ;;
  status)
        status_of_proc -p $PIDFILE $DAEMON $NAME && exit 0 || exit $?
        ;;
 *)
       echo "Usage: $SCRIPTNAME {start|stop|restart|force-reload|status}" >&2
       exit 3
       ;;
esac

