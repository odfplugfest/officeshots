# Officeshots.org - Test your office documents in different applications
# Copyright (C) 2009 Stichting Lone Wolves
# Written by Sander Marechal <s.marechal@jejik.com>

"""
This module is a back-end for generic commandline conversion such as
supported by AbiWord.
"""

import os
import logging
import platform
import subprocess

from pprint import pprint
from backends import Backend, BackendException

class CLIException(BackendException):
	def __str__(self):
		return 'CLIException: ' + BackendException.__str__(self)
	

class CLI(Backend):
	"""
	Backend for the headless OpenOffice.org server
	"""
	def initialize(self):
		self.command = self.config.get(self.section, 'command')
		self.result  = self.config.get(self.section, 'result')
		self.default_format = self.config.get(self.section, 'default_format')
	
	def execute(self, command):
		print("execute()")
		try:
			print("execute1:", command)
		        if platform.system() != 'Windows' and len(command)>0:
                                f = command[0]
                                if f.startswith('"') and f.endswith('"'):
                                        f = f[1:-1]
                                        command[0] = f
			print("execute2:", command)
			p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
			stdout, stderr = p.communicate()
		except OSError as ex:
			raise CLIException(str(ex))

		return (p.returncode, stdout, stderr)

	def process(self, job):
		print("process()")
		if job['format'] == '':
			job['format'] = self.default_format

		self.job = job
		self.source_path = self.save_document(job)

		command  = self.parse(self.command)
		result   = self.parse(self.result)
		print("command:", command)
                
		if platform.system() == 'Windows':
			# Popen needs a string on Windows
			process  = self.execute(command)
		else:
			process  = self.execute(command.split())

		if process[0] > 0:
			raise CLIException('The command `%s` returned with a non-zero exit status (%s, %s).' % (command, process[1], process[2]), True)

		logging.info('CLI converted %s to %s' % (self.source_path, result))
		contents = self.load_document(result)
		os.unlink(self.source_path)
		os.unlink(result)

                logging.info('CLI result size %d' % len(contents))
		return (job['format'], contents)

	def parse(self, string):
		source_dir, source_file = os.path.split(self.source_path)
		source_base, source_ext = os.path.splitext(source_file)
		source_ext = source_ext[1:]

		dest_ext = self.job['format']
		if dest_ext == 'odf':
			dest_ext = self.job['doctype']
		
		tmp_dir = os.path.normpath(self.config.get('global', 'tmp_files'))

		string = string.replace('%source_path', self.source_path)
		string = string.replace('%source_dir',  source_dir)
		string = string.replace('%source_file', source_file)
		string = string.replace('%source_base', source_base)
		string = string.replace('%source_ext',  source_ext)

		string = string.replace('%jobid',    self.job['job'])
		string = string.replace('%format',   self.job['format'])
		string = string.replace('%dest_ext', dest_ext)
		string = string.replace('%temp_dir', tmp_dir)

		return string
		
