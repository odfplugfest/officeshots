# Officeshots.org - Test your office documents in different applications
# Copyright (C) 2009 Stichting Lone Wolves
# Written by Sander Marechal <s.marechal@jejik.com>
#
# Heavily based on PyODConvert, which is
# Copyright (C) 2007 Mirko Nasato <mirko@artofsolving.com>
# Licensed under the GNU LGPL v2.1 - http://www.gnu.org/licenses/lgpl.html

"""
This module is a back-end for the headless OpenOffice.org server
"""

import os
import uno
import logging

from backends import Backend, BackendException
from com.sun.star.io import IOException
from com.sun.star.uno import RuntimeException
from com.sun.star.lang import IllegalArgumentException
from com.sun.star.beans import PropertyValue
from com.sun.star.connection import NoConnectException
from com.sun.star.document.UpdateDocMode import QUIET_UPDATE

FAMILY_MASTER = "Master"
FAMILY_PRESENTATION = "Presentation"
FAMILY_SPREADSHEET = "Spreadsheet"
FAMILY_TEXT = "Text"

FAMILY_BY_DOCTYPE = {
	"odt": FAMILY_TEXT,
	"sxw": FAMILY_TEXT,
	"doc": FAMILY_TEXT,
	"rtf": FAMILY_TEXT,
	"txt": FAMILY_TEXT,
	"wpd": FAMILY_TEXT,
	"html": FAMILY_TEXT,
	"ods": FAMILY_SPREADSHEET,
	"sxc": FAMILY_SPREADSHEET,
	"xls": FAMILY_SPREADSHEET,
	"odp": FAMILY_PRESENTATION,
	"sxi": FAMILY_PRESENTATION,
	"ppt": FAMILY_PRESENTATION
}

FILTER_BY_FORMAT = {
	"pdf": {
		FAMILY_TEXT: "writer_pdf_Export",
		FAMILY_SPREADSHEET: "calc_pdf_Export",
		FAMILY_PRESENTATION: "impress_pdf_Export"
	},
	"odf": {
		FAMILY_TEXT: "writer8",
		FAMILY_SPREADSHEET: "calc8",
		FAMILY_PRESENTATION: "impress8"
	}
}

def _unoProps(**args):
	props = []
	for key in args:
		prop = PropertyValue()
		prop.Name = key
		prop.Value = args[key]
		props.append(prop)
	return tuple(props)


class OOOServerException(BackendException):
	def __str__(self):
		return 'OOOServerException: ' + BackendException.__str__(self)
	

class OOOServer(Backend):
	"""
	Backend for the headless OpenOffice.org server
	"""
	def initialize(self):
		localContext = uno.getComponentContext()
		resolver = localContext.ServiceManager.createInstanceWithContext('com.sun.star.bridge.UnoUrlResolver', localContext)

		if self.config.has_option(self.section, 'ooo_host') and self.config.has_option(self.section, 'ooo_port'):
			self.ooo_host = self.config.get(self.section, 'ooo_host')
			self.ooo_port = self.config.get(self.section, 'ooo_port')
			try:
				context = resolver.resolve('uno:socket,host=%s,port=%s;urp;StarOffice.ComponentContext' % (self.ooo_host, self.ooo_port))
			except NoConnectException:
				raise OOOServerException('Failed to connect to OpenOffice.org on %s port %s' % (self.ooo_host, self.ooo_port))
		elif self.config.has_option(self.section, 'ooo_pipe'):
			self.ooo_pipe = self.config.get(self.section, 'ooo_pipe')
			try:
				context = resolver.resolve('uno:pipe,name=%s;urp;StarOffice.ComponentContext' % self.ooo_pipe)
			except NoConnectException:
				raise OOOServerException('Failed to connect to OpenOffice.org on pipe "%s"' % self.ooo_pipe)
		else:
			raise OOOServerException('Failed to connect to OpenOffice.org. No connection method configured.')
				
		self.desktop = context.ServiceManager.createInstanceWithContext('com.sun.star.frame.Desktop', context)
	
	def filter_name(self, doctype, format):
		try:
			family = FAMILY_BY_DOCTYPE[doctype]
		except KeyError:
			raise OOOServerException("Unknown input doctype: '%s'" % doctype, True)
		try:
			filterByFamily = FILTER_BY_FORMAT[format]
		except KeyError:
			raise OOOServerException("Unknown output format: '%s'" % format, True)
		try:
			return filterByFamily[family]
		except KeyError:
			raise OOOServerException("Unsupported conversion: from '%s' to '%s'" % (doctype, format), True)

	def file_url(self, path):
		return uno.systemPathToFileUrl(os.path.abspath(path))

	def process(self, job):
		src_file = self.save_document(job)
		(root, ext) = os.path.splitext(src_file)
		dst_file = root + '_result' + ext

		if (job['format'] == ''):
			job['format'] = 'pdf'

		src_file_url = self.file_url(src_file)
		filter_name = self.filter_name(job['doctype'], job['format'])

		try:
			document = self.desktop.loadComponentFromURL(src_file_url, "_blank", 0, _unoProps(Hidden=True, ReadOnly=True, UpdateDocMode=QUIET_UPDATE))

			if document is None:
				raise OOOServerException("The document '%s' could not be opened." % src_file_url, True)

			url = self.file_url(dst_file)
			props = _unoProps(FilterName=filter_name)

			try:
				document.storeToURL(url, props)
			except IOException as e:
				raise OOOServerException("IOException: OOOServer could not store document to %s with properties %s. Exception: %s" % (url, props, e), True)

			document.close(True)
		except RuntimeException:
			raise OOOServerException("UNO RuntimeException in OOOServer. The server probably died. Unloading backend.", False)
		except IllegalArgumentException:
			raise OOOServerException("UNO IllegalArgument in OOOServer. Source file cannot be read.", True)


		logging.info('OOOServer converted %s to %s' % (src_file, dst_file))
		contents = self.load_document(dst_file)
		os.unlink(src_file)
		os.unlink(dst_file)

		return (job['format'], contents)

