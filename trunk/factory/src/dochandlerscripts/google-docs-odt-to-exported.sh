#!/usr/bin/env bash

if [ y"$1" == "y--help" -o y"$1" == "y--version" ]; then
    echo "Google Docs conversion tool google-docs-odt-to-exported.sh"
    echo "Version 0.1"
    exit
fi

# The drive command is from https://github.com/prasmussen/gdrive
GDRIVECMD=drive-linux-x64
inpath=${1:-input ODF file path}
outpath=${2:-location to export file to}

format=pdf
[[ $outpath == *.odt ]] && format=odf
[[ $outpath == *.odf ]] && format=odf

SCRIPTDIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
echo "SCRIPTDIR: $SCRIPTDIR"
CONFPATH="$SCRIPTDIR/../../conf/config.ini"
GDRIVECMD=$(grep gdrivepath "$CONFPATH" | sed 's/gdrivepath[ ]*=[ ]*//g')

echo "converting from : $inpath"
echo "to output at    : $outpath"
echo "format desired  : $format"
echo "gdrive command  : $GDRIVECMD"

result=$( $GDRIVECMD upload --convert --file $inpath )
id=$(echo "$result" | grep "Id:" | sed 's/Id:[ ]*//g' )
echo "ID: $id"
if [ "y$id" == "y" ]; then
    echo "Failed to upload, no ID found for document!"
    exit 1
else
    $GDRIVECMD download -i "$id" --format $format --stdout >| $outpath
    $GDRIVECMD delete   -i "$id" 
fi
