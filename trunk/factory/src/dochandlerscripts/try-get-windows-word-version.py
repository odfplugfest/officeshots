#!/usr/bin/env python

import win32com.client
import pythoncom

if __name__ == "__main__":
    print "Trying to find the version of Word on this computer..."
    
    pythoncom.CoInitializeEx(pythoncom.COINIT_APARTMENTTHREADED)
    Word = win32com.client.DispatchEx('Word.Application')
    print Word.Build
    pythoncom.CoUninitialize()
