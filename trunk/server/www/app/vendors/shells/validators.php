<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A shell to re-run validators
 */
class ValidatorsShell extends Shell
{
	/** @var array The models to use */
	public $uses = array('Validator', 'Testsuite');

	/**
	 * Main function. Print help and exit.
	 */
	public function main()
	{
		$this->help();
	}

	/**
	 * Re-run a subset or all validators for a test suite
	 */
	public function rerun()
	{
		$suite_id = $this->_getSuite();
		$validator = $this->_getValidator();
		$state = $this->_getState();

		$validators = $this->_getValidators($suite_id, $validator, $state);
		foreach ($validators as $validator) {
			$this->Validator->id = $validator['Validator']['id'];
			$this->Validator->defer('run');
		}

		$this->out(sprintf('Scheduled %d validators to re-run in the background.', sizeof($validators)));
	}

	/**
	 * Add missing validators to the test suites
	 */
	public function missing()
	{
		$suite_id = $this->_getSuite();
		$validator = $this->_getValidator();

		if ($validator == 'all') {
			$validators = Configure::read('Validator');
		} else {
			$validators = array();
			$validators[$validator] = Configure::read('Validator.' . $validator);
		}

		$count_add = 0;
		$count_remove = 0;

		// Find all requests
		$requests = $this->Testsuite->getRequests($suite_id, array(
			'Validator' => array('fields' => array('id', 'name')),
			'Job',
			'Job.Result',
			'Job.Result.Format',
			'Job.Result.Validator' => array('fields' => array('id', 'name')),
		));

		// Loop through the requests and add missing validators
		foreach ($requests as $request) {
			foreach ($validators as $validator_name => $validator_config) {
				if (!$validator_config) {
					continue;
				}

				$existing = Set::extract('/Validator/name', $request);
				if (!in_array($validator_name, $existing)) {
					$this->Validator->create();
					$this->Validator->save(array('Validator' => array(
						'name' => $validator_name,
						'parent_id' => $request['Request']['id'],
					)));
					$this->Validator->defer('run');
					$count_add++;
				}
			}
		}

		// Find all the results
		$results = Set::extract('/Job/Result', $requests);
		foreach ($results as $result) {
			if (empty($result['Result'])) {
				continue;
			}

			// If this is not and ODF roundtrip result, remove any validators and continue.
			// There was an old bug here that could cause non-ODF results to be assigned validators
			if ($result['Result']['Format']['code'] != 'odf') {
				foreach ($result['Result']['Validator'] as $validator_bug) {
					$this->Validator->id = $validator_bug['id'];
					$this->Validator->delete();
					$count_remove++;
				}
				
				continue;
			}

			foreach ($validators as $validator_name => $validator_config) {
				if (!$validator_config) {
					continue;
				}

				$existing = Set::extract('/Result/Validator/name', $result);
				if (!in_array($validator_name, $existing)) {
					$this->Validator->create();
					$this->Validator->save(array('Validator' => array(
						'name' => $validator_name,
						'parent_id' => $result['Result']['id'],
					)));
					$this->Validator->defer('run');
					$count_add++;
				}
			}
		}

		$this->out(sprintf('Created and scheduled %d validators to run in the background.', $count_add));

		if ($count_remove > 0) {
			$this->out(sprintf('Removed %d validators that were added in error.', $count_add));
		}
	}

	/**
	 * Ask for a test suite
	 */
	private function _getSuite()
	{
		$options = array(1 => 'All test suites');
		$suites = $this->Testsuite->find('list', array(
			'order' => 'Testsuite.name ASC',
			'recursive' => -1,
		));

		foreach ($suites as $suite) {
			$options[] = $suite;
		}

		$this->out('Please coose a test suite.');
		$this->hr();
		foreach ($options as $index => $option) {
			$this->out(sprintf('[%d] %s', $index, $option));
		}

		$suite_id = null;
		while ($suite_id === null) {
			$suiteNum = $this->in('Enter a test suite number, or q to quit', null, 'q');

			if (strtolower($suiteNum) === 'q') {
				$this->out('Exit');
				$this->_stop();
			}

			$suiteNum = intval($suiteNum) - 2;
			if ($suiteNum >= count($suites)) {
				$this->out('Invalid selection');
				continue;
			}

			if ($suiteNum == -1) {
				$suite_id = false;
			} else {
				$suite_ids = array_keys($suites);
				$suite_id = $suite_ids[$suiteNum];
			}
		}

		return $suite_id;
	}

	/**
	 * Ask what validators to rerun
	 */
	private function _getValidator()
	{
		$options = array(1 => 'All validators');

		$validators = Configure::read('Validator');
		foreach ($validators as $validator_name => $validator_config) {
			$options[] = $validator_name;
		}

		$this->out('Please choose a validator.');
		$this->hr();
		foreach ($options as $index => $option) {
			$this->out(sprintf('[%d] %s', $index, $option));
		}

		$validator = '';
		while ($validator == '') {
			$validatorNum = $this->in('Enter a validator number, or q to quit', null, 'q');

			if (strtolower($validatorNum) === 'q') {
				$this->out('Exit');
				$this->_stop();
			}

			$validatorNum = intval($validatorNum);
			if ($validatorNum > count($options)) {
				$this->out('Invalid selection');
				continue;
			}

			if ($validatorNum == 1) {
				$validator = 'all';
			} else {
				$validator = $options[$validatorNum];
			}
		}

		return $validator;
	}

	/**
	 * Ask what validator states to re-run
	 */
	private function _getState()
	{
		$this->out('What validator state so you wish to re-run?');
		$this->hr();
		$this->out('[1] All');
		$this->out('[2] Invalid');
		$this->out('[3] Error');
		$this->out('[4] Invalid and Error');

		while (true) {
			switch(strtolower($this->in('Enter a state number, or q to quit', null, 'q'))) {
				case 'q':
					$this->out('Exit');
					$this->_stop();
					break;
				case '1': return array(Validator::STATE_VALID, Validator::STATE_INVALID, Validator::STATE_ERROR);
				case '2': return array(Validator::STATE_INVALID);
				case '3': return array(Validator::STATE_ERROR);
				case '4': return array(Validator::STATE_INVALID, Validator::STATE_ERROR);
			}
		}
	}

	/**
	 * Get all validators based on test suite, validator name and state
	 */
	private function _getValidators($suite, $name, $state = array())
	{
		$conditions = array();

		if ($state) {
			$conditions['Validator.state'] = $state;
		}

		if ($name != 'all') {
			$conditions['Validator.name'] = $name;
		}

		$requests = $this->Testsuite->getRequests($suite, array(
			'Job',
			'Job.Result',
		));

		$ids = Set::extract('/Request/id', $requests);
		$ids = array_merge($ids, Set::extract('/Job/Result/id', $requests));
		$conditions['Validator.parent_id'] = $ids;

		$validators = $this->Validator->find('all', array(
			'fields' => array('id'),
			'conditions' => $conditions,
			'recursive' => -1,
		));

		return $validators;
	}

	/**
	 * Print shell help
	 */
	public function help()
	{
		$this->out('Commandline interface to work with the validators on the test suites');
		$this->hr();
		$this->out("Usage: cake validators <command>");
		$this->hr();
		$this->out('Commands:');
		$this->out("\n\trerun\n\t\tRe-run a subset or all validators for a test suite.");
		$this->out("\n\thelp\n\t\tShow this help");
		$this->out('');
	}
}

?>
