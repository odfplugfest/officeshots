<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A simple shell for adhoc commands
 */
class TempShell extends Shell
{
	/** @var array The models to use */
	public $uses = array('Request', 'Result', 'User', 'Group', 'Worker', 'Job');

	/**
	 * Main function. Print help and exit.
	 */
	public function main()
	{
		$this->help();
	}

	public function delgoogle()
	{
		$jobs = $this->Job->find('all', array(
			'conditions' => array(
				'Job.application_id' => '4aeecdef-5cb0-471e-9651-5ea5c0a80105',
				'Job.format_id' => '496de976-9a10-403c-aa90-55e6c0a80105',
			),
			'recursive' => -1,
		));

		foreach ($jobs as $job) {
			$this->Job->delete($job['Job']['id']);
		}
	}

	/**
	 * Print shell help
	 */
	public function help()
	{
		$this->out('Ad-hoc commandline interface');
		$this->out('DO NOT USE THIS SHELL UNLESS YOU WROTE IT!');
		$this->hr();
		$this->out("Usage: cake temp <command>");
		$this->hr();
		$this->out('Commands:');
		$this->out("\n\tRead the source, Luke...");
		$this->out('');
	}
}

?>
