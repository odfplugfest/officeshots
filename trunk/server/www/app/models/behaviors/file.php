<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Make a model associate with a file on the filesystem
 */
class FileBehavior extends ModelBehavior
{
	/** @var array Default settings of the behaviour */
	public $settings = array(
		// Mappings for the database fields
		'fields' => array('filename' => 'filename', 'path' => 'path', 'mimetype_id' => 'mimetype_id'),
		// Location that the files will be stored
		'directory' => '',
		// Set to true to save the model record even when there were file errors
		'saveOnError' => false,
		// The attribute in the model that contains an array of valid mimetypes
		'mimetypes' => 'mimetypes',
		// A custom validation method in the model
		'customValidate' => null
	);

	/** @var array The errors that were encountered */
	public $errors = array();

	/**
	 * Initialize the File behaviour
	 *
	 * @param object $model A reference to the model
	 * @param mixed config The configuration of the behaviour
	 * @return void
	 */
	public function setup(&$model, $config = array())
	{
		if (is_array($config)) {
			$this->settings = array_merge($this->settings, $config);
		}

		if (!is_writeable(FILES)) {
			$this->errors[] = __('File storage directory is not writeable.', true);
			return;
		}

		// Permanently bind the Mimetype model
		$model->bindModel(array('belongsTo' => array('Mimetype')), false);
	}

	/**
	 * Set file records to an uploaded file
	 *
	 * @param object $model A reference to the model
	 * @param array $file The file structure from the controller data
	 * @return boolean True on success or false on failure
	 */
	public function setFileUpload(&$model, $file = array())
	{
		$this->errors = array();

		if (!is_array($file) || sizeof($file) == 0) {
			$this->errors[] = __('There was no uploaded file', true);
			return false;
		}

		if (sizeof($file) == 1) {
			$file = array_shift($file);
			if (!is_array($file) || sizeof($file) == 0) {
				$this->errors[] = __('There was no uploaded file', true);
				return false;
			}
		}

		if (!$file['name'] || !isset($file['type']) || !$file['size'] || !$file['tmp_name'] || $file['error'] !== 0) {
			$this->errors[] = __('There was an error uploading the file. Perhaps it was too big?', true);
			$this->log("Error uploading file:\n" . print_r($file, true));
			return false;
		}

		if (!is_uploaded_file($file['tmp_name'])) {
			$this->errors[] = __('File is not an uploaded file. Attempted security breach?', true);
			return false;
		}

		if (!$this->deleteFile($model)) {
			$this->log(__('File is not an uploaded file. Attempted security breach?', true));
			return false; // Error has been set by deleteFile()
		}

		$mimestring = '';
		if (!$mimetype = $this->_getMimetype($model, $file['tmp_name'], $mimestring)) {
			$this->errors[] = sprintf(__('Invalid mimetype %s given for uploaded file', true), $mimestring);
			return false;
		}

		if (!$this->_checkMimetype($mimetype)) {
			$this->errors[] = __('Illegal mimetype.', true);
			return false;
		}

		$destination = FILES . $model->data[$model->name][$this->settings['fields']['path']] . DS . $file['name'];
		if (!file_exists(dirname($destination)) && !mkdir(dirname($destination), 0775, true)) {
			$this->errors[] = __('Could not create the destination directory.', true);
			return false;
		}

		if (!move_uploaded_file($file['tmp_name'], $destination)) {
			$this->errors[] = __('The uploaded file cannot be moved.', true);
			return false;
		}

		$model->set(array(
			$this->settings['fields']['filename'] => $file['name'],
			$this->settings['fields']['mimetype_id'] => $mimetype['Mimetype']['id'],
		));
		
		return true;
	}

	/**
	 * Set the file records to a file buffer
	 *
	 * @param object $model A reference to the model
	 * @param string $buffer A reference to a string holding the file contents
	 * @param string $filename The filename of the file
	 * @param mixed $filters a stream filter or array of stream filters to apply
	 */
	public function setFileBuffer(&$model, &$buffer, $filename, $filters = false)
	{
		$this->errors = array();
		$destination = FILES . $model->data[$model->name][$this->settings['fields']['path']] . DS . $filename;

		if (!file_exists(dirname($destination)) && !mkdir(dirname($destination), 0775, true)) {
			$this->errors[] = __('Could not create the destination directory.', true);
			return false;
		}


		if (!$file = fopen($destination, 'w')) {
			$this->errors[] = __('Cannot write file from buffer.', true);
			return false;
		}

		if ($filters) {
			if (!is_array($filters)) {
				$filters = array($filters);
			}

			foreach ($filters as $filter) {
				stream_filter_append($file, $filter, STREAM_FILTER_WRITE);
			}
		}

		fwrite($file, $buffer);
		fclose($file);
		
		$mimestring = '';
		if (!$mimetype = $this->_getMimetype($model, $destination, $mimestring)) {
			$this->errors[] = sprintf(__('Invalid mimetype %s detected for file buffer', true), $mimestring);
			unlink($destination);
			return false;
		}

		if (!$this->_checkMimetype($mimetype)) {
			$this->errors[] = __('Illegal mimetype.', true);
			unlink($destination);
			return false;
		}
		
		$model->set(array(
			$this->settings['fields']['filename'] => $filename,
			$this->settings['fields']['mimetype_id'] => $mimetype['Mimetype']['id'],
		));
		
		return true;
	}

	/**
	 * Set the file records to any file
	 *
	 * @param object $model A reference to the model
	 * @param string $path full path to the file
	 * @param string $filename The filename that the file should get. Leave as false for the original filename
	 * @param boolean $move True if the file should be moved, false if it should be copied.
	 * @return boolean True on success or false on failure
	 */
	public function setFile(&$model, $path = false, $filename = false, $move = false)
	{
		$this->errors = array();

		if (!$path) {
			$this->errors[] = __('There was no file', true);
			return false;
		}

		if (!is_readable($path)) {
			$this->errors[] = __('The file cannot be read.', true);
			return false;
		}

		$mimestring = '';
		if (!$mimetype = $this->_getMimetype($model, $path, $mimestring)) {
			$this->errors[] = sprintf(__('Invalid mimetype %s detected for file', true), $mimestring);
			return false;
		}

		if (!$this->_checkMimetype($mimetype)) {
			$this->errors[] = __('Illegal mimetype.', true);
			return false;
		}

		if (!$filename) {
			$filename = basename($path);
		}

		$destination = FILES . $model->field($this->settings['fields']['path']) . DS . $filename;

		if (file_exists($destination)) {
			if (!unlink($destination)) {
				$this->errors[] = __('The destination path already exists and cannot be deleted.');
				return false;
			}
		}

		if (!file_exists(dirname($destination))) {
			if (!mkdir(dirname($destination), 0775, true)) {
				$this->errors[] = __('Could not create the destination directory.', true);
				return false;
			}
		}

		if ($move) {
			$result = rename($path, $destination);
		} else {
			$result = copy($path, $destination);
		}

		if (!$result) {
			$this->errors = __('The file could not be copied or moved.');
			return false;
		}

		$model->set(array(
			$this->settings['fields']['filename'] => $filename,
			$this->settings['fields']['mimetype_id'] => $mimetype['Mimetype']['id'],
		));
		
		return true;
	}

	/**
	 * Delete the currrent on-disk file, if any
	 *
	 * @param object $model A reference to the model
	 * @return True on success, false on failure
	 */
	public function deleteFile(&$model)
	{
		if (!$model->id) {
			return true; // There is no model instance, so there is no file
		}

		if (($path = $this->getPath($model)) == false) {
			return true; // There is no file
		}

		if (!is_writeable($path)) {
			$this->errors[] = __('The file cannot be deleted.', true);
			return false;
		}

		return unlink($path);
	}

	/**
	 * Return the contents of the file
	 *
	 * @param object $model A reference to the model
	 * @return string The file contents or null if there is no file or it cannot be read.
	 */
	public function getContents(&$model)
	{
		if (!$model->id) {
			return null;
		}
		
		if ($path = $this->getPath($model)) {
			return file_get_contents($path);
		}

		return null;
	}

	/**
	 * Return the full filesystem path to the file, or false if there is no file
	 *
	 * @param object $model A reference to the model
	 * @return string full path to the file
	 */
	public function getPath(&$model)
	{
		if ($model->id) {
			// Load the data from the database
			$path     = $model->field($this->settings['fields']['path']);
			$filename = $model->field($this->settings['fields']['filename']);
		} elseif (isset($model->data[$model->name][$this->settings['fields']['path']])) {
			// Get the data from the currently assigned model data
			$path     = $model->data[$model->name][$this->settings['fields']['path']];
			$filename = $model->data[$model->name][$this->settings['fields']['filename']];
		} else {
			// Can't retrieve data
			return false;
		}
		
		if (!$path || !$filename || !is_file(FILES . $path . DS . $filename)) {
			return false; // There is no file
		}

		return FILES . $path . DS . $filename;
	}

	/**
	 * Return the Mimetype for a mimetype string
	 *
	 * @param string $path The path to a file to get the mimetype from
	 * return array The Mimetype search result
	 */
	protected function _getMimetype(&$model, $path, &$mimestring = '')
	{
		$finfo = finfo_open(FILEINFO_MIME, '/usr/share/misc/magic.mgc');
		$mimestring = finfo_file($finfo, $path);
		finfo_close($finfo);

		// Newer finfo also returns the charset as in <minestring>; charset=<charset>
		if (strpos($mimestring, ';') !== false) {
			$mimestring = substr($mimestring, 0, strpos($mimestring, ';'));
		}

		if ($mimestring == 'application/zip') {
			// Guess from the extension
			$ext = substr($path, strrpos($path, '.') + 1);

			$mimetype = $model->Mimetype->find('first', array(
				'conditions' => array(
					'Mimetype.extension' => $ext,
				),
			));

			return $mimetype;
		}

		$mimetype = $model->Mimetype->find('first', array(
			'conditions' => array(
				'Mimetype.name' => $mimestring,
			),
		));

		return $mimetype;
	}

	/**
	 * Check mimeinfo of the file
	 *
	 * TODO: Implement this. It's a dummy now
	 * @param array $mimetype The mimetype to check
	 * @return boolean True if the mimetype is valid, False otherwise
	 */
	protected function _checkMimetype($mimetype)
	{
		return true;
	}

	/**
	 * Delete the file when the record is deleted
	 *
	 * @param object $model A reference to the model
	 * @return True on success, false to abort the deletion
	 */
	public function beforeDelete(&$model)
	{
		$path = $this->getPath($model);
		if ($path && file_exists($path)) {
			if (!@unlink($path)) {
				$this->errors[] = __('The file could not be deleted.', true);
			}
		}
		
		if (!empty($this->errors) && !$this->settings['saveOnError']) {
			return false;
		}

		return true;
	}
}
