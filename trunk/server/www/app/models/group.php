<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * The Group Model
 */
class Group extends AppModel
{
	public $actsAs = array('Containable');

	/** @var array Groups have multiple permissions */
	public $hasMany = array(
		'Permission' => array('dependent' => true),
	);

	/** @var array The many-to-many relationships */
	public $hasAndBelongsToMany = array(
		'User' => array('unique' => false)
	);

	/**
	 * Check if a user is a member of a group
	 *
	 * @param string $user_id The user to check
	 * @param string group_id The group ID. If not set, $this->id will be used
	 * @return boolean True if the user is a member, false otherwise
	 */
	public function has_member($user_id, $group_id = null)
	{
		if ($group_id === null) {
			$group_id = $this->id;
		}

		$group = $this->read(null, $group_id);
		if (is_array($group['User'])) {
			foreach ($group['User'] as $user) {
				if ($user['id'] == $user_id) {
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * Add a user to the group
	 *
	 * @param string $user_id The user to add
	 * @param string group_id The group ID. If not set, $this->id will be used
	 * @return boolean Success
	 */
	public function add_member($user_id, $group_id = null)
	{
		if ($group_id !== null) {
			$this->id = $group_id;
		}

		$relation = $this->GroupsUser->find('first', array('conditions' => array(
			'group_id' => $group_id,
			'user_id' => $user_id,
		)));

		if (!empty($relation)) {
			return true;
		}

		$this->GroupsUser->create();
		return $this->GroupsUser->save(array('GroupsUser' => array(
			'group_id' => $group_id,
			'user_id' => $user_id,
		)));
	}

	/**
	 * Add the user to all groups of which he is a default member
	 * Note that this does not remove him from groups of which he is not a default member
	 *
	 * @param string $user_id The user to add
	 */
	public function add_default_member($user_id)
	{
		$user = $this->User->read(null, $user_id);
		$groups = $this->find('all', array(
			'recursive' => -1,
			'conditions' => array('Group.default_memberlist <>' => '')
		));

		if (!is_array($groups)) {
			return;
		}

		foreach ($groups as $group) {
			$this->id = $group['Group']['id'];
			$addresses = @file($group['Group']['default_memberlist'], FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

			if (!is_array($addresses)) {
				continue;
			}

			if (in_array($user['User']['email_address'], $addresses) && !$this->has_member($user['User']['id'])) {
				$this->add_member($user['User']['id']);
			}
		}
	}

	/**
	 * After creating a new 'default' group, add all existing users to it.
	 * @param boolean $created Whether the saved user was newly created or not
	 */
	public function afterSave($created)
	{
		if (!$created || !$this->data['Group']['default']) {
			return;
		}

		$users = $this->User->find('all', array(
			'recursive' => -1,
		));

		foreach ($users as $user) {
			$this->add_member($user['User']['id'], $this->id);
		}
	}
}

?>
