<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

App::import('Core', 'Clamd.Clamd');
App::import('Behavior', 'Pipeline');

/**
 * The Request model
 */
class Request extends AppModel
{
	/**#@+
	 * Request states
	 */
	const STATE_UPLOADING   = 1;
	const STATE_PREPROCESSOR_QUEUED = 2;
	const STATE_SCAN_FOUND  = 4;
	const STATE_PREPROCESSOR_FAILED = 8;
	const STATE_QUEUED      = 16;
	const STATE_FINISHED    = 32;
	const STATE_EXPIRED     = 64;
	const STATE_CANCELLED   = 128;
	/**#@-*/

	/** @var array Every request belongs to a user and is associated with a document type and a desired output format */
	public $belongsTo = array('User');

	/** @var string A request consists of multiple jobs and has multiple ODF validators */
	public $hasMany = array('Job', 'Validator' => array('foreignKey' => 'parent_id'));

	/** @var array Requests can belong to multiple galleries */
	public $hasAndBelongsToMany = array('Gallery' => array('unique' => false));

	/** @var array Use the file behaviour to associate ODF files with Requests */
	public $actsAs = array(
		'Containable',
		'BeanStalk.Deferrable',
		'File',
		'Pipeline' => 'Preprocessor',
	);

	/** @var string Use the filename as the distinguising name */
	public $displayField = 'filename';

	/**
	 * Constructor
	 * Build the pipelines
	 */
	public function __construct()
	{
		parent::__construct();

		$this->Preprocessor->callback('scan');
		$this->Preprocessor->callback('validateFile');
		$this->Preprocessor->callback('queue');
		$this->Preprocessor->errback('preprocessorError');
	}

	/**
	 * Check access control for this request
	 * @param string $user_id The user ID
	 * @param string $type "read" or "write"
	 * @param string $id The request ID
	 * @return boolean True or False
	 */
	public function checkAccess($user_id, $type = 'read', $id = false)
	{
		if (!$id) {
			$id = $this->id;
		}

		if (!$id) {
			return false;
		}

		if ($type == 'admin') {
			return true;
		}

		$request = $this->find('first', array(
			'contain' => array('Gallery'),
			'conditions' => array('Request.id' => $id),
		));

		if ($request['Request']['user_id'] == $user_id) {
			return true;
		}

		if (isset($request['Gallery']) && !empty($request['Gallery'])) {
			foreach ($request['Gallery'] as $gallery) {
				if ($this->Gallery->checkAccess($user_id, $gallery['id'])) {
					return true;
				}
			}

			if ($type == 'read') {
				return true;
			}
		}

		if (Configure::read('Auth.allowAnonymous') && empty($request['Request']['user_id'])) {
			return true;
		}

		return false;
	}

	/**
	 * Check if the request belongs to a test suite
	 * @param The request ID, or NULL for $this->id
	 * @return Boolean
	 */
	public function inTestsuite($id = null)
	{
		if (!$id) {
			$id = $this->id;
		}

		if (!$id) {
			return false;
		}

		$request = $this->find('first', array(
			'contain' => array('Gallery'),
			'conditions' => array('Request.id' => $id),
		));

		if (isset($request['Gallery']) && !empty($request['Gallery'])) {
			foreach ($request['Gallery'] as $gallery) {
				if ($this->Gallery->isTestsuite($gallery['id'])) {
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * Add an upload to the request
	 *
	 * If the add fails, &$errors contains the error messages
	 *
	 * @param array $data The data to save if not saving $this->data, including a file and the jobs
	 * @param array &$errors The errors
	 * @return boolean Success
	 */
	public function addUpload($data, &$errors = array())
	{
		if (empty($data)) {
			$errors[] = __('The request was empty.', true);
			return false;
		}

		// Check that we have a file and jobs
		if ((!isset($data['Request']['FileUpload']) || !is_array($data['Request']['FileUpload']))) {
			$errors[] = __('You did not submit a file.', true);
			return false;
		}

		if (!$this->setFileUpload($data['Request']['FileUpload'])) {
			$errors[] = __('The file upload failed. Please try again.', true);
			$errors = array_merge($errors, $this->Behaviors->File->errors);
			return false;
		}
		$this->save();

		// Run the ODF anonymiser if requested
		if ($data['Request']['anonymise']) {
			if (!$this->anonymise()) {
				$this->deleteFile();
				$errors[] = __('The Anonymiser failed to run', true);
				return false;
			}
		}
		unset($data['Request']['anonymise']);

		return $this->save();
	}

	/**
	 * Add jobs to this request
	 *
	 * @param array $jobs An array of jobs
	 * @param string $id The request ID
	 * @return int The number of jobs created
	 */
	public function addJobs($jobs = array(), $id = null)
	{
		// Load the Application model. We need that below.
		$Application = ClassRegistry::init('Application');

		// Load the request
		if (!$id) {
			$id = $this->id;
		}

		if (!$id) {
			return 0;
		}

		$request = $this->find('first', array(
			'conditions' => array('Request.id' => $id),
			'recursive' => -1,
		));

		// Find the Mimetype associated with the request
		// We can use this to check the doctype on the submitted jobs below
		$mimetype = $this->Mimetype->find('first', array('conditions' => array(
			'Mimetype.id' => $request['Request']['mimetype_id'],
		)));

		$jobCount = 0;
		foreach ($jobs as $job) {
			$job['request_id'] = $id;

			// Check the request mimetype against the supported doctype
			$count = $Application->find('count', array(
				'contain' => array(
					'Application.id' => $job['application_id'],
					'Doctype.code' => $mimetype['Doctype']['code']
				),
			));

			if ($count == 0) {
				continue;
			}

			$this->Job->create();
			if ($this->Job->save(array('Job' => $job))) {
				$jobCount++;
			}
		}

		// If this request had already finished and isn't expired yet, set it back to queued.
		$requeue = (
			$request['Request']['state'] == self::STATE_FINISHED
			&& ($request['Request']['expire'] == '0000-00-00 00:00:00' || strtotime($request['Request']['expire']) < time())
		);

		if ($requeue) {
			$this->saveField('state', self::STATE_QUEUED);
		}

		return $jobCount;
	}

	/**
	 * Add ODF validators for the request
	 */
	public function addValidators()
	{
		// First remove any existing validators
		$this->Validator->deleteAll(array(
			'Validator.parent_id' => $this->id,
		));

		// Add new validators
		$validators = Configure::read('Validator');
		foreach ($validators as $validator_name => $validator_config) {
			if (!$validator_config) {
				continue;
			}

			$this->Validator->create();
			$this->Validator->save(array('Validator' => array(
				'name' => $validator_name,
				'parent_id' => $this->id,
			)));
		}
	}

	/**
	 * Cancel the request
	 *
	 * @return void
	 */
	public function cancel()
	{
		if (!$this->id) {
			return;
		}

		$this->save(array('Request' => array(
			'state' => self::STATE_CANCELLED,
			'expire' => date('Y-m-d H:i:s', time() - 1)
		)));

		$this->failJobs();
	}

	/**
	 * Set Expired state on all expired requests
	 *
	 * @return boolean Success
	 */
	public function expireAll()
	{
		// Mark all old requests as expired

		$requests = $this->find('all', array(
			'conditions' => array(
				'Request.expire <=' => date('Y-m-d H:i:s'),
				'Request.state' => array(self::STATE_UPLOADING, self::STATE_PREPROCESSOR_QUEUED, self::STATE_QUEUED),
				'NOT' => array('Request.expire' => null),
			),
			'recursive' => -1,
		));

		foreach ($requests as $request) {
			$this->id = $request['Request']['id'];
			$this->expire();
		}

		return true;
	}

	/**
	 * Expire a single request
	 */
	public function expire()
	{
		$this->saveField('state', self::STATE_EXPIRED);
		$this->failJobs();
	}

	/**
	 * Delete all jobs and results belonging to the request
	 */
	public function deleteJobs($id = null)
	{
		if (!$id) {
			$id = $this->id;
		}

		if (!$id) {
			return;
		}

		$request = $this->find('first', array(
			'contain' => array('Job'),
			'conditions' => array('Request.id' => $id),
		));

		if (!$request || !isset($request['Job']) || empty($request['Job'])) {
			return;
		}

		foreach ($request['Job'] as $job) {
			$this->Job->delete($job['id']); // Results are deleted in Job::beforeDelete
		}
	}

	/**
	 * Mark all unfinished jobs for this request as failed
	 */
	public function failJobs($id = null)
	{
		if (!$id) {
			$id = $this->id;
		}

		if (!$id) {
			return;
		}

		$request = $this->find('first', array(
			'contain' => array('Job'),
			'conditions' => array('Request.id' => $id),
		));

		if (!$request || !isset($request['Job']) || empty($request['Job'])) {
			return;
		}

		foreach ($request['Job'] as $job) {
			if ($job['state'] == Job::STATE_QUEUED) {
				$this->Job->id = $job['id'];
				$this->Job->saveField('state', Job::STATE_FAILED);
			}
		}
	}

	/**
	 * Generate a zipfile containing the original request and all results
	 *
	 * @return array an array containing the directory, id and filename of the zipfile to be used by the media view, or null
	 */
	public function createZip()
	{
		if (!isset($this->id)) {
			return null;
		}

		$root = FILES . $this->field('root');
		$filename = $this->field('filename');
		$filename = substr($filename, 0, strrpos($filename, '.'));
		$directory = TMP . 'zips';
		$id = $this->id . '.zip';

		if (file_exists($directory . DS . $id)) {
			unlink($directory . DS . $id);
		}

		$cwd = getcwd();
		chdir($root);
		shell_exec('cd "' . $root . '" && zip -r "' . $directory . DS . $id . '" .');
		chdir($cwd);

		return compact('id', 'directory', 'filename');
	}

	/**
	 * Convert the Markdown description to HTML before saving
	 * @return boolean True to continue saving
	 */
	public function beforeSave()
	{
		if (isset($this->data['Request']['description'])) {
			// Convert the description to HTML
			App::import('Vendor', 'markdown');
			App::import('Vendor', 'HTMLPurifier', array('file' => 'htmlpurifier/HTMLPurifier.standalone.php'));

			$config = HTMLPurifier_Config::createDefault();
			$config->set('Cache.SerializerPath', CACHE . DS . 'htmlpurifier');
			$purifier = new HTMLPurifier($config);

			$desc_html = Markdown($this->data['Request']['description']);
			$this->data['Request']['description_html'] = $purifier->purify($desc_html);

			// Save the Markdown version to file
			if ($this->id) {
				$path = FILES . $this->field('root') . DS . 'description.txt';
			} else {
				$path = FILES . $this->data['Request']['root'] . DS . 'description.txt';
			}

			file_put_contents($path, $this->data['Request']['description']);
		}

		return true;
	}

	/**
	 * When a request is deleted, also delete the files and jobs
	 */
	public function beforeDelete($cascade)
	{
		$this->deleteJobs();
		$root = $this->data['Request']['root'];
		if ($root && file_exists(FILES . $root)) {
			$Folder = new Folder(FILES . $root);
			$Folder->delete();
		}
	}

	/**
	 * Scan this request for viruses
	 * This function is called from the Preprocessor pipeline
	 * @return True to continue the pipeline, False to switch the pipeline to errback
	 */
	public function scan()
	{
		$clamd_config = Configure::read('Clamd');
		if (!$clamd_config) {
			$this->log('Clamd not configued. Skipping scan...', LOG_DEBUG);
			return true; // Continue with the rest of the pipeline
		}

		$this->contain();
		$this->read();

		$clamd = new Clamd($clamd_config);
		$path = $this->getPath();
		$result = '';

		if (!$path) {
			$this->log('Request could not be scanned. ' . $path . ' (Request ID: ' . $this->id . ') does not exists. ');
			return false;
		}

		$status = $clamd->scan($path, $result);
		if ($status == Clamd::OK) {
			// Scan OK. Queue the Request for processing
			$this->log('Clamd scanned ' . $path . ' (Request ID: ' . $this->id . '): OK', LOG_DEBUG);
		} elseif ( $status == Clamd::FOUND ) {
			// There was a virus. Remove all jobs and alert user
			// Note that the file is *not* deleted. This was we can later check if there really was a virus
			$this->Job->deleteAll(array('Job.request_id' => $this->id));
			$this->data['Request']['state'] = self::STATE_SCAN_FOUND;
			$this->data['Request']['state_info'] = $result;
			$this->data['Request']['expire'] = date('Y-m-d H:i:s');
			$this->save();

			$this->log('Clamd scanned ' . $path . ' (Request ID: ' . $this->id . '): FOUND ' . $result, LOG_DEBUG);
			return false;
		} else {
			// There was an error. Remove all jobs and alert user
			if ($status === false) {
				$result = $clamd->lastError();
				$status = Clamd::ERROR;
			}

			$this->Job->deleteAll(array('Job.request_id' => $this->id));
			$this->data['Request']['state_info'] = $result;
			$this->data['Request']['expire'] = date('Y-m-d H:i:s');
			$this->save();

			$this->log('Clamd error scanning ' . $path . ' (Request ID: ' . $this->id . '): ' . $result);
			return false;
		}

		return $this->save();
	}

	/**
	 * Run all the ODF validators associated with this request
	 */
	public function validateFile()
	{
		$this->contain('Validator');
		$this->read();

		$this->errors = array();
		if (!is_array($this->data['Validator'])) {
			$this->log('No validators found for Request: ' . $this->id, LOG_DEBUG);
			return true;
		}

		foreach ($this->data['Validator'] as $validator) {
			$this->Validator->id = $validator['id'];
			$this->Validator->run();
		}

		return true; // To continue the rest of the pipeline
	}

	/**
	 * Run the request through the iTools greeking utility
	 * @return boolean Success
	 */
	public function anonymise()
	{
		$bin = Configure::read('Anonymiser.path');
		if (!$bin || !is_executable($bin)) {
			$this->log('Anonymiser not configured');
			return false;
		}

		$path = $this->getPath();
		$this->log('Anonymiser path: ' . $path, LOG_DEBUG);
		if (!is_readable($path) || !is_writeable($path)) {
			$this->log('Anonymiser target file not readable or writeable');
			return false;
		}

		$spec = array(
			0 => array('pipe', 'r'), // STDIN
			1 => array('pipe', 'w'), // STDOUT
			2 => array('pipe', 'w')  // STDERR
		);

		$proc = proc_open($bin . ' -o ' . $path . ' ' . $path, $spec, $pipes);
		if (!is_resource($proc)) {
			$this->log('Anonymiser failed to run');
			return false;
		}

		$stdout = stream_get_contents($pipes[1]);
		$stderr = stream_get_contents($pipes[2]);
		fclose($pipes[0]);
		fclose($pipes[1]);
		fclose($pipes[2]);
		proc_close($proc);

		$this->log('Anonymiser STDOUT: ' . $stdout, LOG_DEBUG);
		$this->log('Anonymiser STDERR: ' . $stderr, LOG_DEBUG);

		if (trim($stderr)) {
			return false;
		}

		return true;
	}

	public function queue()
	{
		if ($this->field('job_count') == 0) {
			$this->saveField('state', self::STATE_FINISHED);
			$this->log('Request ' . $this->id . ' finished. No jobs found.', LOG_DEBUG);
		} else {
			$this->saveField('state', self::STATE_QUEUED);
			$this->log('Request ' . $this->id . ' queued.', LOG_DEBUG);
		}
	}

	public function preprocessorError()
	{
		$state = $this->field('state');
		if ($state == self::STATE_PREPROCESSOR_QUEUED) {
			$this->saveField('state', self::STATE_PREPROCESSOR_FAILED);
		}
		$this->log('Request ' . $this->id . ' halted. Preprocessor failed.', LOG_DEBUG);
	}
}

?>
