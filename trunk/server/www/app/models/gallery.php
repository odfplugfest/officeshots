<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A helper class to render a cachable HTML table fragment that shows
 * a gallery. It is here so that the cron task can easily call this to
 * create and updated HTML fragment that can be used for fast gallery
 * viewing.
 *
 * The method you are interested in calling is getCachedTableString().
 */
class GalleryCacheCreator
{
    public $indent = 0;
    
	public function htmlimage( $img )
    {
		$ret = '<img src="/img/' . $img . '"/>';
		return $ret;
	}
    public function htmllink( $name, $earl, $lid )
    {
        $earl = $earl . '/' . $lid;
        $link = '<a href="' . $earl . '">' . $name . '</a>';
        return $link;
    }
	public function getStateIcon($validator, $link = true)
    {
		if (!is_array($validator)) {
			return '';
		}

		if (!isset($validator['Validator'])) {
			$validator = array('Validator' => $validator);
		}
		
		switch ($validator['Validator']['state']) {
		case Validator::STATE_PENDING:
			$image = $this->htmlimage('icons/validator-pending.png');
			break;
		case Validator::STATE_VALID:
			$image = $this->htmlimage('icons/validator-valid.png');
			break;
		case Validator::STATE_INVALID:
			$image = $this->htmlimage('icons/validator-invalid.png');
			break;
		case Validator::STATE_ERROR:
			$image = $this->htmlimage('icons/validator-error.png');
			break;
		}

		if ($link) {
			return $this->htmllink($image, '/validators/view/', $validator['Validator']['id']);
		}
		return $image;
	}
    
    public function indentToStyleSubTag()
    {
        return ' text-align:left; padding-left: ' . $this->indent . 'em; ';
    }
    
    public function getCachedTableStringForSubGallery( $gallery, $applications ) 
    {
        $ret = '';
        if (!(isset($gallery['Request']) && is_array($gallery['Request'])))
            return $ret;

        $ret .= "<tbody>";
        $ret .= "<tr>";
        foreach ($gallery['Request'] as $request) {
            $ret .= '<tr class="testsuite-request">';
            $earl = '/requests/view/' . $request['id'];
            $name = $request['filename'];
            $ret .= '<td class="request-name" style="' . $this->indentToStyleSubTag() . ' ">'
                .   '<a href="' . $earl . '">' . $name . '</a></td>';

            $ret .= '<td>';
            foreach ($request['Validator'] as $validator) { $ret .= $this->getStateIcon($validator) . ' '; } 
            $ret .= '</td>';

            foreach ($applications as $application) {
				foreach ($request['Job'] as $job) {
					$match = (
						$job['Application']['name'] == $application['Application']['name']
						&& $job['version'] == $application['Application']['version']
						&& $job['Format']['code'] == 'odf'
                        );

					if ($match) {
						$class = 'white';
						if ($job['Result']) {
							switch($job['Result']['verified']) {
								case Result::VERIFY_PASS: $class = 'green'; break;
								case Result::VERIFY_FAIL: $class = 'red'; break;
							}
						}
						$ret .= '<td class="' . $class . '">';

						if (!$job['Result'] || !$job['Result']['id']) {
							$ret .= $this->htmlimage('/icons/validator-pending.png');
							break;
						}

						if (isset($job['Result']['Validator'])) {
							foreach ($job['Result']['Validator'] as $validator) {
								$ret .= $this->getStateIcon($validator) . ' ';
							}
						}

						$request['result_count']--;
						break;
					}
				}

				if (!$match) {
					$ret .= '<td> -';
				}
			}


			$ret .= "<td>";
            if ($request['result_count']) {
                $ret .= $this->htmllink(
                    str_replace(' ', '&nbsp;', sprintf(__('%d more', true), $request['result_count'])),
                    '/requests/view/', $request['id']);
            }
			$ret .= "</td>";
			if ($access) {
				$ret .= '<td class="actions">';
				$ret .= $this->htmllink(__('Remove', true), '/galleries/remove_document/' . $gallery['Gallery']['slug'], $request['id']);
				$ret .= '</td>';
			}


            $ret .= '</tr>';
        }
        $ret .= "</tbody>";
        return $ret;
    }

    

    public function getCachedTableStringBody( $gallery, $applications )
    {
        $ret = '';

        foreach ($gallery['children'] as $child) {
            $name = $child['Gallery']['name'];
            $ret .= '<thead class="testsuite-request" '
                . ' style="background-color: #ddddff; '
                . '" >';
            $ret .= '<tr class="subgallery">'
                . '<td class="subgallery-name" style="' . $this->indentToStyleSubTag() . ' ">'
                . $this->htmllink($name,'/galleries/view/',$child['Gallery']['slug'])
                . '</td>';
            $extra = 2;

            if ($access) {
                $extra++;
            }
            for ($i = 0; $i < count($applications)+$extra; $i++) {
                $ret .= '<td>-</td>';
            }
            $ret .= "</thead>\n";

            $this->indent += 2;
            $ret .= $this->getCachedTableStringBody( $child, $applications );
            
            // display the documents in this subgallery
            $ret .= $this->getCachedTableStringForSubGallery( $child, $applications );
            $this->indent -= 2;
        }

        
        
        return $ret;
    }
    
    public function getCachedTableString( $gallery, $applications )
    {
        $this->indent = 1;
        $appsz = count($applications);
        $ret = "";

//        $ret .= "<pre>" . print_r($gallery) . "</pre>";
        
        // datatables.
        //$ret .= '<table id="gallery" class="stripe row-border order-column" >'; // class="table table-striped">';

        // bootstrap.
        $ret .= '<table id="gallery" class="table table-striped">';
        $ret .= "\n";

        // header for each app
        $ret .= '<thead>';
        $ret .= '<tr>';
        $ret .= '<th>File</th>';
        $ret .= '<th>Validators</th>';
		foreach($applications as $application) {
			$ret .= '<th>';
            if ($application['Application']['icon']) {
                $ret .= $this->htmlimage('/icons/applications/' . $application['Application']['icon']);
                $ret .= '<br />' . $application['Application']['version'];
            } else {
                $ret .= $application['Application']['name'] . '<br />' . $application['Application']['version'];
            }
			$ret .= '</th>';
		}
        $ret .= '<th>Other</th>';
        if ($access) {
        	$ret .= '<th>Actions</th>';
        }
        $ret .= '</tr>';
        $ret .= '</thead>';


        // show each subgallery
        $ret .= $this->getCachedTableStringBody( $gallery, $applications );
        if( count($gallery['children']) == 0 ) 
        {
            $ret .= $this->getCachedTableStringForSubGallery( $gallery, $applications );
        }

        
        $ret .= "</tbody></table>\n";
        $ret .= "<br/>";
        $slug = $gallery['Gallery']['slug'];
        $ret .= '<p class="lead"><a href="http://officeshots.org/galleries/viewold/' . $slug . '">Click to see live results...</a></p>';
        return $ret;
    }
    
    
    
}

/**
 * The Gallery model
 *
 * Galleries can be used to publicly display requests
 */
class Gallery extends AppModel
{
	/** @var array Galleries have an owner and optionally a group who can edit the gallery */
	public $belongsTo = array('Group', 'User');

	/** @var string Every application supports a certain ODF doctype */
	public $hasAndBelongsToMany = array('Request' => array('unique' => false));

	/** Some galleries have a Testsuite */
	public $hasOne = 'Testsuite';

	/** @var array The model behaviors */
	public $actsAs = array(
		'Containable',
		'Sluggable' => array('label' => 'name', 'overwrite' => true),
		'Tree',
	);

	/**
	 * Check access control for this gallery
	 * @param string $user_id The user ID
	 * @param string $id The gallery ID
	 * @return boolean True or False
	 */
	public function checkAccess($user_id, $id = false)
	{
		if (!$id) {
			$id = $this->id;
		}

		if (!$id) {
			return false;
		}

		$gallery = $this->find('first', array(
			'conditions' => array('Gallery.id' => $id),
			'recursive' => -1,
		));

		if (!$gallery) {
			return false;
		}

		if ($gallery['Gallery']['user_id'] == $user_id) {
			return true;
		}

		if ($this->User->Group->has_member($user_id, $gallery['Gallery']['group_id'])) {
			return true;
		}

		return false;
	}

	/**
	 * Determine if this gallery is a test suite gallery
	 * @param string $id The gallery ID
	 * @return boolean True or False
	 */
	public function isTestsuite($id = null)
	{
		if (!$id) {
			$id = $this->id;
		}

		if (!$id) {
			return false;
		}

		// First, find the root of the gallery tree
		$gallery = $this->find('first', array(
			'conditions' => array('Gallery.id' => $id),
			'recursive' => -1,
		));

		if ($gallery['Gallery']['parent_id']) {
			$root = $this->find('first', array(
				'conditions' => array(
					'Gallery.parent_id IS NULL',
					'Gallery.lft <=' => $gallery['Gallery']['lft'],
					'Gallery.rght >=' => $gallery['Gallery']['rght'],
				),
				'recursive' => -1,
			));
		} else {
			$root = $gallery;
		}

		$count = $this->Testsuite->find('count', array(
			'conditions' => array('Testsuite.gallery_id' => $root['Gallery']['id']),
			'recursive' => -1,
		));

		return ($count > 0);
	}

	/**
	 * Add a request to a gallery
	 * @param string $request_id The request to add
	 * @param string $id The gallery ID, or $this->id
	 */
	public function addRequest($request_id, $id = null)
	{
		if (!$id) {
			$id = $this->id;
		}

		$relation = $this->GalleriesRequest->find('first', array('conditions' => array(
			'gallery_id' => $id,
			'request_id' => $request_id,
		)));

		if (!empty($relation)) {
			return true;
		}

		$this->GalleriesRequest->create();
		return $this->GalleriesRequest->save(array('GalleriesRequest' => array(
			'gallery_id' => $id,
			'request_id' => $request_id,
		)));
	}

	/**
	 * Remove a request from a gallery
	 * @param string $request_id The request to add
	 * @param string $id The gallery ID, or $this->id
	 */
	public function removeRequest($request_id, $id = null)
	{
		if (!$id) {
			$id = $this->id;
		}

		$relation = $this->GalleriesRequest->find('first', array('conditions' => array(
			'gallery_id' => $id,
			'request_id' => $request_id,
		)));

		if (!empty($relation)) {
			return $this->GalleriesRequest->del($relation['GalleriesRequest']['id']);
		}

		return true; // The relation didn't exist to begin with
	}

	/**
	 * Count the number of requests in this gallery
	 * @param int $id The Gallery ID
	 * @param bool $recursive True to count requests in subgalleries as well
	 */
	public function requestCount($id = null, $recursive = false)
	{
		if (!$id) {
			$id = $this->id;
		}

		if (!$id) {
			return 0;
		}

		$num_requests = $this->GalleriesRequest->find('count', array(
			'conditions' => array('gallery_id' => $id),
		));

		if ($recursive) {
			$gallery = $this->read(null, $id);

			if (!$gallery['Gallery']['lft'] && !$gallery['Gallery']['rght']) {
				return $num_requests;
			}

			$num_documents = $this->Request->query('SELECT COUNT(*) as `count`
				FROM `requests` AS `Request`
				LEFT JOIN `galleries_requests` AS `GalleriesRequest`
					ON `Request`.`id` = `GalleriesRequest`.`request_id`
				LEFT JOIN `galleries` AS `Gallery`
					ON `GalleriesRequest`.`gallery_id` = `Gallery`.`id`
				WHERE
					`Gallery`.`lft` > ' . $gallery['Gallery']['lft'] . '
					AND `Gallery`.`rght` < ' . $gallery['Gallery']['rght']);
			$num_requests = $num_requests + $num_documents[0][0]['count'];
		}

		return $num_requests;
	}

	/**
	 * Return all requests and jobs associated with a gallery and all subgalleries
	 * @param int $id The Gallery ID
	 * @return array
	 */
	public function getRequests($id = null, $contain = array())
	{
		if (!$id) {
			$id = $this->id;
		}

		if (!$id) {
			return array();
		}

		$gallery = $this->find('first', array(
			'conditions' => array('Gallery.id' => $id),
			'recursive' => -1,
		));

		if (empty($gallery)) {
			return array();
		}

		$result = $this->GalleriesRequest->find('all', array(
			'conditions' => array('GalleriesRequest.gallery_id' => $id),
			'recursive' => -1,
		));
		$request_ids = Set::extract('/GalleriesRequest/request_id', $result);

		$result = $this->query('SELECT DISTINCT `GalleriesRequest`.`request_id`
			FROM `galleries_requests` AS `GalleriesRequest`
			LEFT JOIN `galleries` AS `Gallery`
				ON `GalleriesRequest`.`gallery_id` = `Gallery`.`id`
			WHERE
				`Gallery`.`lft` > ' . $gallery['Gallery']['lft'] . '
				AND `Gallery`.`rght` < ' . $gallery['Gallery']['rght']);
		$subgallery_request_ids = Set::extract('/GalleriesRequest/request_id', $result);
		$request_ids = Set::merge($request_ids, $subgallery_request_ids);

		$requests = $this->Request->find('all', array(
			'contain' => $contain,
			'conditions' => array('Request.id' => $request_ids),
		));

		return $requests;
	}

	/**
	 * Get the view data for a gallery
	 * This is implemented in the model so we can call it in the background and cache the result
	 */
	public function viewData($id)
	{
		// Get the gallery
		$prof_start = microtime(true);
		$gallery = $this->find('first', array(
			'conditions' => array('Gallery.id' => $id),
			'contain' => array(
				'Request' => array('order' => 'Request.filename ASC'),
				'Request.Validator' => array(
					'fields' => array('id', 'parent_id', 'name', 'state'),
					'order' => 'Validator.name ASC',
				),
				'Request.Job' => array('order' => array(
					'Application.name ASC',
					'Job.version ASC',
					'Platform.name ASC',
					'Format.name ASC',
				)),
				'Request.Job.Application',
				'Request.Job.Format',
				'Request.Job.Platform',
				'Request.Job.Result',
				'Request.Job.Result.Validator' => array(
					'fields' => array('id', 'parent_id', 'name', 'state'),
					'order' => 'Validator.name ASC',
				),
			),
		));

		if (empty($gallery)) {
			return array(
				'gallery' => array(),
				'applications' => array(),
				'path' => array(),
				'abbreviate' => false,
				'isTestsuite' => false,
			);
		}

		// Find out if this test suite needs to be abbreviated
		$requestCount = $this->requestCount($id, true);
		$isTestsuite = $this->isTestsuite($id);
		$abbreviate = ($requestCount >= Configure::read('Gallery.abbreviate') && !$isTestsuite);

		// Get the children of this gallery
		$children = $this->find('all', array(
			'conditions' => array(
				'Gallery.lft >' => $gallery['Gallery']['lft'],
				'Gallery.rght <' => $gallery['Gallery']['rght'],
			),
			'contain' => array('Request' => array('order' => 'Request.filename ASC')),
			'order' => array('Gallery.name ASC'),
		));

		foreach ($children as &$child) {
			foreach ($child['Request'] as &$request) {

				$request['Validator'] = $this->Request->Validator->find('all', array(
					'fields'     => array('id', 'parent_id', 'name', 'state'),
					'conditions' => array('Validator.parent_id' => $request['id']),
					'order'      => 'Validator.name ASC',
					'recursive'  => -1,
				));

				if (!$abbreviate) {
					$request['Job'] = $this->Request->Job->query("SELECT
							`Job`.`id`,
							`Job`.`request_id`,
							`Job`.`platform_id`,
							`Job`.`application_id`,
							`Job`.`version`,
							`Job`.`format_id`,
							`Job`.`result_id`,
							`Job`.`locked`,
							`Platform`.`id`,
							`Platform`.`name`,
							`Application`.`id`,
							`Application`.`name`,
							`Result`.`id`,
							`Result`.`state`,
							`Result`.`state_info`,
							`Result`.`verified`,
							`Result`.`created`,
							`Format`.`id`,
							`Format`.`name`,
							`Format`.`icon`,
							`Format`.`code`
						FROM `jobs` AS `Job`
							LEFT JOIN `platforms` AS `Platform` ON (`Job`.`platform_id` = `Platform`.`id`)
							LEFT JOIN `applications` AS `Application` ON (`Job`.`application_id` = `Application`.`id`)
							LEFT JOIN `results` AS `Result` ON (`Job`.`result_id` = `Result`.`id`)
							LEFT JOIN `formats` AS `Format` ON (`Job`.`format_id` = `Format`.`id`)
						WHERE `Job`.`request_id` = '" . $request['id'] . "'
						ORDER BY
							`Application`.`name` ASC,
							`Job`.`version` ASC,
							`Platform`.`name` ASC,
							`Format`.`name` ASC");

					// Fix array layout to match a regular query layout
					foreach ($request['Job'] as &$job) {
						foreach ($job['Job'] as $attr => $value) {
							$job[$attr] = $value;
						}
						unset($job['Job']);

						if (!isset($job['Result']['id']) || !$job['Result']['id']) {
							continue;
						}

						$job['Result']['Validator'] = $this->Request->Validator->find('all', array(
							'fields'     => array('id', 'parent_id', 'name', 'state'),
							'conditions' => array('Validator.parent_id' => $job['Result']['id']),
							'order'      => 'Validator.name ASC',
							'recursive'  => -1,
						));
					}
					unset($job);
				}
			}
		}

		// If this is a Testsuite then we need some extra information
		$applications = array();
		if ($isTestsuite) {
			$versions = array();
			$jobs = Set::extract('/Request/Job', $gallery);
			$jobs = array_merge(Set::extract('/Request/Job', $children), $jobs);

			foreach ($jobs as $job) {
				if (empty($job['Job'])) { // When a testsuite request has no jobs yet
					continue;
				}

				if ($job['Job']['Format']['code'] != 'odf') {
					continue;
				}

				$application = $job['Job']['application_id'];
				if (!isset($versions[$application])) {
					$versions[$application] = $job['Job']['version'];
					continue;
				}

				if ($job['Job']['version'] > $versions[$application]) {
					$versions[$application] = $job['Job']['version'];
				}
			}
			unset($application); // Because we're referencing below

			$applications = $this->Request->Job->Application->find('all', array(
				'conditions' => array('Application.id' => array_keys($versions)),
				'order' => array('Application.name'),
				'recursive' => -1,
			));

			foreach ($applications as &$application) {
				$application['Application']['version'] = $versions[$application['Application']['id']];
			}
			unset($application); // Drop the reference
		}

		// Sort the subgalleries for display
		$gallery['children'] = $this->_sortThreaded($children);

		// Get the path to this gallery
		$path = $this->getpath($gallery['Gallery']['id']);
		array_pop($path);

		$this->log(sprintf('Gallery %s: generated viewData in %f seconds', $id, microtime(true) - $prof_start), LOG_DEBUG);
		return compact('gallery', 'applications', 'path', 'abbreviate', 'isTestsuite');
	}

	/**
	 * Sort an array of Galleries threaded, like find('threaded') would do
	 */
	private function _sortThreaded($results)
	{
		$return = $idMap = array();
		$ids = Set::extract($results, '{n}.Gallery.id');

		foreach ($results as $result) {
			$result['children'] = array();
			$id = $result['Gallery']['id'];
			$parentId = $result['Gallery']['parent_id'];
			if (isset($idMap[$id]['children'])) {
				$idMap[$id] = array_merge($result, (array)$idMap[$id]);
			} else {
				$idMap[$id] = array_merge($result, array('children' => array()));
			}
			if (!$parentId || !in_array($parentId, $ids)) {
				$return[] =& $idMap[$id];
			} else {
				$idMap[$parentId]['children'][] =& $idMap[$id];
			}
		}
		if (count($return) > 1) {
			$ids = array_unique(Set::extract('/Gallery/parent_id', $return));
			if (count($ids) > 1) {
				$root = $return[0]['Gallery']['parent_id'];
				foreach ($return as $key => $value) {
					if ($value['Gallery']['parent_id'] != $root) {
						unset($return[$key]);
					}
				}
			}
		}
		return $return;
	}

	/**
	 * Convert the Markdown description to HTML before saving
	 * @return boolean True to continue saving
	 */
	public function beforeSave()
	{
		if (isset($this->data['Gallery']['description'])) {
			// Convert the markdown description to HTML
			App::import('Vendor', 'markdown');
			App::import('Vendor', 'HTMLPurifier', array('file' => 'htmlpurifier/HTMLPurifier.standalone.php'));

			$config = HTMLPurifier_Config::createDefault();
			$config->set('Cache.SerializerPath', CACHE . DS . 'htmlpurifier');
			$purifier = new HTMLPurifier($config);

			$desc_html = Markdown($this->data['Gallery']['description']);
			$this->data['Gallery']['description_html'] = $purifier->purify($desc_html);

			// Save the markdown version to file
			$suite = $this->Testsuite->find('first', array(
				'conditions' => array('Testsuite.gallery_id' => $this->id),
				'recursive' => -1,
			));

			if ($suite) {
				$path = FILES . $suite['Testsuite']['root'] . DS . 'description.txt';
				file_put_contents($path, $this->data['Gallery']['description']);
			}
		}

		return true;
	}
}

?>
