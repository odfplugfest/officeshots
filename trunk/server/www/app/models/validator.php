<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

function startsWith($haystack, $needle)
{
     $length = strlen($needle);
     return (substr($haystack, 0, $length) === $needle);
}

/**
 * The Validator model
 *
 * This model is an ODF Validator, not to be confused with the Validation class used for validating model data
 */
class Validator extends AppModel
{
	const STATE_PENDING	= 1;
	const STATE_VALID	= 2;
	const STATE_INVALID	= 3;
	const STATE_ERROR	= 4;

	/** @var array Model behaviors */
	public $actsAs = array(
		'Containable',
		'BeanStalk.Deferrable' => array('priority' => 2048),
	);

	/**
	 * @var string The parent models.
	 * This is a bit of a hack. They both refer to the same field but because the ID's are UUID they will never collide
	 * and the right parent model should be picked automatically.
	 * */
	public $belongsTo = array(
		'Request' => array('foreignKey' => 'parent_id'),
		'Result' => array('foreignKey' => 'parent_id'),
	);

	/**
	 * Run the validator. Requires $this->id to be set.
	 */
	public function run()
	{
		$this->log('Validating ID: ' . $this->id, LOG_DEBUG);

		if (!$this->id) {
			return;
		}

		$this->read();
		if (!($validator = $this->_getValidator())) {
			$this->log('Could not get validator class');
			return;
		}

		$file = $this->_getFile();
		$validator->run($file);

		// Save the validator result to file
		$destination = $this->_getPath();
		$destination = FILES . $destination . DS . strtolower($this->data['Validator']['name']) . '-validator.' . $validator->ext;
		file_put_contents($destination, $validator->response);

		// Save the validator result to the satabase
		$this->save(array(
			'state'    => $validator->state,
			'response' => $validator->response,
		));
	}

	/**
	 * Delete the on-disk validator result
	 */
	public function beforeDelete($cascade)
	{
		if (!$this->id) {
			return false;
		}

		$this->read();
		if (!($validator = $this->_getValidator())) {
			return true;
		}

		if ($destination = $this->_getPath()) {
			$destination = FILES . $destination . DS . strtolower($this->data['Validator']['name']) . '-validator.' . $validator->ext;
			if (file_exists($destination)) {
				@unlink($destination);
			}
		}

		return true;
	}

	/**
	 * Get the validator class based off $this->data
	 */
	private function _getValidator()
	{
		$className = $this->data['Validator']['name'] . 'Validator';
		if (!$this->data['Validator']['name'] || !class_exists($className)) {
			$this->log('Invalid ODF Validator: ' . $this->data['Validator']['name'] . 'Validator');
			return null;
		}

		$validator = new $className();
		return $validator;
	}

	/**
	 * Get the Request.path or Result.path from $this->data
	 */
	private function _getPath()
	{
		if ($this->data['Request']['id']) {
			$this->Request->id = $this->data['Request']['id'];
			return $this->data['Request']['path'];
		}

		$this->Result->id = $this->data['Result']['id'];
		return $this->data['Result']['path'];
	}

	/**
	 * Get the Request file or Result file from $this->data
	 */
	private function _getFile()
	{
		if ($this->data['Request']['id']) {
			$this->Request->id = $this->data['Request']['id'];
			return $this->Request->getPath();
		}

		$this->Result->id = $this->data['Result']['id'];
		return $this->Result->getPath();
	}
}

/**
 * Validate an ODF document using the Cyclone3 stand-alone validator
 */
class CycloneValidator extends Object
{
	/** @var int The validator's state */
	public $state = Validator::STATE_PENDING;

	/** @var string The validator's full response */
	public $response = false;

	/** @var string Path to validator.pl */
	private $bin = false;

	/** @var string file extension for the response */
	public $ext = 'txt';

	/**
	 * Run the validator
	 * @var string $path Full path to the file to validate
	 */
	public function run($path)
	{
		// Make sure the validator is configured
		$this->bin = Configure::read('Validator.Cyclone');
		if (!$this->bin || !is_executable($this->bin)) {
			$this->state = Validator::STATE_ERROR;
			$this->response = sprintf('Cyclone validator "%s" not configured correctly.', $this->bin);
			$this->log($this->response);
			return;
		}

		// Make sure the file exists
		if (!is_readable($path)) {
			$this->state = Validator::STATE_ERROR;
			$this->response = sprintf('File %s cannot be read.', $file);
			$this->log($this->response);
			return;
		}

		// Run Cyclone validator
		$cwd = getcwd();
		chdir(dirname($this->bin));

		$spec = array(
			0 => array('pipe', 'r'), // STDIN
			1 => array('pipe', 'w'), // STDOUT
			2 => array('pipe', 'w')  // STDERR
		);

		$proc = proc_open($this->bin . ' "' . $path . '"', $spec, $pipes);
		if (!is_resource($proc)) {
			$this->state = Validator::STATE_ERROR;
			$this->response = sprintf('Cyclone validator "%s" failed to execute.', $this->bin);
			$this->log($this->response);
			return;
		}

		$stdout = stream_get_contents($pipes[1]);
		$stderr = stream_get_contents($pipes[2]);
		fclose($pipes[0]);
		fclose($pipes[1]);
		fclose($pipes[2]);
		proc_close($proc);

		chdir($cwd);

		// Parse the results
		if (strpos($stdout, 'Result: VALID') != false) {
			$this->state = Validator::STATE_VALID;
			$this->log(sprintf('Cyclone: Validated %s: Valid', $path), LOG_DEBUG);
		} elseif (strpos($stdout, 'Result: INVALID') !== false) {
			$this->state = Validator::STATE_INVALID;
			$this->log(sprintf('Cyclone: Validated %s: Invalid', $path), LOG_DEBUG);
		} else {
			$this->state = Validator::STATE_ERROR;
			$this->log(sprintf("Cyclone: Validated %s: Error\nSTDOUT: %s\nSTDERR: %s", $path, $stdout, $stderr));
		}

		$this->response = $stdout;
	}
}

/**
 * Validate an ODF document using the Office-o-tron online validator
 */
class OfficeotronValidator extends Object
{
	/** @var int The validator's state */
	public $state = Validator::STATE_PENDING;

	/** @var string The validator's full response */
	public $response = false;

	/** @var string file extension for the response */
	public $ext = 'html';

	/**
	 * Run the validator
	 * @var string $path Full path to the file to validate
	 */
	public function run($path)
	{
		// Make sure the validator is configured
		$endpoint = Configure::read('Validator.Officeotron');
		if (!$endpoint) {
			$this->state = Validator::STATE_ERROR;
			$this->response = sprintf('Officeotron validator "%s" not configured correctly.', $endpoint);
			$this->log($this->response);
			return;
		}

		// Make sure the file exists
		if (!is_readable($path)) {
			$this->state = Validator::STATE_ERROR;
			$this->response = sprintf('File %s cannot be read.', $file);
			$this->log($this->response);
			return;
		}

		// POST the file to Office-o-tron and get a response
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $endpoint);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, file_get_contents($path));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_TIMEOUT, 120);
		$this->response = curl_exec($curl);

		// No response, an error or a timeout
		if ($this->response === false) {
			$this->state = Validator::STATE_ERROR;
			$this->response = sprintf('Officeotron: Could not get a response from %s. Error %d: %s', $endpoint, curl_errno($curl), curl_error($curl));
			$this->log($this->response);
			return;
		}
		curl_close($curl);

		if (preg_match('/Grand total count of validity errors: (\d+)/', $this->response, $match)) {
			if ($match[1] == 0) {
				$this->state = Validator::STATE_VALID;
				$this->log(sprintf('Officeotron: Validated %s: Valid', $path), LOG_DEBUG);
			} else {
				$this->state = Validator::STATE_INVALID;
				$this->log(sprintf('Officeotron: Validated %s: Invalid', $path), LOG_DEBUG);
			}
		} else {
			$this->state = Validator::STATE_ERROR;
			$this->log(sprintf("Officeotron: Validated %s: Error:\n%s", $path, $this->response));
		}
	}
}

/**
 * A validator for Sun's ODFToolkit Validator.
 * NOTE: A bit ugly. It screen-scrapes the public online validator
 */
class ODFToolkitValidator extends Object
{
	/** @var int The validator's state */
	public $state = Validator::STATE_PENDING;

	/** @var string The validator's full response */
	public $response = false;

	/** @var string file extension for the response */
	public $ext = 'txt';

	/**
	 * Run the validator
	 *
	 * FIXME: as of 2015 this is old code, the validator should run locally,
	 *        I have kept this code around to preserve the option of a remote run too.
	 *
	 * @var string $path Full path to the file to validate
	 */
	public function runUsingRemote($path)
	{
		// Make sure the validator is configured
		$endpoint = Configure::read('Validator.ODFToolkit');
		if (!$endpoint) {
			$this->state = Validator::STATE_ERROR;
			$this->response = sprintf('ODFToolkit validator "%s" not configured correctly.', $endpoint);
			$this->log($this->response);
			return;
		}

		// Make sure the file exists
		if (!is_readable($path)) {
			$this->state = Validator::STATE_ERROR;
			$this->response = sprintf('File %s cannot be read.', $file);
			$this->log($this->response);
			return;
		}

		// POST the file to Office-o-tron and get a response
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $endpoint);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, array(
			'config' => 'auto',
			'mode' => 0,
			'loglevel' => 1,
//			'File1' => '@' . $path,
			'fileupload' => '@' . $path,
		));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_TIMEOUT, 120);
		$this->response = curl_exec($curl);

		// No response, an error or a timeout
		if ($this->response === false) {
			$this->state = Validator::STATE_ERROR;
			$this->response = sprintf('ODFToolkit: Could not get a response from %s. Error %d: %s', $endpoint, curl_errno($curl), curl_error($curl));
			$this->log($this->response);
			return;
		}
		curl_close($curl);

		// Parse the results
		if (strpos($this->response, 'This file is valid') != false) {
			$this->state = Validator::STATE_VALID;
			$this->response = 'This file is valid';
			$this->log(sprintf('ODFToolkit: Validated %s: Valid', $path), LOG_DEBUG);
		} elseif (strpos($this->response, 'This file is NOT valid') !== false) {
			$this->state = Validator::STATE_INVALID;
			preg_match('#<pre>(.*)</pre>#s', $this->response, $match);
			if (isset($match[1])) {
				$this->response = $match[1];
			}
			$this->log(sprintf('ODFToolkit: Validated %s: Invalid', $path), LOG_DEBUG);
		} else {
			$this->state = Validator::STATE_ERROR;
			$this->log(sprintf("ODFToolkit: Validated %s: Error\nResponse: %s", $path, $this->response));
		}
	}
	
	/**
	 * Run the validator locally on the OfficeShots machine.
	 *
	 * @var string $path Full path to the file to validate
	 */
	public function runUsingLocal($path)
	{
		// Make sure the validator is configured
		$this->bin = Configure::read('Validator.ODFToolkit');
		if (!$this->bin || !is_executable($this->bin)) {
			$this->state = Validator::STATE_ERROR;
			$this->response = sprintf('ODF validator "%s" not configured correctly.', $this->bin);
			$this->log($this->response);
			return;
		}

		// Make sure the file exists
		if (!is_readable($path)) {
			$this->state = Validator::STATE_ERROR;
			$this->response = sprintf('File %s cannot be read.', $file);
			$this->log($this->response);
			return;
		}

		// Run ODF validator
		$cwd = getcwd();
		chdir(dirname($this->bin));

		$spec = array(
			0 => array('pipe', 'r'), // STDIN
			1 => array('pipe', 'w'), // STDOUT
			2 => array('pipe', 'w')  // STDERR
		);

		$proc = proc_open($this->bin . ' "' . $path . '"', $spec, $pipes);
                $this->log(sprintf('ODF Validator: running %s on %s', $this->bin, $path), LOG_DEBUG);
		if (!is_resource($proc)) {
			$this->state = Validator::STATE_ERROR;
			$this->response = sprintf('ODF validator "%s" failed to execute.', $this->bin);
			$this->log($this->response);
			return;
		}

		$stdout = stream_get_contents($pipes[1]);
		$stderr = stream_get_contents($pipes[2]);
		fclose($pipes[0]);
		fclose($pipes[1]);
		fclose($pipes[2]);
		proc_close($proc);
		$this->log('ODF Validator: finished');
                $this->log(sprintf('ODF Validator: finished running %s on %s', $this->bin, $path), LOG_DEBUG);

		chdir($cwd);
		$expanded = explode("\n", $stdout);
		$finalline = array_pop( $expanded );
		if( !strlen($finalline)) {
		    $finalline = array_pop( $expanded );
		}
		
		// Parse the results
		if (strpos($finalline, 'no errors, no warnings') != false) {
			$this->state = Validator::STATE_VALID;
			$this->log(sprintf('ODF Validator: Validated %s: Valid', $path), LOG_DEBUG);
		} elseif (strpos($finalline, 'errors,') !== false) {
			$this->state = Validator::STATE_INVALID;
			$this->log(sprintf('ODF Validator: Validated %s: Invalid', $path), LOG_DEBUG);
		} else {
			$this->state = Validator::STATE_ERROR;
			$this->log(sprintf("ODF Validator: Validated %s: Error\nSTDOUT: %s\nSTDERR: %s", $path, $stdout, $stderr));
		}

		$this->response = $stdout;
	}
	
	public function run($path)
	{
		return $this->runUsingLocal($path);
	}
	
}



?>
