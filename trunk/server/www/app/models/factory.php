<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * The Factory model
 */
class Factory extends AppModel
{
	/** @var array A factory belongs to a user and runs a specific OS */
	public $belongsTo = array('User', 'Operatingsystem');

	/** @var array Every factory can have multiple workers (applications) installed and have multiple jobs and results assigned. */
	public $hasMany = array('Worker', 'Job', 'Result');

	/** @var array Factories are deeply nested, so use Containable */
	public $actsAs = array('Containable');

	/**
	 * Find a factory for a user ID and factory name
	 *
	 * @param string $user_id The user ID
	 * @param string $factory_name The name of the factory
	 * @return array The factory or null on failure
	 */
	public function findFactory($user_id, $factory)
	{
		$factory = $this->find('first', array(
			'conditions' => array(
				'Factory.user_id' => $user_id,
				'Factory.name' => $factory
			),
			'contain' => array(
				'Operatingsystem.id',
				'Operatingsystem.platform_id',
				'Worker',
				'Worker.Application.id',
				'Worker.Application.Doctype',
				'Worker.Doctype',
				'Worker.Format',
			),
		));

		if (!$factory) {
			return null;
		}

		return $factory;
	}

	/**
	 * Update the last_poll and active_since fields
	 */
	public function poll()
	{
		if (!$this->id) {
			return;
		}

		$update = (
			$this->field('active_since') == '0000-00-00 00:00:00'
			|| strtotime($this->field('last_poll')) < time() - Configure::read('Factory.polltime')
		);

		if ($update) {
			$this->saveField('active_since', date('Y-m-d H:i:s'));
		}

		$this->saveField('last_poll', date('Y-m-d H:i:s'));
	}
}

?>
