<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * The Testsuite  model
 */
class Testsuite extends AppModel
{
	/** @var string Requests can be assigned a specific format, and every format maps to one or more mimetypes */
	public $belongsTo = array('Gallery');

	/**
	 * Synchronise all testsuite files to the database
	 */
	public function synchronise($id = 'all')
	{
		if ($id == 'all') {
			$suites = $this->find('all');
		} else {
			$suites = $this->find('all', array(
				'conditions' => array('Testsuite.id' => $id),
			));
		}

		foreach ($suites as &$suite) {
			$this->log(sprintf('Synchronising testsuite "%s"', $suite['Testsuite']['name']), LOG_DEBUG);
			$this->_syncGallery($suite['Testsuite']['id']);
			$this->_syncSource($suite['Testsuite']['id']);
		}
	}

	/**
	 * Create or update the gallery belonging to the suite
	 * @param $suite_id The ID of the Testsuite
	 */
	private function _syncGallery($suite_id)
	{
		$this->id = $suite_id;
		$name     = $this->field('name');
		
		$this->Gallery->id = $this->field('gallery_id');
		$gallery = $this->Gallery->read();

		if ($gallery) {
			// Update the gallery
			if ($gallery['Gallery']['name'] != $name) {
				$this->Gallery->set('name', $name);
				$this->Gallery->save();
			}
		} else {
			// Create a new gallery
			$this->Gallery->create();
			$this->Gallery->set(array(
				'name' => $name,
				'user_id' => Configure::read('Testsuite.user_id'),
				'group_id' => Configure::read('Testsuite.group_id'),
			));
			$this->Gallery->save();

			$this->saveField('gallery_id', $this->Gallery->id);
			
			$this->log(sprintf('Created gallery "%s"', $name), LOG_DEBUG);
		}
	}

	/**
	 * Synchronise the files in the source directory
	 * Note: It only adds new files and directories, it does not remove them!
	 *
	 * @param $suite_id The ID of the Testsuite
	 */
	private function _syncSource($suite_id)
	{
		$this->id   = $suite_id;
		$source     = $this->field('source');
		$root       = $this->field('root');
		$gallery_id = $this->field('gallery_id');

		$this->_syncDirectory($source, $root, $gallery_id);
	}

	/**
	 * Synchronise a directory with a gallery
	 * @param string $path Path to the source directory
	 * @param string $root Root of the target directory
	 * @param int $gallery_id The Gallery that this directory is part of
	 */
	private function _syncDirectory($path, $root, $gallery_id)
	{
		$this->log(sprintf('Synchronising gallery %s (%s to %s)', $gallery_id, $path, $root), LOG_DEBUG);

		// Loop through all the subdirectories
		$directories = glob(FILES . $path . DS . '*', GLOB_NOSORT | GLOB_ONLYDIR);
		foreach ($directories as $directory) {
			// Skip testDoc directories in the Fellowship test suite
			if (is_dir($directory . DS . 'testDoc')) {
				$this->_syncDirectory($path . DS . basename($directory) . DS . 'testDoc', $root, $gallery_id);
				continue;
			}
			
			if (is_dir($directory . DS . 'testdoc')) {
				$this->_syncDirectory($path . DS . basename($directory) . DS . 'testdoc', $root, $gallery_id);
				continue;
			}

			// Skip directories that just have one file named "testDoc.*"
			$docs = glob($directory . DS . '*');
			if (sizeof($docs) == 1 && substr(basename($docs[0]), 0, 10) == 'testDoc.od') {
				$this->_syncDirectory($path . DS . basename($directory), $root, $gallery_id);
				continue;
			}

			// Skip Subversion (for security, because glob should not return hidden files normally)
			if (substr($directory, -3) == 'svn') {
				continue;
			}

			// See if a gallery exists
			$gallery_name = basename($directory);
			$gallery = $this->Gallery->find('first', array(
				'conditions' => array(
					'Gallery.parent_id' => $gallery_id,
					'Gallery.name' => $gallery_name,
				),
			));

			if ($gallery) {
				$this->Gallery->id = $gallery['Gallery']['id'];
			} else {
				$this->Gallery->create();
				$this->Gallery->set(array(
					'name' => $gallery_name,
					'parent_id' => $gallery_id,
					'user_id' => Configure::read('Testsuite.user_id'),
					'group_id' => Configure::read('Testsuite.group_id'),
				));
				$this->Gallery->save();
				
				$this->log(sprintf('Created gallery %s: "%s"', $this->Gallery->id, $gallery_name), LOG_DEBUG);
			}

			$this->_syncDirectory($path . DS . basename($directory), $root . DS . basename($directory), $this->Gallery->id);
		}

		// Loop through all the files in the directory
		$files = glob(FILES . $path . DS . '*', GLOB_NOSORT);
		foreach ($files as $file) {
			// Directories have been handled above
			if (is_dir($file)) {
				continue;
			}

			$this->_syncFile($file, $root, $gallery_id);
		}

		// If there are no requests or subgalleries, remove this gallery
		$subgalleryCount = $this->Gallery->childcount($gallery_id, true); // Use direct here. It comes from the database, not $data!
		$requestCount    = $this->Gallery->requestCount($gallery_id);

		if ($subgalleryCount == 0 && $requestCount == 0) {
			$this->Gallery->delete($gallery_id);
		}
	}

	/**
	 * Synchronise a file
	 * @param string $path Path to the source file
	 * @param string $root Root of the target directory
	 * @param int $gallery_id The Gallery that this file is part of
	 */
	private function _syncFile($path, $root, $gallery_id)
	{
		$this->log(sprintf('Synchronising %s to %s', $path, $root), LOG_DEBUG);

		$gallery = $this->Gallery->read(null, $gallery_id);

		// Rename the Fellowship testDoc documents to something more sensible
		$filename = basename($path);
		if (strtolower(substr($filename, 0, 8)) == 'testdoc.') {
			// Rename to parent directory. If that is testDoc too, rename to grandparent
			$filename  = dirname($path);
			if (strtolower(substr($filename, -7)) == 'testdoc') {
				$filename  = dirname($filename);
			}

			$filename  = basename($filename);
			$filename .= strrchr($path, '.');
		}

		// See if a request exists for this file
		$this->Gallery->Request->bindModel(array('hasOne' => array('GalleriesRequest')));
		$request = $this->Gallery->Request->find('first', array(
			'conditions' => array(
				'Request.filename' => $filename,
				'GalleriesRequest.gallery_id' => $gallery_id,
			),
		));

		if (!$request) {
			$stamp = date('Y-m-d H:i:s', filemtime($path));

			// Create a new request
			$this->Gallery->Request->create();
			$this->Gallery->Request->set(array(
				'user_id' => Configure::read('Testsuite.user_id'),
				'ip_address' => 0,
				'root' => $root . DS . $filename,
				'path' => $root . DS . $filename . DS . 'source',
				'priority' => 10,
				'state' => Request::STATE_PREPROCESSOR_QUEUED,
				'created' => $stamp,
				'modified' => $stamp,
			));
			$this->Gallery->Request->save();

			// Add the file to the request
			if (!$this->Gallery->Request->setFile($path, $filename)) {
				$error = implode("\n", $this->Gallery->Request->Behaviors->File->errors);
				$this->log('Adding file failed: ' . $error, LOG_DEBUG);
				$this->Gallery->Request->delete();
				return;
			}
			$this->Gallery->Request->save();

			// Add Validators to the request
			$this->Gallery->Request->addValidators();

			// Schedule the preprocessor for the request
			if (!$this->Gallery->Request->defer('run', 'Preprocessor')) {
				$this->log('Failed to queue the preprocessor for request ' . $this->Gallery->Request->id);
			}
			
			// Add the request to the gallery
			$this->Gallery->addRequest($this->Gallery->Request->id, $gallery_id);

			// Done
			$this->log(sprintf('Created request "%s"', $root . DS . $filename), LOG_DEBUG);
			return;
		}
		
		// See if the request needs updating
		if (strtotime($request['Request']['modified']) < filemtime($path)) {
			$this->Gallery->Request->id = $request['Request']['id'];
			$this->Gallery->Request->deleteJobs();
			$this->Gallery->Request->setFile($path);
			$this->Gallery->Request->set(array(
				'modified' => filemtime($path),
				'state' => Request::STATE_PREPROCESSOR_QUEUED,
			));
			$this->Gallery->Request->save();
			
			// Add Validators to the request
			$this->Gallery->Request->addValidators();

			// Schedule the preprocessor for the request
			if (!$this->Gallery->Request->defer('run', 'Preprocessor')) {
				$this->log('Failed to queue the preprocessor for request ' . $this->Gallery->Request->id);
			}
			
			$this->log(sprintf('Updated request %s "%s"', $request['Request']['id'], $filename), LOG_DEBUG);
		}
	}

	/**
	 * Add jobs to all the test suite requests for all stable factories
	 */
	public function addJobs()
	{
		// Load the necessary models
		$this->Worker = ClassRegistry::init('Worker');
		$this->Request = ClassRegistry::init('Request');

		$workers = $this->Worker->getActive(true);
		$requests = $this->getRequests(false, array('Job', 'Mimetype'));

		foreach ($requests as $request) {
			$this->_addJobs($request, $workers);
		}
	}

	/**
	 * Get all requests and jobs that belong to all test suites
	 */
	public function getRequests($id = false, $contain = array())
	{
		$requests = array();
		$options = array('recursive' => -1);

		if ($id) {
			$options['conditions'] = array('Testsuite.id' => $id);
		}

		$suites = $this->find('all', $options);
		foreach ($suites as $suite) {
			if (!$suite['Testsuite']['gallery_id']) {
				continue;
			}

			$requests = array_merge(
				$requests,
				$this->Gallery->getRequests($suite['Testsuite']['gallery_id'], $contain)
			);
		}

		return $requests;
	}

	/**
	 * Add jobs for $workers to a single request
	 * @param array $workers An array of available workers
	 * @param array $request The request to add the jobs to
	 */
	private function _addJobs($request, $workers)
	{
		$newJobs = array();
		foreach ($workers as $worker) {
			// If the worker does not support this doctype, skip it.
			if ($request['Mimetype']['doctype_id'] != $worker['Doctype']['id']) {
				continue;
			}

			foreach ($worker['Format'] as $format) {
				$match = false;

				foreach ($request['Job'] as $job) {
					$match = (
						$job['platform_id'] == $worker['Platform']['id']
						&& $job['application_id'] == $worker['Application']['id']
						&& $job['version'] == $worker['Worker']['version']
						&& $job['format_id'] == $format['id']
					);

					// If a job already exists we're done checking
					if ($match) {
						break;
					}
				}

				if ($match) {
					continue;
				}

				// If we get here, no suitable job exists and we need to add one
				$newJobs[] = array(
					'platform_id' => $worker['Platform']['id'],
					'application_id' => $worker['Application']['id'],
					'version' => $worker['Worker']['version'],
					'format_id' => $format['id'],
				);
			}
		}
		
		$this->Request->id = $request['Request']['id'];
		$this->Request->addJobs($newJobs);
	}

	/**
	 * Regenerate the cached data for all the test suite gallery views
	 */
	public function recache()
	{
		$doAllGal = 1;
		
		$suites = $this->find('all', array('recursive' => -1));
		$this->log('Caching view data for ' . sizeof($suites) . ' testsuites', LOG_DEBUG);

		foreach ($suites as $suite) {
			if (!$suite['Testsuite']['gallery_id']) {
				continue;
			}

			$gid = $suite['Testsuite']['gallery_id'];
			if( $doAllGal ) {

				// Cache the view data for the top-level view
				$data = $this->Gallery->viewData($suite['Testsuite']['gallery_id']);
				Cache::write('gallery/' . $suite['Testsuite']['gallery_id'], $data);

	            if ($data['isTestsuite'])
        	    {
	                $gc = new GalleryCacheCreator();
        	        extract($data);
                	$txt = $gc->getCachedTableString( $gallery, $applications );
	                $gallery_cache_dir = "/var/www/www.officeshots.org/trunk/www/app/tmp/cache/galleries/";
	                file_put_contents ( $gallery_cache_dir . "/" . $gid . ".php", $txt );
	                
	            }
	            
/*	
                if( $gid == 106 ) {
                    $hr = print_r( $data, true );
                    file_put_contents ( "/tmp/gal106", $hr );
                }
*/
                $cachekey = 'gallery/' . $suite['Testsuite']['gallery_id'];
                Cache::write($cachekey,$data,'gallery');
                $start_time = microtime(TRUE);
                $rd = Cache::read($cachekey,'gallery');
                $end_time = microtime(TRUE);
                $dt = $end_time - $start_time;
                $this->log('reading back using gallery cache took ' . $dt, LOG_DEBUG);
                $rc = Cache::write($cachekey,$data,'gallerymem');
                $start_time = microtime(TRUE);
                $rd = Cache::read($cachekey,'gallerymem');
                $end_time = microtime(TRUE);
                $dt = $end_time - $start_time;
                $this->log('key ' . $cachekey . ' write rc:' . $rc . ' reading back using gallerymem cache took ' . $dt, LOG_DEBUG);



                // Get all subgalleries
                $gallery = $this->Gallery->find('first', array(
                    'conditions' => array('Gallery.id' => $suite['Testsuite']['gallery_id']),
                    'recursive' => -1,
                ));
			
                $subgalleries = $this->Gallery->find('all', array(
                    'conditions' => array(
                        'Gallery.lft >' => $gallery['Gallery']['lft'],
                        'Gallery.rght <' => $gallery['Gallery']['rght'],
                    ),
                    'recursive' => -1,
                ));
            }

            if( $doAllGal ) {
                // Cache the data for all the subgallery views
                foreach ($subgalleries as $subgallery) {
                    $this->log('processing subgallery ' . $subgallery['Gallery']['id'], LOG_DEBUG);
                    $data = $this->Gallery->viewData($subgallery['Gallery']['id']);
                    $cachekey = 'gallery/' . $subgallery['Gallery']['id'];
                    Cache::write('gallery/' . $subgallery['Gallery']['id'], $data);

                    $start_time = microtime(TRUE);
                    $rd = Cache::read($cachekey);
                    $end_time = microtime(TRUE);
                    $dt = $end_time - $start_time;
                    $this->log('key ' . $cachekey
                               . ' reading back using normal cache took ' . $dt,
                               LOG_DEBUG);

                    $rc = Cache::write($cachekey,$data,'gallerymem');
                    $start_time = microtime(TRUE);
                    $rd = Cache::read($cachekey,'gallerymem');
                    $end_time = microtime(TRUE);
                    $dt = $end_time - $start_time;
                    $this->log('key ' . $cachekey
                               . ' write rc:' . $rc . ' reading back using gallerymem cache took ' . $dt,
                               LOG_DEBUG);
                }
            }
        }

		return true;
	}
}

?>
