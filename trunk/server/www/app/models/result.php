<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

App::import('Behavior', 'Pipeline');

/**
 * The Result model
 */
class Result extends AppModel
{
	/**#@+
	 * Request states
	 */
	const STATE_UPLOADING            =  1;
	const STATE_POSTPROCESSOR_QUEUED =  2;
	const STATE_SCAN_FOUND           =  4;
	const STATE_POSTPROCESSOR_FAILED =  8;
	const STATE_FINISHED             = 16;
	/**#@-*/
	
	/**#@+
	 * Manual verification states
	 */
	const VERIFY_PENDING = 0;
	const VERIFY_PASS    = 1;
	const VERIFY_FAIL    = 2;
	/**#@-*/

	/** @var array A result belongs to a job and a factory that created it */
	public $belongsTo = array('Factory', 'Format');

	/**
	 * @var array A result is always associated with a job.
	 *
	 * This looks backwards but otherwise it is impossible to find Jobs that have no Result yet.
	 */
	public $hasOne = array('Job');

	/**
	 * A result can have multiple ODF validators
	 */
	public $hasMany = array('Validator' => array('foreignKey' => 'parent_id', 'dependent' => true));

	/** @var array The result is a file */
	public $actsAs = array(
		'Containable',
		'BeanStalk.Deferrable',
		'File',
		'Pipeline' => 'Postprocessor',
	);

	/** @var string Use the filename as the distinguising name */
	public $displayField = 'filename';

	/**
	 * Constructor
	 * Build the pipelines
	 */
	public function __construct()
	{
		parent::__construct();

		$this->Postprocessor->callback('scan');
		$this->Postprocessor->callback('validateFile');
		$this->Postprocessor->callback('saveField', 'state', self::STATE_FINISHED);
		$this->Postprocessor->errback('saveField', 'state', self::STATE_POSTPROCESSOR_FAILED);
	}

	/**
	 * Check access control for this result
	 * @param string $user_id The user ID
	 * @param string $type "read" or "write"
	 * @param string $id The result ID
	 * @return boolean True or False
	 */
	public function checkAccess($user_id, $type = 'read', $id = false)
	{
		if (!$id) {
			$id = $this->id;
		}

		if (!$id) {
			return false;
		}

		if ($type == 'admin') {
			return true;
		}

		$result = $this->find('first', array(
			'contain' => array('Job', 'Job.Request'),
			'conditions' => array('Result.id' => $id),
		));

		if (!empty($result)) {
			return $this->Job->Request->checkAccess($user_id, $type, $result['Job']['Request']['id']);
		}

		return false;
	}

	/**
	 * Add an upload to the result
	 *
	 * If the add fails, &$errors contains the error messages
	 *
	 * @param array $data The data to save if not saving $this->data, including a file and the jobs
	 * @param array &$errors The errors
	 * @return boolean Success
	 */
	public function addUpload($data, &$errors = array())
	{
		if (empty($data)) {
			$errors[] = __('The result was empty.', true);
			return false;
		}

		// Check that we have a file and jobs
		if ((!isset($data['Result']['FileUpload']) || !is_array($data['Result']['FileUpload']))) {
			$errors[] = __('You did not submit a file.', true);
			return false;
		}

		if (!$this->setFileUpload($data['Result']['FileUpload'])) {
			$errors[] = __('The file upload failed. Please try again.', true);
			$errors = array_merge($errors, $this->Behaviors->File->errors);
			return false;
		}

		return true;
	}

	/**
	 * When deleting a result, find the Job that has it's ID and clear it.
	 * Also remove the files from the filesystem
	 */
	public function beforeDelete()
	{
		$data = $this->read();
		$this->Job->id = $data['Job']['id'];
		$this->Job->saveField('result_id', '');

		$path = $this->field('path');
		if ($path) {
			$Folder = new Folder(FILES . $path);
			$Folder->delete();
		}

		return true;
	}

	/**
	 * Convert the Markdown description to HTML before saving
	 * @return boolean True to continue saving
	 */
	public function beforeSave()
	{
		if (isset($this->data['Result']['description'])) {
			// Convert the description to HTML
			App::import('Vendor', 'markdown');
			App::import('Vendor', 'HTMLPurifier', array('file' => 'htmlpurifier/HTMLPurifier.standalone.php'));

			$config = HTMLPurifier_Config::createDefault();
			$config->set('Cache.SerializerPath', CACHE . DS . 'htmlpurifier');
			$purifier = new HTMLPurifier($config);

			$desc_html = Markdown($this->data['Result']['description']);
			$this->data['Result']['description_html'] = $purifier->purify($desc_html);

			// Save the Markdown version to file
			$path = FILES . $this->field('path') . DS . 'description.txt';
			file_put_contents($path, $this->data['Result']['description']);
		}

		return true;
	}

	/**
	 * After creating a new result, schedule it for scanning
	 */
	public function afterSave($created)
	{
		$this->read();
		if ($created === false || empty($this->data['Result']['path'])) {
			return;
		}
                //$this->log(__FILE__.':'.__LINE__.': '. print_r($this->data, true));
		
		// Create validators for this result
		if ($this->data['Format']['code'] == 'odf') {
			$validators = Configure::read('Validator');
			foreach ($validators as $validator_name => $validator_config) {
				if (!$validator_config) {
					continue;
				}

				$this->Validator->create();
				$this->Validator->save(array('Validator' => array(
					'name' => $validator_name,
					'parent_id' => $this->id,
				)));
			}
		}

		// Schedule the postprocessor to run
		if (!$this->defer('run', 'Postprocessor')) {
			$this->log('Failed to queue the result for the virus scanner.', true);
		}

		$this->saveField('state', self::STATE_POSTPROCESSOR_QUEUED);
	}

	/**
	 * Run ClamAV on the file
	 */
	public function scan()
	{
		$clamd_config = Configure::read('Clamd');
		if (!$clamd_config) {
			return true; // Continue with the rest of the pipeline
		}

		$clamd = new Clamd($clamd_config);
		$path = $this->getPath();
		$result = '';

		if (!$path) {
			$this->log('Result could not be scanned. ' . $path . ' (Result ID: ' . $this->id . ') does not exists. ');
			return false;
		}

		$status = $clamd->scan($path, $result);
		if ($status == Clamd::OK) {
			// Scan OK.
			$this->log('Clamd scanned ' . $path . ' (Result ID: ' . $this->id . '): OK', LOG_DEBUG);
		} elseif ( $status == Clamd::FOUND ) {
			// Note that the file is *not* deleted. This was we can later check if there really was a virus
			$this->data['Result']['state'] = self::STATE_SCAN_FOUND;
			$this->data['Result']['state_info'] = $result;
			$this->log('Clamd scanned ' . $path . ' (Result ID: ' . $this->id . '): FOUND ' . $result, LOG_DEBUG);
			return false;
		} else {
			// There was an error.
			if ($status === false) {
				$result = $clamd->lastError();
				$status = Clamd::ERROR;
			}

			$this->data['Result']['state_info'] = $result;
			$this->log('Clamd error scanning ' . $path . ' (Result ID: ' . $this->id . '): ' . $result);
			return false;
		}

		return $this->save();
	}

	/**
	 * Run all the ODF validators associated with this request
	 */
	public function validateFile()
	{
		$this->read();
		if (!is_array($this->data['Validator'])) {
			$this->log('No validators found for Result: ' . $this->id, LOG_DEBUG);
			return true;
		}

		foreach ($this->data['Validator'] as $validator) {
			$this->Validator->id = $validator['id'];
			$this->Validator->run();
		}

		return true; // To continue the rest of the pipeline
	}
}

?>
