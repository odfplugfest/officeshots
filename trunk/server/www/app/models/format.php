<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * The Fomat model
 *
 * A format is a certain output format supported by the office application
 */
class Format extends AppModel
{
	/** @var array Many-to-many relation to workers */
	public $hasAndBelongsToMany = array(
		'Worker' => array('unqiue' => true)
	);

	/** @var string Requests can be assigned a specific format, and every format maps to one or more mimetypes */
	public $hasMany = array('Mimetype');

	/**
	 * Return an array containing all format codes
	 */
	public function codes()
	{
		$list = $this->find('all', array('recursive' => -1));
		return Set::extract('/Format/code', $list);
	}
}

?>
