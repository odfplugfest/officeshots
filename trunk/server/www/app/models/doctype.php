<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * The Doctype model. A doctype is a certain type of ODF file, such as Text, Spreadsheet or Presentation
 */
class Doctype extends AppModel
{
	/** @var string Every application supports a doctype */
	public $hasAndBelongsToMany = array('Application', 'Worker');

	/** @var array Multiple Mimetypes can be associated with a single Doctype */
	public $hasMany = array('Mimetype');

	/** @var array The Doctypes should be ordered for display on the front page */
	public $actsAs = array('Ordered' => array('field' => 'order', 'foreign_key' => false));
}

?>
