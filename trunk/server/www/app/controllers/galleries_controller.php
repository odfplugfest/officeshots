<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * The galleries controller
 */
class GalleriesController extends AppController
{
	/** @var array The components this controller uses */
	public $components = array('AuthCert');
	
	/** The controller helpers */
	public $helpers = array('Html', 'Form', 'Javascript', 'RequestModel', 'JobModel', 'ValidatorModel');

	/** The models this controller uses */
	public $uses = array('Gallery', 'Request', 'User', 'Application');

	/**
	 * Set the auth permissions for this controller
	 * @return void
	 */
	public function beforeFilter()
	{
		parent::beforeFilter();
		$this->AuthCert->allow('index', 'view');
	}

	/**
	 * Check if the current user has write access to the gallery
	 * @param string $slug The unique gallery slug
	 */
	private function _checkAccess($slug)
	{
		$gallery = $this->Gallery->find('first', array(
			'conditions' => array('Gallery.slug' => $slug),
			'recursive' => -1,
		));

		// Gallery does not exist
		if (empty($gallery)) {
			return false;
		}

		// User owns the gallery
		if ($gallery['Gallery']['user_id'] == $this->AuthCert->user('id')) {
			return true;
		}

		// Gallery is assigned to a group the user is a member of
		if (!empty($gallery['Gallery']['group_id'])) {
			return $this->User->Group->has_member($this->AuthCert->user('id'), $gallery['Gallery']['group_id']);
		}

		return false;
	}

	/**
	 * Show a list of all galleries
	 */
	public function index()
	{
		$this->Gallery->contain(array('User', 'Request'));
		$galleries = $this->paginate('Gallery', array('Gallery.parent_id' => null));

		foreach ($galleries as &$gallery) {
			$gallery['Gallery']['num_documents'] = $this->Gallery->requestCount($gallery['Gallery']['id'], true);
		}

		$this->set('galleries', $galleries);
	}


	/**
	 * View a gallery
	 * @param string $slug The gallery slug
	 */
	public function viewold($slug = null)
	{
		if (!$slug) {
			$this->Session->setFlash(__('Invalid Gallery.', true));
			$this->redirect(array('action'=>'index'));
		}

		// Get the gallery
		$gallery = $this->Gallery->find('first', array(
			'conditions' => array('Gallery.slug' => $slug),
			'recursive' => -1,
		));


		$cachekey = 'gallery/' . $gallery['Gallery']['id'];
                $cachekey = 'gallery/' . $gallery['Gallery']['id'];
		if (isset($this->passedArgs['cache']) && $this->passedArgs['cache'] == 'false') {
                        $this->log('cache forced off for cachekey ' . $cachekey, LOG_DEBUG);
			$data = false;
		} else {
                        $start_time = microtime(TRUE);
			$data = Cache::read('gallery/' . $gallery['Gallery']['id']);
                        $end_time = microtime(TRUE);
                        $dt = $end_time - $start_time;
                        $this->log('gallery render, reading back using oldcache took ' . $dt, LOG_DEBUG);
		}

		if ($data === false) {
			$gid = $gallery['Gallery']['id'];
                        $this->log('didnt have a cache for gid ' . $gid . ' so starting from scratch...', LOG_DEBUG);
			$data = $this->Gallery->viewData($gallery['Gallery']['id']);
			$rc = 'no write';
                        $this->log('didnt have a cache for gid ' . $gid . ' so writing it rc:' . $rc, LOG_DEBUG);
		}

		// Don't use the cached gallery descriptions
		$data['gallery']['Gallery']['description_html'] = $gallery['Gallery']['description_html'];

		// Check access
		$access = $this->_checkAccess($slug);
		$data = array_merge($data, compact('access'));
		$this->set($data);

		// Render
		if ($data['isTestsuite']) {
			$this->render('testsuite');
		}
		else {
			$this->render('view');
		}
	}

	/**
	 * View a gallery
	 * @param string $slug The gallery slug
	 */
	public function view($slug = null)
	{
		if (!$slug) {
			$this->Session->setFlash(__('Invalid Gallery.', true));
			$this->redirect(array('action'=>'index'));
		}

		// Get the gallery
		$gallery = $this->Gallery->find('first', array(
			'conditions' => array('Gallery.slug' => $slug),
			'recursive' => -1,
		));

                //
                // If there is a precached view then try to use that if we can
                //
                $gid = $gallery['Gallery']['id'];
                $cacheBasePath = '/var/www/www.officeshots.org/trunk/www/app/tmp/cache/galleries/';
                $cachePath = $cacheBasePath . $gid . ".php";

                $this->log('cachePath ' . $cachePath, LOG_DEBUG);
                $this->log('exists    ' . file_exists ( $cachePath ), LOG_DEBUG);
                $this->log('exists    ' . file_exists ( $cachePath ), LOG_DEBUG);
        
		// At the moment, a cache is only generated if ($data['isTestsuite'])
                if( preg_match("/[0-9]+/i", $gid)
                    && file_exists ( $cachePath )) 
                {
		    $data = false;
                    $this->log('using cache!    ' . $cachePath, LOG_DEBUG);
                    $this->log('gid             ' . $gid, LOG_DEBUG);
                    $cachegid = $gid;
                    $access = $this->_checkAccess($slug);
                    $viewusesbootstrap = '1';
                    $viewusescontentid = 'contentnoscroll';
                    $data = array_merge( compact('access'), compact('gid'), compact('cachegid'),
                                         compact('viewusesbootstrap'), compact('viewusescontentid') );
                    $this->set($data);
                    $this->render('viewcache');
                    return;
                }
        
     
		if (isset($this->passedArgs['cache']) && $this->passedArgs['cache'] == 'false') {
			$data = false;
		} else {
			$data = Cache::read('gallery/' . $gallery['Gallery']['id']);
		}

		if ($data === false) {
			$data = $this->Gallery->viewData($gallery['Gallery']['id']);
		}

		// Don't use the cached gallery descriptions
		$data['gallery']['Gallery']['description_html'] = $gallery['Gallery']['description_html'];

		// Check access
		$access = $this->_checkAccess($slug);
		$data = array_merge($data, compact('access'));
		$this->set($data);

		// Render
		if ($data['isTestsuite']) {
			$this->render('testsuite');
		}
		else {
			$this->render('view');
                }
    }


	/**
	 * Add a new gallery
	 */
	public function add()
	{
		if (!empty($this->data)) {
			$this->data['Gallery']['user_id'] = $this->AuthCert->user('id');

			$this->Gallery->create();
			if ($this->Gallery->save($this->data)) {
				$this->Session->setFlash(__('The Gallery has been saved', true));
				$gallery = $this->Gallery->read('slug', $this->Gallery->id);
				$this->redirect(array('action' => 'view', $gallery['Gallery']['slug']));
			} else {
				$this->Session->setFlash(__('The Gallery could not be saved. Please, try again.', true));
			}
		}

		$groups = $this->User->find('first', array(
			'contain' => array('Group'),
			'conditions' => array('User.id' => $this->AuthCert->user('id')),
		));

		$groups = array_combine(
			Set::extract('/Group/id', $groups),
			Set::extract('/Group/name', $groups)
		);

		$this->set(compact('groups'));
		$this->render('edit');
	}

	/**
	 * Edit a gallery
	 * @param string $slug the unique gallery slug
	 */
	public function edit($slug = null)
	{
		if (!empty($this->data)) {
			$slug = $this->Gallery->read('slug', $this->data['Gallery']['id']);
			$slug = $slug['Gallery']['slug'];
		}

		if (!$slug && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Gallery', true));
			$this->redirect(array('action' => 'index'));
		}

		if (!$this->_checkAccess($slug)) {
			$this->Session->setFlash(__('You are not allowed to edit this gallery', true));
			$this->redirect(array('action' => 'view', $slug));
		}

		if (!empty($this->data)) {
			if ($this->Gallery->save($this->data)) {
				$this->Session->setFlash(__('The Gallery has been saved', true));
				$this->redirect(array('action' => 'view', $slug));
			} else {
				$this->Session->setFlash(__('The Gallery could not be saved. Please, try again.', true));
			}
		} else {
			$this->data = $this->Gallery->find('first', array(
				'conditions' => array('Gallery.slug' => $slug),
				'recursive' => -1,
			));
		}

		$groups = $this->User->find('first', array(
			'contain' => array('Group'),
			'conditions' => array('User.id' => $this->AuthCert->user('id')),
		));

		$groups = array_combine(
			Set::extract('/Group/id', $groups),
			Set::extract('/Group/name', $groups)
		);

		$this->set(compact('groups'));
	}

	/**
	 * Delete a gallery
	 * @param string $slug the unique gallery slug
	 */
	public function delete($slug = null)
	{
		if (!$slug) {
			$this->Session->setFlash(__('Invalid Gallery', true));
			$this->redirect(array('action'=>'index'));
		}

		if (!$this->_checkAccess($slug)) {
			$this->Session->setFlash(__('You are not allowed to delete this gallery', true));
			$this->redirect(array('action' => 'view', $slug));
		}

		$gallery = $this->Gallery->find('first', array(
			'recursive' => -1,
			'conditions' => array('Gallery.slug' => $slug),
		));

		if ($this->Gallery->del($gallery['Gallery']['id'])) {
			$this->Session->setFlash(__('Gallery deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

	/**
	 * Add a document to the gallery
	 * @param string $slug the unique gallery slug
	 */
	public function add_document($slug)
	{
		if (!$this->_checkAccess($slug)) {
			$this->Session->setFlash(__('You are not allowed to edit this gallery', true));
			$this->redirect(array('action' => 'view', $slug));
		}

		if (!empty($this->data)) {
			$gallery = $this->Gallery->find('first', array(
				'conditions' => array('Gallery.slug' => $slug),
				'recursive' => -1,
			));

			if (!$this->Gallery->addRequest(array_shift($this->data['Gallery']['requests']), $gallery['Gallery']['id'])) {
				$this->Session->setFlash(__('The document could not be added.', true));
			}
		
			$this->redirect(array('action' => 'view', $slug));
		}

		$tmp_requests = $this->Request->find('all', array(
			'conditions' => array('Request.user_id' => $this->AuthCert->user('id')),
			'order' => 'Request.created DESC',
			'recursive' => -1,
		));

		$requests = array();
		foreach ($tmp_requests as $request) {
			$requests[$request['Request']['id']] = $request['Request']['created'] . ': '
				. $request['Request']['filename']
				. ' (' . $request['Request']['result_count']
				. '/'  . $request['Request']['job_count'] . ')';
		}

		$this->set(compact('slug', 'requests'));
	}

	/**
	 * Remove a document from a gallery
	 * @param string $slug the unique gallery slug
	 * @param string $request_id The request ID
	 */
	public function remove_document($slug, $request_id)
	{
		if (!$this->_checkAccess($slug)) {
			$this->Session->setFlash(__('You are not allowed to edit this gallery', true));
			$this->redirect(array('action' => 'view', $slug));
		}

		$gallery = $this->Gallery->find('first', array(
			'conditions' => array('Gallery.slug' => $slug),
			'recursive' => -1,
		));

		if (!$this->Gallery->removeRequest($request_id, $gallery['Gallery']['id'])) {
			$this->Session->setFlash(__('The document could not be removed.', true));
		}
		
		$this->redirect(array('action' => 'view', $slug));
	}
}

?>
