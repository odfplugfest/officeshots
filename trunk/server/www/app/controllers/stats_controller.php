<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

App::import('Core', 'BeanStalk.BeanStalkManager');

/**
 * Controller for the statistics pages
 */
class StatsController extends AppController
{
	/** @var array The components this controller uses */
	public $components = array('AuthCert');
	
	/** @var array The helpers that will be available on the view */
	public $helpers = array('Html');

	public $uses = array();

	/**
	 * Set the auth permissions for this controller
	 * @return void
	 */
	public function beforeFilter()
	{
		parent::beforeFilter();
		$this->AuthCert->allow('index');
	}

	public function index()
	{
		$stats = false;
		$beanstalk = BeanStalkManager::getBeanStalk();
		$beanstalk->stats($stats);

		$this->set(array(
			'stats' => $stats
		));
	}
}

?>
