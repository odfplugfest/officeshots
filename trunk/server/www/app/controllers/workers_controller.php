<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * The workers controller
 */
class WorkersController extends AppController
{
	/** @var array The helpers that will be available on the view */
	public $helpers = array('Html', 'Form', 'Javascript');

	/** @var array The components this controller uses */
	public $components = array('AuthCert');

	/**
	 * Redirect worker index to factory index
	 * @return void
	 */
	public function index()
	{
		$this->redirect(array('controller' => 'factories', 'action'=>'index'));
	}

	/**
	 * Filter unsupported doctypes from $this->data
	 * @param boolean $flash Add a flash error message when doctypes are all invalid
	 * @return true if the doctypes are valid, false if not
	 */
	private function _filterDoctypes($flash = true)
	{
		$application = $this->Worker->Application->find('first', array(
			'conditions' => array('Application.id' => $this->data['Worker']['application_id']),
			'contain' => array('Doctype'),
		));
		$doctypes = Set::extract('/Doctype/id', $application);

		if (!empty($this->data['Doctype']['Doctype'])) {
			foreach ($this->data['Doctype']['Doctype'] as $index => $doctype_id) {
				if (!in_array($doctype_id, $doctypes)) {
					unset($this->data['Doctype']['Doctype'][$index]);
				}
			}
		}

		if (!empty($this->data['Doctype']['Doctype'])) {
			return true;
		}

		if ($flash) {
			$this->Session->setFlash(sprintf(
				__('You did not select any valid document types for application "%s". Please, try again.', true),
				$application['Application']['name']
			));
		}

		return false;
	}

	/**
	 * Generate a parsable list of doctypes per application
	 */
	private function _applicationDoctypes()
	{
		$applications = $this->Worker->Application->find('all', array(
			'contain' => array('Doctype'),
		));

		$return = array();
		foreach ($applications as $application) {
			$return[$application['Application']['id']] = Set::extract('/Doctype/id', $application);
		}

		return $return;
	}

	/**
	 * Add a new worker to a factory
	 * @param string $factory_id The factory ID to add the worker to
	 * @return void
	 */
	public function add($factory_id = null)
	{
		if ($factory_id == null && !empty($this->data)) {
			$factory_id = $this->data['Worker']['factory_id'];
		}

		if (!$factory_id) {
			$this->Session->setFlash(__('Invalid Factory ID', true));
			$this->redirect(array('controller' => 'factories', 'action'=>'index'));
		}

		$this->Worker->Factory->id = $factory_id;
		if ($this->Worker->Factory->field('user_id') != $this->AuthCert->user('id')) {
			$this->Session->setFlash(__('Invalid Factory', true));
			$this->redirect(array('controller' => 'factories', 'action'=>'index'));
		}

		if (!empty($this->data)) {
			if ($this->_filterDoctypes()) {
				$this->Worker->create();
				if ($this->Worker->save($this->data)) {
					$this->Session->setFlash(__('The Worker has been saved', true));
					$this->redirect(array('controller' => 'factories', 'action'=>'view', $factory_id));
				} else {
					$this->Session->setFlash(__('The Worker could not be saved. Please, try again.', true));
				}
			}
		}

		$this->data['Worker']['factory_id'] = $factory_id;
		$this->data['Factory']['name'] = $this->Worker->Factory->field('name');

		$doctypes = $this->Worker->Doctype->find('list');
		$formats = $this->Worker->Format->find('list');
		$applications = $this->Worker->Application->find('list');
		$applicationDoctypes = $this->_applicationDoctypes();

		$this->set(compact('doctypes', 'formats', 'applications', 'applicationDoctypes'));

		$this->render('edit');
	}


	/**
	 * Edit a worker
	 * @param string $id The worker ID
	 * @return void
	 */
	public function edit($id = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid application for this factory', true));
			$this->redirect(array('action'=>'index'));
		}

		$this->Worker->id = $id;
		$this->Worker->Factory->id = $this->Worker->field('factory_id');
		if ($this->Worker->Factory->field('user_id') != $this->AuthCert->user('id')) {
			$this->Session->setFlash(__('Invalid factory', true));
			$this->redirect(array('controller' => 'factories', 'action'=>'index'));
		}

		if (!empty($this->data)) {
			if ($this->_filterDoctypes()) {
				if ($this->Worker->save($this->data)) {
					$this->Session->setFlash(__('The application has been saved', true));
					$this->redirect(array('controller' => 'factories', 'action'=>'view', $this->Worker->Factory->id));
				} else {
					$this->Session->setFlash(__('The application could not be saved. Please, try again.', true));
				}
			}
		} else {
			$this->data = $this->Worker->read(null, $id);
		}

		$doctypes = $this->Worker->Doctype->find('list');
		$formats = $this->Worker->Format->find('list');
		$applications = $this->Worker->Application->find('list');
		$applicationDoctypes = $this->_applicationDoctypes();

		$this->set(compact('doctypes', 'formats', 'applications', 'applicationDoctypes'));
	}

	/**
	 * Delete a worker
	 *
	 * @param string $id The worker ID
	 * @return void
	 */
	public function delete($id = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid application id', true));
			$this->redirect(array('action'=>'index'));
		}

		$this->Worker->id = $id;
		$this->Worker->Factory->id = $this->Worker->field('factory_id');
		if ($this->Worker->Factory->field('user_id') != $this->AuthCert->user('id')) {
			$this->Session->setFlash(__('Invalid factory', true));
			$this->redirect(array('controller' => 'factories', 'action'=>'index'));
		}

		if ($this->Worker->del($id)) {
			$this->Session->setFlash(__('The application has been deleted from this factory', true));
			$this->redirect(array('controller' => 'factories', 'action'=>'view', $this->Worker->Factory->id));
		}
	}

	/**
	 * View a list of all workers
	 */
	public function admin_index()
	{
		$this->Worker->recursive = 0;
		$this->set('workers', $this->paginate());
	}

	/**
	 * Add a new worker to a factory
	 * @param string $factory_id The factory ID to add the worker to
	 * @return void
	 */
	public function admin_add($factory_id = null)
	{
		if ($factory_id == null && !empty($this->data)) {
			$factory_id = $this->data['Worker']['factory_id'];
		}

		if (!$factory_id) {
			$this->Session->setFlash(__('Invalid Factory ID', true));
			$this->redirect(array('controller' => 'factories', 'action'=>'index'));
		}
		$this->Worker->Factory->id = $factory_id;

		if (!empty($this->data)) {
			if ($this->_filterDoctypes()) {
				$this->Worker->create();
				if ($this->Worker->save($this->data)) {
					$this->Session->setFlash(__('The Worker has been saved', true));
					$this->redirect(array('controller' => 'factories', 'action'=>'view', $factory_id));
				} else {
					$this->Session->setFlash(__('The Worker could not be saved. Please, try again.', true));
				}
			}
		}

		$this->data['Worker']['factory_id'] = $factory_id;
		$this->data['Factory']['name'] = $this->Worker->Factory->field('name');
		$doctypes = $this->Worker->Doctype->find('list');
		$formats = $this->Worker->Format->find('list');
		$applications = $this->Worker->Application->find('list');
		$applicationDoctypes = $this->_applicationDoctypes();

		$this->set(compact('doctypes', 'formats', 'applications', 'applicationDoctypes'));

		$this->render('edit');
	}

	/**
	 * Edit a worker
	 * @param string $id The worker ID
	 * @return void
	 */
	public function admin_edit($id = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid application for this factory', true));
			$this->redirect(array('action'=>'index'));
		}

		$this->Worker->id = $id;
		$this->Worker->Factory->id = $this->Worker->field('factory_id');

		if (!empty($this->data)) {
			if ($this->_filterDoctypes()) {
				if ($this->Worker->save($this->data)) {
					$this->Session->setFlash(__('The application has been saved', true));
					$this->redirect(array('controller' => 'factories', 'action'=>'view', $this->Worker->Factory->id));
				} else {
					$this->Session->setFlash(__('The application could not be saved. Please, try again.', true));
				}
			}
		} else {
			$this->data = $this->Worker->read(null, $id);
		}

		$doctypes = $this->Worker->Doctype->find('list');
		$formats = $this->Worker->Format->find('list');
		$applications = $this->Worker->Application->find('list');
		$applicationDoctypes = $this->_applicationDoctypes();

		$this->set(compact('doctypes', 'formats', 'applications', 'applicationDoctypes'));
		$this->render('edit');
	}

	/**
	 * Delete a worker
	 *
	 * @param string $id The worker ID
	 * @return void
	 */
	public function admin_delete($id = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid application id', true));
			$this->redirect(array('action'=>'index'));
		}

		$this->Worker->id = $id;
		$this->Worker->Factory->id = $this->Worker->field('factory_id');

		if ($this->Worker->del($id)) {
			$this->Session->setFlash(__('The application has been deleted from this factory', true));
			$this->redirect(array('controller' => 'factories', 'action'=>'view', $this->Worker->Factory->id));
		}
	}
}

?>
