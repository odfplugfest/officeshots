<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * The Results controller
 */
class ResultsController extends AppController
{
	/** @var array The helpers that will be available on the view */
	public $helpers = array('Html', 'Form', 'ValidatorModel');
	
	/** @var array The components this controller uses */
	public $components = array('AuthCert');

	/** @var array Add Result and GalleriesRequest model */
	public $uses = array('Result', 'GalleriesRequest');

	/**
	 * Set the auth permissions for this controller
	 * @return void
	 */
	public function beforeFilter()
	{
		parent::beforeFilter();
		$this->AuthCert->allow('view', 'edit', 'download');
	}

	/**
	 * Get the result by it's ID and check access control.
	 *
	 * Calling methods should ensure that Job.Request is contained in the model query.
	 *
	 * @param string $id The result ID
	 * @param string $type The access type. Should be 'read' or 'write'
	 * @param return array An array containing the result.
	 */
	private function _getResult($id, $type = 'read')
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid Result.', true));
			$this->redirect(array('controller' => 'requests', 'action'=>'add'));
		}
		
		$result = $this->Result->read(null, $id);
		if (empty($result) || !$this->Result->checkAccess($this->AuthCert->user('id'), $type, $id)) {
			$this->Session->setFlash(__('Invalid Result.', true));
			$this->redirect(array('controller' => 'requests', 'action'=>'add'));
		}

		return $result;
	}
	
	/**
	 * You can't index results. Redirect to requests.
	 * @return void
	 */
	public function index()
	{
		$this->redirect(array('controller' => 'requests', 'action' => 'index'));
	}

	/**
	 * View a single Result
	 *
	 * @param string $id The Result ID
	 * @return void
	 */
	public function view($id = null)
	{
		$this->helpers[] = 'ResultModel';

		$this->Result->contain(array(
			'Job',
			'Job.Request',
			'Job.Application',
			'Job.Platform',
			'Factory',
			'Factory.User',
			'Factory.Operatingsystem',
			'Format',
			'Mimetype',
			'Validator',
		));

		$result = $this->_getResult($id);
		$writeAccess = $this->Result->Job->Request->checkAccess($this->AuthCert->user('id'), 'write', $result['Job']['Request']['id']);

		$this->set(compact('result', 'writeAccess'));
	}

	/**
	 * Edit a result description
	 *
	 * @param string $id The result ID
	 * @return void
	 */
	public function edit($id = null)
	{
		$this->helpers[] = 'ResultModel';
		$this->helpers[] = 'Javascript';

		$this->Result->contain(array(
			'Job',
			'Job.Application',
			'Job.Platform',
			'Mimetype',
			'Mimetype.Format',
		));
		$result = $this->_getResult($id, 'write');

		if (!empty($this->data)) {
			$this->data['Result']['id'] = $id;
			if (!$this->Result->save($this->data)) {
				$this->Session->setFlash(__('Unable to save the description', true));
			} else {
				$this->redirect(array('action' => 'view', $id));
			}
		}

		$verify = array(
			Result::VERIFY_PENDING => __('Not manually verified', true),
			Result::VERIFY_PASS    => __('Correct result', true),
			Result::VERIFY_FAIL    => __('Incorrect result', true),
		);

		$this->set(compact('result', 'verify'));
	}

	/**
	 * Action to download a single result
	 *
	 * @param string $id The Result ID
	 * @return void
	 */
	public function download($id = null)
	{
		$this->Result->contain(array(
			'Job',
			'Job.Request',
			'Mimetype',
		));

		$result = $this->_getResult($id);
		$path = $this->Result->getPath();
		$filename = basename($path);
		$extpos = strrpos($filename, '.');
		$name = substr($filename, 0, $extpos);
		$extension = substr($filename, $extpos + 1);

		$this->view = 'Media';
		$this->set(array(
			'id' => $filename,
			'name' => $name,
			'download' => true,
			'extension' => $extension,
			'path' => dirname($path) . DS,
			// NOTE: This short-circuits CakePHP's crude filetype check
			'mimeType' => array($extension => $result['Mimetype']['name']),
		));
	}

	/**
	 * Delete a single result.
	 *
	 * This is only for backend developers who want to re-use their requests while developing
	 *
	 * @param string $id The Result ID
	 * @return void
	 */
	public function delete($id = null)
	{
		$this->Result->contain(array(
			'Job',
			'Job.Request',
		));

		$result = $this->_getResult($id);
		if ($this->Result->del($id)) {
			$this->Session->setFlash(__('Result deleted', true));
			$this->redirect(array('controller' => 'requests', 'action'=>'view', $result['Job']['Request']['id']));
		}
	}

	public function admin_view($id = null)
	{
		$this->helpers[] = 'ResultModel';

		$this->Result->contain(array(
			'Job',
			'Job.Request',
			'Job.Application',
			'Job.Platform',
			'Factory',
			'Factory.User',
			'Factory.Operatingsystem',
			'Format',
			'Mimetype',
			'Validator',
		));

		$result = $this->_getResult($id, 'admin');

		$this->set(compact('result'));
		$this->render('view');
	}

	public function admin_edit($id = null)
	{
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Result', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Result->save($this->data)) {
				$this->Session->setFlash(__('The Result has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Result could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Result->read(null, $id);
		}
		$factories = $this->Result->Factory->find('list');
		$formats = $this->Result->Format->find('list');
		$this->set(compact('factories','formats'));
	}

	public function admin_delete($id = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Result', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Result->del($id)) {
			$this->Session->setFlash(__('Result deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}
}

?>
