<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

App::import('Component', 'Auth');

/**
 * Allow authentication with SSL client certificated on top of the standard Auth component
 *
 * All client certificate validation should be handled by the webserver. This model simply
 * uses the exported environment variables from e.g. mod_ssl or mod_gnutls
 */
class AuthCertComponent extends AuthComponent
{
	/** @var object Reference to the controller */
	public $controller = null;

	/** @var string The proper value of SSL_CLIENT_VERIFY after stripping REDIRECT_ */
	public $ssl_client_verify;

	/** @var string The proper value of SSL_CLIENT_S_DN after stripping REDIRECT_ */
	public $ssl_client_s_dn;

	/** @var boolean Has the user presented a user certificate? */
	public $hasCert = false;

	/** @var boolean Is the user certificate valid? */
	public $isCertValid = false;

	/** @var string The email address in the certificate */
	public $certEmail = '';

	/** @var bool Only alow login with SSL client certificate and disable regular Auth */
	public $certificateOnly = false;

	/** @var string The field in the User model that holds the e-mail address */
	public $emailField = 'email_address';

	/** @var integer How deep the redirect layers of Apache go. -1 means not set. */
	private $redirectLevel = -1;

	/**
	 * Initialize the AuthCert component
	 *
	 * @param object $controller A reference to the instanciating controller object
	 * @return void
	 */
	public function initialize(&$controller)
	{
		$this->controller =& $controller;
		$this->ssl_client_verify = $this->getEnv('SSL_CLIENT_VERIFY');
		$this->ssl_client_s_dn = $this->getEnv('SSL_CLIENT_S_DN');

		$this->hasCert = (
			$this->ssl_client_verify &&
			$this->ssl_client_verify != 'NONE'
		);
		$this->isCertValid = (
			$this->hasCert &&
			($this->ssl_client_verify == 'SUCCESS' || substr($this->ssl_client_verify, 0, 8) == 'GENEROUS')
		);

		parent::initialize($controller);

		if ($this->isCertValid) {
			if (!preg_match('/EMAIL=([^,]+)/i', $this->ssl_client_s_dn, $match)) {
				if (!preg_match('/emailAddress=([^,\/]+)/i', $this->ssl_client_s_dn, $match)) {
					$this->log("Email address could not be extracted from SSL cert: " . $this->ssl_client_s_dn, LOG_ERROR);
					return;
				}
			}

			if (!($certEmail = $match[1])) {
				$this->log("Email address in SSL cert is empty: " . $this->ssl_client_s_dn, LOG_ERROR);
				return;
			}

			$this->certEmail = $certEmail;
		}

	}

	/**
	 * Main execution method. If client certificate is not detected, pass on to Auth component
	 *
	 * @param object $controller A reference to the instanciating controller object
	 * @return boolean
	 */
	public function startup(&$controller)
	{
		$isErrorOrTests = (
			strtolower($controller->name) == 'cakeerror' ||
			(strtolower($controller->name) == 'tests' && Configure::read() > 0)
		);
		if ($isErrorOrTests) {
			return true;
		}
		if (!$this->__setDefaults()) {
			return false;
		}

		if ($this->hasCert) {
			if ($this->isCertValid && $this->user()) {
				return parent::startup($controller);
			}

			$this->notAuthorized();
		}

		if ($this->certificateOnly) {
			$this->notAuthorized();
		}
		
		return parent::startup($controller);
	}

	/**
	 * Get the current user from the session or from the SSL client certificate
	 *
	 * @param string $key field to retrive.  Leave null to get entire User record
	 * @return mixed User record. or null if no user is logged in.
	 */
	public function user($key = null)
	{
		$this->__setDefaults();

		if ($this->Session->check($this->sessionKey) && $this->certEmail) {
			$session = $this->Session->read($this->sessionKey);
			if ($session[$this->emailField] != $this->certEmail) {
				$this->Session->del($this->sessionKey);
			}
		}

		if (!$this->Session->check($this->sessionKey) && $this->isCertValid) {
			// Parse the e-mail address out of the client certificate
			if (!$this->certEmail) {
				return null;
			}

			// Find the user matching the e-mail address
			$model =& $this->getModel();
			$conditions = array($this->userModel . '.' . $this->emailField => $this->certEmail);
			$data = $model->find('first', array('conditions' => $conditions));

			if (!empty($data)) {
				if (!empty($data[$this->userModel][$this->fields['password']])) {
					unset($data[$this->userModel][$this->fields['password']]);
				}
				
				$this->Session->write($this->sessionKey, $data[$this->userModel]);
				// $this->log("Succesfully authenticated SSL cert user " . $this->certEmail, LOG_DEBUG);
			} else {
				$this->log("Could not authenticate SSL cert user '" . $this->certEmail . "'");
				return null;
			}
		}		

		return parent::user($key);
	}

	/**
	 * Render a 403 error page and give up
	 */
	public function notAuthorized()
	{
		$this->controller->viewPath = 'errors';
		$this->controller->set(array(
			'name' => __('Forbidden', true),
			'message' => $this->controller->here,
		));
		
		header("HTTP/1.0 403 Forbidden");
		$this->controller->render('error403');
		$this->controller->afterFilter();
		echo $this->controller->output;
		$this->_stop();
	}

	/**
	 * Get an environment variable with all the REDIRECT_ prefixes stripped off
	 *
	 * When redirecting Apache can add multiple REDIRECT_ prefixes. Typical SSL client certification
	 * takes two already, not counting any other redirects.
	 */
	private function getEnv($var)
	{
		// Find out how deep the redirect goes
		if ($this->redirectLevel == -1) {
			reset($_SERVER);
			$key = key($_SERVER);
			$this->redirectLevel = substr_count($key, 'REDIRECT_');
		}

		$result = '';
		$prefix = '';
		for ($i = 0; $i < $this->redirectLevel + 1; $i++) {
			if (isset($_SERVER[$prefix . $var])) {
				$result = $_SERVER[$prefix . $var];
			}
			$prefix .= 'REDIRECT_';
		}
		return $result;
	}
}

?>
