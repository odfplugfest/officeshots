<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class XmlrpcController extends AppController
{
	/** @var array No models are used by default */
	public $uses = array();

	/** @var array Use the RequestHandler component to output the correct XML headers */
	public $components = array('RequestHandler');

	/**
	 * @var array An array listing all controllers that will be searched for xmlrpc_ actions
	 * 
	 * The controller names should not include the Controller prefix.
	 */
	public $xmlrpc_controllers = array('Jobs');
	
	/**
	 * Set the auth permissions for this controller
	 * @return void
	 */
	function beforeFilter()
	{
		parent::beforeFilter();
		$this->AuthCert->allow('index');
	}

	/**
	 * The entry point for the XMLRPC interface
	 *
	 * @param string $method For interactive API documentation browsing, this specifies the method to view.
	 */
	public function index($method = false)
	{
		if ($this->RequestHandler->isPost()) {
			$this->serveXmlrpc();
			return;
		}

		$this->browseApi($method);
	}

	/**
	 * Serve XMLRPC requests
	 */
	public function serveXmlrpc()
	{
		// Set our own error handler
		Configure::write('debug', 0);
		error_reporting(E_ALL);
		set_error_handler(array($this, 'handleError'));

		// Check permissions. We need to do this manually because the original 'index' action
		// is allowed for everyone in order to browse the API documentation
		$this->action = 'api';
		if (!$this->isAuthorized()) {
			$this->redirect(null, 403);
		}

		// Create the XMLRPC server
		$server = xmlrpc_server_create();
		$methods = $this->getMethods();

		foreach ($methods as $method) {
			$method[0]->constructClasses();
			$methodName = Inflector::underscore($method[0]->name) . '.' . substr($method[1], 7);
			xmlrpc_server_register_method($server, $methodName, $method);
		}

		// TODO: Add XMLRPC introsection support
		// xmlrpc_server_register_introspection_callback($server, array($this, 'introspect'));

		// Run it
		if (!isset($HTTP_RAW_POST_DATA))
			$HTTP_RAW_POST_DATA = file_get_contents('php://input');

		if ($response = xmlrpc_server_call_method($server, $HTTP_RAW_POST_DATA, '')) {
			$this->set('response', $response);
		}

		// Display it as XML
		$this->RequestHandler->respondAs('xml');
		$this->layout = 'xmlrpc';
		$this->action = 'xmlrpc';
	}

	/**
	 * Browse the XMLRPC API documentation. Uses reflection to generate the documentation from the source.
	 *
	 * @param string $method The method to view the documentation for, or false for the method index.
	 */
	public function browseApi($method = false)
	{
		if ($method) {
			list($controllerName, $methodName) = explode('.', $method);
			$controllerName = Inflector::camelize($controllerName);
			App::import('Controller', $controllerName);
			
			$reflector = new ReflectionMethod($controllerName . 'Controller', 'xmlrpc_' . $methodName);
			$docblock = preg_replace('/\n\t+ \*/', "\n", $reflector->getDocComment()); // Strip DocBlock comment
			$docblock = preg_replace('/^\s@\w+[^\n]+$\n/m', '', $docblock); // Strip PHPDoc tags
			$docblock = implode("\n", array_slice(explode("\n", $docblock), 1, -1)); // Strip start and end line

			$this->set('methodName', $method);
			$this->set('docblock', h($docblock));
			$this->action = 'method';
			return;
		}

		$methodIndex = array();
		$methods = $this->getMethods();
		foreach ($methods as $callback) {
			$methodIndex[] = Inflector::tableize($callback[0]->name) . '.' . substr($callback[1], 7);
		}

		$this->set('methods', $methodIndex);
		$this->action = 'index';
	}

	/**
	 * Return an array containing all XMLRPC functions as callbacks
	 * 
	 * The callbacks are in the form of array(&$controller, $methodName);
	 *
	 * @return array
	 */
	private function getMethods()
	{
		$methods = array();
		foreach ($this->xmlrpc_controllers as $controllerName) {
			App::import('Controller', $controllerName);
			$controllerName .= 'Controller';
			$controller = new $controllerName();

			foreach ($controller->methods as $methodName) {
				if (substr($methodName, 0, 7) == 'xmlrpc_') {
					$methods[] = array(&$controller, $methodName);
				}
			}
		}

		return $methods;
	}
	
	/**
	 * Overrides PHP's default error handling.
	 *
	 * @param integer $code Code of error
	 * @param string $description Error description
	 * @param string $file File on which error occurred
	 * @param integer $line Line that triggered the error
	 * @param array $context Context
	 * @return boolean true if error was handled
	 * @access public
	 */
	public function handleError($code, $description, $file = null, $line = null, $context = null) 
	{
		if (error_reporting() == 0 || $code == 2048) {
			return;
		}

		$trace = array();
		if (function_exists('xdebug_get_function_stack')) {
			$trace = xdebug_get_function_stack();
		}

		echo '<?xml version="1.0"?>
<methodResponse>
   <fault>
      <value>
         <struct>
            <member>
               <name>faultCode</name>
               <value><int>1</int></value>
            </member>
            <member>
               <name>faultString</name>
	       <value>
	          <string>
		  	PHP Error occurred:
		  	code: ' . $code . '
			description: ' . $description . '
			file: ' . $file . '
			line: ' . $line . '
			trace: ' . str_replace(array('<', '>'), array('&lt;', '&gt;'), print_r($trace, true)) . '
		  </string>
	       </value>
            </member>
         </struct>
      </value>
   </fault>
</methodResponse>';
		die();
	}
}

?>
