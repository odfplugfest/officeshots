<?php
class MimetypesController extends AppController {

	var $name = 'Mimetypes';
	var $helpers = array('Html', 'Form');

	function _get_icons() {
		$icons = glob(IMAGES . 'icons/*.*');
		foreach ($icons as &$icon) {
			$icon = basename($icon);
		}
		$icons = array_combine($icons, $icons);
		return $icons;
	}

	function admin_index() {
		$this->Mimetype->recursive = 0;
		$this->set('mimetypes', $this->paginate());
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Mimetype.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('mimetype', $this->Mimetype->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->Mimetype->create();
			if ($this->Mimetype->save($this->data)) {
				$this->Session->setFlash(__('The Mimetype has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Mimetype could not be saved. Please, try again.', true));
			}
		}
		$doctypes = $this->Mimetype->Doctype->find('list');
		$formats = $this->Mimetype->Format->find('list');
		$icons = $this->_get_icons();

		$this->set(compact('doctypes', 'formats', 'icons'));
		$this->render('admin_edit');
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Mimetype', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Mimetype->save($this->data)) {
				$this->Session->setFlash(__('The Mimetype has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Mimetype could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Mimetype->read(null, $id);
		}
		$doctypes = $this->Mimetype->Doctype->find('list');
		$formats = $this->Mimetype->Format->find('list');
		$icons = $this->_get_icons();

		$this->set(compact('doctypes', 'formats', 'icons'));
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Mimetype', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Mimetype->del($id)) {
			$this->Session->setFlash(__('Mimetype deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

}
?>
