<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * The factories controller
 */
class FactoriesController extends AppController
{
	/** @var array The helpers that will be available on the view */
	public $helpers = array('Html', 'Form');

	/** @var array The components this controller uses */
	public $components = array('AuthCert');

	/** @var array Define the pagination style */
	public $paginate = array('limit' => 25);

	/**
	 * Set the auth permissions for this controller
	 * @return void
	 */
	function beforeFilter()
	{
		parent::beforeFilter();
		$this->AuthCert->allow('index', 'view', 'active');
	}

	/**
	 * View a list of all factories
	 *
	 * @param return void
	 */
	public function index()
	{
		$this->Factory->recursive = 0;
		$this->set(array(
			'canAddFactories' => $this->__permitted('factories', 'add'),
			'factories' => $this->paginate(),
		));
	}

	/**
	 * View a list of all active factories
	 *
	 * @param return void
	 */
	public function active()
	{
		$cutoff = date('Y-m-d H:i:s', time() - Configure::read('Factory.polltime'));
		$this->paginate['conditions'] = array('Factory.last_poll >' => $cutoff);

		$this->Factory->recursive = 0;
		$this->set(array(
			'canAddFactories' => $this->__permitted('factories', 'add'),
			'factories' => $this->paginate(),
		));
		$this->render('index');
	}

	/**
	 * View a single factory
	 *
	 * @param string $id The factory ID
	 * @return void
	 */
	public function view($id = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid Factory.', true));
			$this->redirect(array('action'=>'index'));
		}

		$this->Factory->contain(array(
			'User',
			'Operatingsystem',
			'Worker',
			'Worker.Application',
			'Worker.Format' => array('order' => 'Format.name ASC'),
		));

		$factory = $this->Factory->read(null, $id);
		$this->set(array(
			'factory' => $factory,
			'canEdit' => ($factory['Factory']['user_id'] == $this->AuthCert->user('id')),
		));
	}

	/**
	 * Add a new factory
	 *
	 * @return void
	 */
	public function add()
	{
		if (!empty($this->data)) {
			$this->Factory->create();
			$this->data['Factory']['user_id'] = $this->AuthCert->user('id');

			if ($this->Factory->save($this->data)) {
				$this->Session->setFlash(__('The Factory has been saved', true));
				$this->redirect(array('action'=>'view', $this->Factory->id));
			} else {
				$this->Session->setFlash(__('The Factory could not be saved. Please, try again.', true));
			}
		}

		$operatingsystems = $this->Factory->Operatingsystem->find('all', array('recursive' => -1));
		$this->set(compact('operatingsystems'));
		
		$this->render('edit');
	}

	/**
	 * Edit a factory
	 *
	 * @param string $id The factory ID
	 * @return void
	 */
	public function edit($id = null)
	{
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Factory', true));
			$this->redirect(array('action'=>'index'));
		}

		$this->Factory->id = $id;
		if ($this->Factory->field('user_id') != $this->AuthCert->user('id')) {
			$this->Session->setFlash(__('Invalid Factory', true));
			$this->redirect(array('action'=>'index'));
		}

		if (!empty($this->data)) {
			if ($this->Factory->save($this->data)) {
				$this->Session->setFlash(__('The Factory has been saved', true));
				$this->redirect(array('action'=>'view', $id));
			} else {
				$this->Session->setFlash(__('The Factory could not be saved. Please, try again.', true));
			}
		} else {
			$this->data = $this->Factory->read(null, $id);
		}

		$operatingsystems = $this->Factory->Operatingsystem->find('all', array('recursive' => -1));
		$this->set(compact('operatingsystems'));
	}

	/**
	 * Show a list of all factories
	 * @param return void
	 */
	public function admin_index()
	{
		$this->index();
		$this->render('index');
	}

	/**
	 * View a list of all active factories
	 * @param return void
	 */
	public function admin_active()
	{
		$this->active();
		$this->render('index');
	}

	/**
	 * View a single factory
	 * @param string $id The factory ID
	 * @param return void
	 */
	public function admin_view($id = null)
	{
		$this->view($id);
		$this->set('canEdit', true);
		$this->render('view');
	}

	/**
	 * Add a new factory
	 * @param return void
	 */
	public function admin_add()
	{
		if (!empty($this->data)) {
			$this->Factory->create();
			$this->data['Factory']['user_id'] = $this->AuthCert->user('id');

			if ($this->Factory->save($this->data)) {
				$this->Session->setFlash(__('The Factory has been saved', true));
				$this->redirect(array('action'=>'view', $this->Factory->id));
			} else {
				$this->Session->setFlash(__('The Factory could not be saved. Please, try again.', true));
			}
		}

		$users = $this->Factory->User->find('list');
		$operatingsystems = $this->Factory->Operatingsystem->find('all', array('recursive' => -1));
		$this->set(compact('users', 'operatingsystems'));
		$this->render('edit');
	}

	/**
	 * Edit a factory
	 * @param string $id The factory ID
	 * @param return void
	 */
	public function admin_edit($id = null)
	{
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Factory', true));
			$this->redirect(array('action'=>'index'));
		}

		if (!empty($this->data)) {
			if ($this->Factory->save($this->data)) {
				$this->Session->setFlash(__('The Factory has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Factory could not be saved. Please, try again.', true));
			}
		}

		if (empty($this->data)) {
			$this->data = $this->Factory->read(null, $id);
		}

		$users = $this->Factory->User->find('list');
		$operatingsystems = $this->Factory->Operatingsystem->find('all', array('recursive' => -1));
		$this->set(compact('users','operatingsystems'));
		$this->render('edit');
	}
}

?>
