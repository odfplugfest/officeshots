<?php
/* SVN FILE: $Id: bootstrap.php 7945 2008-12-19 02:16:01Z gwoo $ */
/**
 * Short description for file.
 *
 * Long description for file
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) :  Rapid Development Framework (http://www.cakephp.org)
 * Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright     Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 * @link          http://www.cakefoundation.org/projects/info/cakephp CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.app.config
 * @since         CakePHP(tm) v 0.10.8.2117
 * @version       $Revision: 7945 $
 * @modifiedby    $LastChangedBy: gwoo $
 * @lastmodified  $Date: 2008-12-18 20:16:01 -0600 (Thu, 18 Dec 2008) $
 * @license       http://www.opensource.org/licenses/mit-license.php The MIT License
 */

/**
 *
 * This file is loaded automatically by the app/webroot/index.php file after the core bootstrap.php is loaded
 * This is an application wide file to load any function that is not used within a class define.
 * You can also use this to include or require any files in your application.
 *
 */

/**
 * Set a path to the files directory
 */
define('FILES',  ROOT . DS . APP_DIR . DS . 'files' . DS);

/**
 * Set default timezone
 */
date_default_timezone_set('Europe/Amsterdam');

/**
 * Convert an IP address from presentation to decimal(39,0) format suitable for storage in MySQL
 *
 * @param string $ip_address An IP address in IPv4, IPv6 or decimal notation
 * @return string The IP address in decimal notation
 */
function inet_ptod($ip_address)
{
	// IPv4 address
	if (strpos($ip_address, ':') === false && strpos($ip_address, '.') !== false) {
		$ip_address = '::' . $ip_address;
	}

	// IPv6 address
	if (strpos($ip_address, ':') !== false) {
		$network = inet_pton($ip_address);
		$parts = unpack('N*', $network);

		foreach ($parts as &$part) {
			if ($part < 0) {
				$part = bcadd((string) $part, '4294967296');
			}

			if (!is_string($part)) {
				$part = (string) $part;
			}
		}

		$decimal = $parts[4];
		$decimal = bcadd($decimal, bcmul($parts[3], '4294967296'));
		$decimal = bcadd($decimal, bcmul($parts[2], '18446744073709551616'));
		$decimal = bcadd($decimal, bcmul($parts[1], '79228162514264337593543950336'));

		return $decimal;
	}

	// Decimal address
	return $ip_address;
}

/**
 * Convert an IP address from decimal format to presentation format
 *
 * @param string $decimal An IP address in IPv4, IPv6 or decimal notation
 * @return string The IP address in presentation format
 */
function inet_dtop($decimal)
{
	// IPv4 or IPv6 format
	if (strpos($decimal, ':') !== false || strpos($decimal, '.') !== false) {
		return $decimal;
	}

	// Decimal format
	$parts = array();
	$parts[1] = bcdiv($decimal, '79228162514264337593543950336', 0);
	$decimal = bcsub($decimal, bcmul($parts[1], '79228162514264337593543950336'));
	$parts[2] = bcdiv($decimal, '18446744073709551616', 0);
	$decimal = bcsub($decimal, bcmul($parts[2], '18446744073709551616'));
	$parts[3] = bcdiv($decimal, '4294967296', 0);
	$decimal = bcsub($decimal, bcmul($parts[3], '4294967296'));
	$parts[4] = $decimal;

	foreach ($parts as &$part) {
		if (bccomp($part, '2147483647') == 1) {
			$part = bcsub($part, '4294967296');
		}

		$part = (int) $part;
	}

	$network = pack('N4', $parts[1], $parts[2], $parts[3], $parts[4]);
	$ip_address = inet_ntop($network);

	// Turn IPv6 to IPv4 if it's IPv4
	if (preg_match('/^::\d+.\d+.\d+.\d+$/', $ip_address)) {
		return substr($ip_address, 2);
	}

	return $ip_address;
}

/**
 * The settings below can be used to set additional paths to models, views and controllers.
 * This is related to Ticket #470 (https://trac.cakephp.org/ticket/470)
 *
 * $modelPaths = array('full path to models', 'second full path to models', 'etc...');
 * $viewPaths = array('this path to views', 'second full path to views', 'etc...');
 * $controllerPaths = array('this path to controllers', 'second full path to controllers', 'etc...');
 *
 */
//EOF
?>
