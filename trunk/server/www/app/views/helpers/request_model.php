<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

App::import('Model', 'Request');

/**
 * A helper to assist in outputting Requests
 */
class RequestModelHelper extends AppHelper
{
	/** @var array Add the Time helper */
	public $helpers = array('Time');

	/**
	 * Convert Request state to a huma readable message
	 *
	 * @param array $request an array containing the request
	 * @return string the state message
	 */
	public function getState($request) {
		if (!is_array($request)) {
			return '';
		}

		if (!isset($request['Request'])) {
			$request = array('Request' => $request);
		}
		
		switch ($request['Request']['state']) {
		case Request::STATE_UPLOADING:
			return $this->output(__('Uploading', true));
		case Request::STATE_PREPROCESSOR_QUEUED:
			return $this->output(__('Queued for preprocessor', true));
		case Request::STATE_SCAN_FOUND:
			return $this->output(sprintf(__('Scan failed. File infected with "%s"', true), $request['Request']['state_info']));
		case Request::STATE_PREPROCESSOR_FAILED:
			return $this->output(sprintf(__('Preprocessor failed with error "%s"', true), $request['Request']['state_info']));
		case Request::STATE_QUEUED:
			if (!$request['Request']['expire']) {
				return $this->output(__('Queued. Never expires', true));
			}
			return $this->output(sprintf(__('Queued. Expires in %s', true), $this->Time->relativeTime($request['Request']['expire'])));
		case Request::STATE_FINISHED:
			return $this->output(__('Finished', true));
		case Request::STATE_EXPIRED:
			return $this->output(sprintf(__('Expired %s', true), $this->Time->relativeTime($request['Request']['expire'])));
		case Request::STATE_CANCELLED:
			return $this->output(__('Cancelled', true));
		default:
			return $this->output(__('Unknown', true));
		}
	}
}

?>
