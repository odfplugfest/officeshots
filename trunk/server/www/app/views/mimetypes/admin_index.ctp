<div class="mimetypes index">
<h2><?php __('Mimetypes');?></h2>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('name');?></th>
	<th><?php echo $paginator->sort('icon');?></th>
	<th><?php echo $paginator->sort('extension');?></th>
	<th><?php echo $paginator->sort('doctype_id');?></th>
	<th><?php echo $paginator->sort('format_id');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php foreach ($mimetypes as $mimetype):?>
	<tr>
		<td>
			<?php echo $html->link($mimetype['Mimetype']['name'], array('action'=>'view', $mimetype['Mimetype']['id'])); ?>
		</td>
		<td>
			<?php echo $mimetype['Mimetype']['icon']; ?>
		</td>
		<td>
			<?php echo $mimetype['Mimetype']['extension']; ?>
		</td>
		<td>
			<?php echo $html->link($mimetype['Doctype']['name'], array('controller'=> 'doctypes', 'action'=>'view', $mimetype['Doctype']['id'])); ?>
		</td>
		<td>
			<?php echo $html->link($mimetype['Format']['name'], array('controller'=> 'formats', 'action'=>'view', $mimetype['Format']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('Edit', true), array('action'=>'edit', $mimetype['Mimetype']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action'=>'delete', $mimetype['Mimetype']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $mimetype['Mimetype']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New Mimetype', true), array('action'=>'add')); ?></li>
	</ul>
</div>
