<?php
	$html->css('wmd', null, array(), false);
	$javascript->link('showdown', false);
	$javascript->link('wmd', false);
?>

<div class="results view">
<h2><?php printf(__('Filename "%s"', true), $result['Result']['filename']);?></h2>
	<?php if ($result['Result']['state'] == Result::STATE_SCAN_FOUND): ?>
		<img src="/img/icons/virus.png" alt="" style="float: left;" />
	<?php else: ?>
		<img src="/img/icons/<?php echo $result['Mimetype']['icon'];?>" alt="" style="float: left;" />
	<?php endif; ?>
	<dl style="margin-left: 10em;">
		<dt><?php __('Uploaded'); ?></dt>
		<dd>
			<?php echo $result['Result']['created']; ?>
			&nbsp;
		</dd>
		<dt><?php __('Status'); ?></dt>
		<dd>
			<?php echo $resultModel->getState($result); ?>
			&nbsp;
		</dd>
		<dt><?php __('Application'); ?></dt>
		<dd>
			<?php echo $result['Job']['Application']['name'];?> <?php echo $result['Job']['version'];?>
			(<?php echo $result['Job']['Platform']['name'];?>)
			&nbsp;
		</dd>
		<dt><?php __('Document type'); ?></dt>
		<dd>
			<?php echo $result['Mimetype']['Format']['name']; ?>
			&nbsp;
		</dd>
	</dl>
</div>

<?php echo $form->create('Result', array('url' => '/results/edit/' . $result['Result']['id']));?>
	<h3><?php __('Manual verification');?></h3>
	<p><?php __('Have you manually checked if this conversion result is correct?');?></p>
	<div class="input">
		<?php echo $form->radio('Result.verified', $verify, array('legend' => false, 'default' => $result['Result']['verified']));?>
	</div>

	<h3><?php __('Description');?></h3>
	<div id="wmd-button-bar"></div>
	<textarea id="wmd-input" name="data[Result][description]"><?php echo h($result['Result']['description']);?></textarea>
	<div id="wmd-preview" class="polaroid"></div>
<?php echo $form->end(__('Save', true));?>
	
