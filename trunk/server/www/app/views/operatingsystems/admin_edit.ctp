<div class="operatingsystems form">
<?php echo $form->create('Operatingsystem');?>
	<fieldset>
		<legend><?php 
			if ($this->action == 'admin_edit') {
				__('Edit Operatingsystem');
			} else {
				__('Add Operatingsystem');
			}
		?></legend>
	<?php
		if ($this->action == 'admin_edit') {
			echo $form->input('id');
		}
		echo $form->input('name');
		echo $form->input('version');
		echo $form->input('codename');
		echo $form->input('platform_id');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
