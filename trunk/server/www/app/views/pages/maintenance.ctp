<h3>Temporarily down for maintenance</h3>

<?php echo $this->element('sidebar'); ?>

<p>We're temporarily down for maintenance while we install some exciting new additions to Officeshots. Please stand by...</p>
