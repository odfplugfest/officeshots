<h2><?php __('Disclaimer:');?></h2>

<?php echo $this->element('sidebar'); ?>

<ul>
	<li><?php __('Do not submit confidential documents. We don&#8217;t publish them, but there is no protection in the cloud.');?></li>
	<li><?php __('If you get strange results, check if your input document is technically sound through an <a href="http://odf-validator.rhcloud.com/">ODF Validartor</a>.');?></li>
<!--	<li><?php // __('If you use your own fonts, you may be unpleasantly surprised to find out that office files don&#8217;t carry any font information. So your beautifully styled pages with hand written fonts have always looked that bad when other people opened them.');?></li> -->
	<li><?php __('Don&#8217;t forget that the quality of your input document matters! It might be that your current software supplier has an immature implementation of ODF.');?></li>
</ul>
