<h3>Creates transparency in the Office market.</h3>

<?php echo $this->element('sidebar'); ?>

<p><em>An online service where end users and professionals can compare the output of different office suites - for their own
documents!</em></p>

<p>A thousand flowers are blossoming in the world of productivity tools, thanks to the rise of open standards like the <A href="http://www.oasis-open.org/committees/tc_home.php?wg_abbrev=office">Open Document Format</A> (ISO 26300:2006) for storing and sharing content. Innovation is taking place where it was once thought impossible. With diversity comes choice, but also the need to make sure you are working with the best-of-breed solutions - wether they are open source, freeware, software-as-a-service or paid solutions. </p>

<p>A document may look nice on your screen while you are working on it in its native application, but that is just the outside. If the exchange formats are not implemented correctly, it might look like a sloppy mess to the person receiving the business proposal, invoice or report you worked so hard on. Officeshots.org was inspired by an open source project from the web standards world called <A href="http://www.browsershots.org">browsershots.org</A>. 
</p>

<p>Officeshots.org will help you make a better choice by letting <b>you</b> compare the output and other behaviour of a wide variety of applications. Does your corporate style - the technical basis for many documents - actually look consistent across the board of applications - from OpenOffice.org 3.0, Adobe Buzzword and Symphony 1.2 to Microsoft Office 2000 with the ODF addin from Microsoft - or the one from Sun Microsystems? And how does it look on Mac OS X in iWork? When you are in an acquisition phase, officeshots.org will help you do a reality check if that fancy new open source suite or that productivity package you can get a bargain deal at - actually does what it says. On the spot.</p>

<p>After submitting a document to officeshots.org, the site will deliver the print, screen and code output as produced by a variety of different productivity applications - in different versions and across operating system platforms. Anyone can upload ODF-documents, at no cost. This is because officeshots.org actually divides the work among rendering servers hosted by vendors and the community. That could be you (if you're interested, <A href="mailto:info@opendocsociety.org">contact us</A> or look here to see the <a href="http://docs.officeshots.org">technical documentation</a>).</p>

<p>Officeshots.org is an initiative by <a href="http://www.opendocsociety.org">OpenDoc Society</a> and the <a href="http://www.noiv.nl">Netherlands in Open Connection</a>-program from the Netherlands government. Officeshots.org is sponsored by NLnet foundation, and is developed as an open source (Affero GPLv3-licenced) project in a team lead by Sander Marechal from <a href="http://www.jejik.com">Lone Wolves</a>.</p>
