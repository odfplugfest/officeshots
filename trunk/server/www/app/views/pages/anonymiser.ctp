<h2>The ODF Anonymiser</h2>

<?php echo $this->element('sidebar'); ?>

<p>The ODF Anonymiser tries to make your document completely anonymous while maintaining it's overall structure.
All metadata is removed or cleaned. All text in the document is replaces with gibberish text that has approximately
the same word length and word distribution. All images are replaced with placeholder images. All unknown content is removed</p>

<p>The result of the anonymiser is a document that has the same general structure but with made-up contents. If your original document
does not work in a certain application, the anonymised version of the document should fail in the same manner. By using the anonymiser
you can test your private documents without exposing the contents to our rendering clients.</p>

<p>Officeshots already tries to treat your doucments in a private manner. If you submit a document when you are logged in, then only you can
download the result. If you submit a document without logging in, then the result will be given an URL that is almost impossible to guess.
We vet everyone who wants to run a rendering client for Officeshots. The ODF Anonymiser is simply an extra layer on top of this. When you
use the Anonymiser, your uploaded document is replaced with the anonymous version. We don't store the original document any longer than
required and rendering clients never get to see the original document, just the anonymised version.</p>

<p>Nevertheless, the same disclaimer still applies:</p>

<p><strong>Do not submit confidential documents. We don&#8217;t publish them, but there is no protection in the cloud.</strong></p>

<p>We make no guarantees. If you want to be sure, you can download the ODF Anonymiser we use separately. It is written and maintained
by the people who created <a href="http://www.hforge.org/itools">iTools</a>. Simply install the iTools suite on your computer and use the
included <tt>iodf-greek.py</tt> script to create an anonymous version of your document. Check the resulting document before you upload it
to make sure that it worked.</p>
