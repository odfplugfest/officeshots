<h2>Press information</h2>

<?php echo $this->element('sidebar'); ?>

<h3>Logos</h3>

<ul>
	<li><a href="/img/logos/officeshots-logo.svg">Scalable Vector Graphics (SVG), 51.8 KiB</a></li>
	<li><a href="/img/logos/officeshots-logo-100x33.png">Portable Network Graphics (PNG) 100x33 px, 4.0 KiB</a></li>
	<li><a href="/img/logos/officeshots-logo-200x66.png">Portable Network Graphics (PNG) 200x66 px, 9.5 KiB</a></li>
	<li><a href="/img/logos/officeshots-logo-399x132.png">Portable Network Graphics (PNG) 399x132 px, 21.9 KiB</a></li>
	<li><a href="/img/logos/officeshots-logo-767x253.png">Portable Network Graphics (PNG) 767x253 px, 47.7 KiB</a></li>
</ul>

<h3>About OpenDoc Society</h3>

<p>The goal of OpenDoc Society is to bring together individuals and
organisations with a stake or interest in the openness and future of
documents to learn from each other and share knowledge. OpenDoc Society
offers a platform where developers, publishers, decision makers,
educators, vendors, IT managers, academics, writers, archivists and
other stakeholders kan bring their knowledge together and learn and
collaborate with interesting like-minded people. OpenDoc Society wants
to build local human networks of experts and stakeholders in <a href="http://www.opendocumentformat.org">ODF</a> from
all areas. It want to be a leading organisation in spreading knowledge
about open formats like ODF and PDF to society at large, through
publications, workshops, masterclasses, tutorials for developers,
decision makers, users and other stakeholders. It wants to foster and
strengthen the ecosystem around open document formats: from enterprise
content management, assistive software for the visually impaired up to
readers for cell phones and game consoles. OpenDoc Society is supported
by a large number of organisations, including government bodies,
international corporations, educational, cultural and scientific
institutions and NGO's .  If you care about an open information society
and a transparent market, why don't you join?</p>

<p>More info: <a href="http://nl.opendocsociety.org">http://nl.opendocsociety.org</a> 
and <a href="http://www.opendocsociety.org">http://www.opendocsociety.org</a></p>


<h3>About Netherlands in Open Connection (Nederland Open in Verbinding)</h3>

<p>The Netherlands in Open Connection (NOiV) informs government
organizations about the possibilities of open standards (OS) and open
source software (OSS) and encourages them, where possible to fit into
their information systems. The program is based on the Action programmes
Netherlands in Open Connection, initated by the Netherlands minister of
Foreign Trade and the state secretary of Interior and Kingdom Relations
in September 2007. The program NOiV offers practical support in the form
of information, knowledge and tools for government organizations open
source software can use. The program is executed by ICTU Foundation,
commissioned by the Ministry of Economy Affairs and the Ministry of
Interior Affairs and Kingdom Relations. The activities of the program
are primarily aimed at policy makers and public sector ICT management
and their suppliers. In addition, NOiV has a coordinating role with the
parties from the government and semi-government and the private sector.</p>

<p>More info: <a href="http://noiv.nl">http://noiv.nl</a></p>

<h3>About NLnet Foundation</h3>

<p>NLnet Foundation is a private charity fund supporting open standards and
open source worldwide, and has over the years actively contributed to
(internet) standards, open source projects and subsidiary or enabling
activities such as the development of GPLv3. NLnet foundation is an
independent organisation whose means came initially from interest on a
very substantial own capital formed in 1997 by the sale of the first
Dutch Internet Service Provider. Its private capital ensures an absolute
independent position. The articles of association for the NLnet
foundation state: "to promote the exchange of electronic information and
all that is related or beneficial to that purpose".</p>
<p>NLnet foundation manages the <a href="http://www.nlnet.nl/odf">ODF Fund</a>. Please
consider making a contribution.</p>
<p>More info: <a href="http://www.nlnet.nl">http://nlnet.nl</a></p>


<h3>About Lone Wolves Foundation</h3>

<p>Lone Wolves Foundation is a small development company from The Netherlands
that focusses on free and open source software. All profits from its open source
work and its commercial offers are invested in new and existing open source projects.
Notable projects include Officeshots.org, ODF-XSLT and Gnome Hearts.</p>

<p>More info: <a href="http://www.jejik.com">http://www.jejik.com</a></p>
