<div class="doctypes index">
<h2><?php __('Doctypes');?></h2>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('name');?></th>
	<th><?php echo $paginator->sort('code');?></th>
	<th><?php echo $paginator->sort('order');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php foreach ($doctypes as $doctype):?>
	<tr>
		<td>
			<?php echo $html->link($doctype['Doctype']['name'], array('action'=>'view', $doctype['Doctype']['id'])); ?>
		</td>
		<td>
			<?php echo $doctype['Doctype']['code']; ?>
		</td>
		<td>
			<?php echo $doctype['Doctype']['order']; ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('Up', true), array('action'=>'moveup', $doctype['Doctype']['id'])); ?>
			<?php echo $html->link(__('Down', true), array('action'=>'movedown', $doctype['Doctype']['id'])); ?>
			<?php echo $html->link(__('Edit', true), array('action'=>'edit', $doctype['Doctype']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action'=>'delete', $doctype['Doctype']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $doctype['Doctype']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New Doctype', true), array('action'=>'add')); ?></li>
		<li><?php echo $html->link(__('Reset order', true), array('action'=>'reset')); ?></li>
	</ul>
</div>
