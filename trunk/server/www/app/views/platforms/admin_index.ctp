<div class="platforms index">
<h2><?php __('Platforms');?></h2>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('name');?></th>
	<th><?php echo $paginator->sort('order');?></th>
	<th><?php echo $paginator->sort('created');?></th>
	<th><?php echo $paginator->sort('modified');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($platforms as $platform):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $platform['Platform']['name']; ?>
		</td>
		<td>
			<?php echo $platform['Platform']['order']; ?>
		</td>
		<td>
			<?php echo $platform['Platform']['created']; ?>
		</td>
		<td>
			<?php echo $platform['Platform']['modified']; ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('View', true), array('action'=>'view', $platform['Platform']['id'])); ?>
			<?php echo $html->link(__('Edit', true), array('action'=>'edit', $platform['Platform']['id'])); ?>
			<?php echo $html->link(__('Move up', true), array('action'=>'moveup', $platform['Platform']['id'])); ?>
			<?php echo $html->link(__('Move down', true), array('action'=>'movedown', $platform['Platform']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action'=>'delete', $platform['Platform']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $platform['Platform']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New Platform', true), array('action'=>'add')); ?></li>
		<li><?php echo $html->link(__('Reset order', true), array('action'=>'reset')); ?></li>
	</ul>
</div>
