<div class="testsuites index">
<h2><?php __('Testsuites');?></h2>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('name');?></th>
	<th><?php echo $paginator->sort('source');?></th>
	<th><?php echo $paginator->sort('root');?></th>
	<th><?php echo $paginator->sort('created');?></th>
	<th><?php echo $paginator->sort('modified');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($testsuites as $testsuite):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $html->link($testsuite['Testsuite']['name'], array('action'=>'view', $testsuite['Testsuite']['id'])); ?>
		</td>
		<td>
			<?php echo $testsuite['Testsuite']['source']; ?>
		</td>
		<td>
			<?php echo $testsuite['Testsuite']['root']; ?>
		</td>
		<td>
			<?php echo $testsuite['Testsuite']['created']; ?>
		</td>
		<td>
			<?php echo $testsuite['Testsuite']['modified']; ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('Edit', true), array('action'=>'edit', $testsuite['Testsuite']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action'=>'delete', $testsuite['Testsuite']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $testsuite['Testsuite']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New Testsuite', true), array('action'=>'add')); ?></li>
	</ul>
</div>
