<div class="testsuites form">
<?php echo $form->create('Testsuite');?>
	<fieldset>
		<legend><?php
			if ($this->action == 'admin_edit') {
				__('Edit Testsuite');
			} else {
				__('Add Testsuite');
			}
		?></legend>
	<?php
		if ($this->action == 'admin_edit') {
			echo $form->input('id');
		}
		echo $form->input('name');
		echo $form->input('source', array('type' => 'text', 'between' => 'Path to the testsuite files, relative to <em>' . FILES . '</em>, no trailing slash.'));
		echo $form->input('root', array('type' => 'text', 'between' => 'Path to a directory to store the results, relative to <em>' . FILES . '</em>, no trailing slash.'));
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
