<?php
/* SVN FILE: $Id: default.ctp 7945 2008-12-19 02:16:01Z gwoo $ */
/**
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) :  Rapid Development Framework (http://www.cakephp.org)
 * Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright     Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 * @link          http://www.cakefoundation.org/projects/info/cakephp CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.view.templates.layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @version       $Revision: 7945 $
 * @modifiedby    $LastChangedBy: gwoo $
 * @lastmodified  $Date: 2008-12-18 20:16:01 -0600 (Thu, 18 Dec 2008) $
 * @license       http://www.opensource.org/licenses/mit-license.php The MIT License
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php echo $html->charset(); ?>
	<title>
		<?php __('Officeshots.org: Test ODF compatibility:'); ?>
		<?php echo $title_for_layout; ?>
	</title>
	<?php
                $contentid = "content";

		echo $html->meta('icon');

		echo $html->css('cake.generic');
		echo $html->css('cake.override');


		echo $scripts_for_layout;

                if( isset($viewusesbootstrap)) {
		    echo $html->css('bootstrap.min');
                }
                if( isset($viewusescontentid)) {
                    $contentid = $viewusescontentid;
                }
/*
                if( isset($cachetxt)) {

		    echo $html->css('datatables.min');
		    echo '<script type="text/javascript" src="/js/datatables.min.js"></script> ';
echo '<script type="text/javascript" >';
echo '  $(document).ready(function() { ';
echo "      $('#gallery').DataTable({   ";
echo '        } );';
echo '  } );                           ';   
echo '</script>'; echo "\n";
                }
*/

	?>
</head>
<body>
	<div id="container">
		<div id="header" style="background-image: url(/img/places/icon-home.png);">
			<h1><?php echo $html->link('Officeshots', '/'); ?></h1>
			<div id="lang-select">
				<?php echo $html->image('flags/catalonia.png', array('alt' => 'Català', 'title' => 'Català', 'url' => '/lang/cat', 'class' => ($language == 'cat' ? 'active' : '')));?>
				<?php echo $html->image('flags/cn.png', array('alt' => '简体字', 'title' => '简体字', 'url' => '/lang/chi', 'class' => ($language == 'chi' ? 'active' : '')));?>
				<?php echo $html->image('flags/de.png', array('alt' => 'Deutsch', 'title' => 'Deutsch', 'url' => '/lang/deu', 'class' => ($language == 'deu' ? 'active' : '')));?>
				<?php echo $html->image('flags/gb.png', array('alt' => 'English', 'title' => 'English', 'url' => '/lang/eng', 'class' => ($language == 'eng' ? 'active' : '')));?>
				<?php echo $html->image('flags/es.png', array('alt' => 'Español', 'title' => 'Español', 'url' => '/lang/spa', 'class' => ($language == 'spa' ? 'active' : '')));?>
				<?php echo $html->image('flags/fr.png', array('alt' => 'Français', 'title' => 'Français', 'url' => '/lang/fre', 'class' => ($language == 'fre' ? 'active' : '')));?>
				<?php echo $html->image('flags/hu.png', array('alt' => 'Magyar', 'title' => 'Magyar', 'url' => '/lang/hun', 'class' => ($language == 'hun' ? 'active' : '')));?>
				<?php echo $html->image('flags/nl.png', array('alt' => 'Nederlands', 'title' => 'Nederlands', 'url' => '/lang/nld', 'class' => ($language == 'nld' ? 'active' : '')));?>
				<?php echo $html->image('flags/ru.png', array('alt' => 'Русский', 'title' => 'Русский', 'url' => '/lang/rus', 'class' => ($language == 'rus' ? 'active' : '')));?>
			</div>
			<p id="top-controllers">
				<a href="/"><?php __('Home'); ?></a>
				<a href="/galleries"><?php __('Galleries'); ?></a>
				<a href="/pages/contribute"><?php __('Contribute'); ?></a>
				<?php foreach ($topControllers as $controllerName => $controllerPath): ?>
					<a href="<?php echo $controllerPath; ?>"><?php echo $controllerName; ?></a>
				<?php endforeach; ?>
			</p>
			<br class="clearer" />
		</div>
		<div id="<?php echo $contentid; ?>">

			<?php $session->flash(); ?>

			<?php echo $content_for_layout; ?>

		</div>
		<div id="footer">
			<p id="bottom-controllers">
				<a href="/"><?php __('Home'); ?></a>
				<a href="/pages/about"><?php __('About'); ?></a>
				<a href="/pages/press"><?php __('Press'); ?></a>
				<a href="/pages/disclaimer"><?php __('Disclaimer'); ?></a>
				<?php foreach ($bottomControllers as $controllerName => $controllerPath): ?>
					<a href="<?php echo $controllerPath; ?>"><?php echo $controllerName; ?></a>
				<?php endforeach; ?>
			</p>
		</div>
	</div>
	<p id="admin-controllers">
		<?php __('Admin:');?>
		<?php if (isset($adminControllers)):?>
			<?php foreach ($adminControllers as $controllerName => $controllerPath): ?>
				<a href="<?php echo $controllerPath; ?>"><?php echo $controllerName; ?></a>
			<?php endforeach; ?>
		<?php endif;?>
	</p>
	<?php echo $cakeDebug; ?>
</body>
</html>
