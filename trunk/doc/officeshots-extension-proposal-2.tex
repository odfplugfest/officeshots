\documentclass[a4paper]{article}

\usepackage{color,graphicx}

\hyphenation{Open-Office}

\title{Officeshots Second Extension Proposal}
\author{Sander Mar\'echal, Stichting Lone Wolves\\
  \texttt{s.marechal@jejik.com}}

\begin{document}

\maketitle

The Open Document Format\footnote{http://en.wikipedia.org/wiki/OpenDocument} (ODF) is a new, vendor neutral and open standard for document exchange. ODF is currently supported by multiple office suites such as OpenOffice.org, KOffice, AbiWord, IBM Lotus Symphony and Microsoft Office. Officeshots.org is a webservice where users can upload ODF documents and check how all these applications perform using their documents.

The Officeshots project plan has been defined in ``Officeshots project proposal'' dated May 18, 2010 and ``Officeshots extension proposal'' dated November 17, 2010. This second proposal contains several enhancements to the Officeshots service.

\section{Test suite enhancements}

With the previous extensions to Officeshots, the service gained the ability to create public galleries and to do batch processing of large numbers of documents. The system administrator can upload large numbers of documents directly on the server and configure Officeshots to process these documents with all available factories. The documents and results are put into a standard public gallery. I propose several enhancements to the way Officeshots handles test suites.

\subsection{New gallery layout}
\label{sec:new-gallery}

The current gallery layout works fine for people who want to showcase their own documents but it has several drawbacks when displaying verly large sets of documents like some of the test suites. The gallery view currently limits the amount of information when more than 25 documents are displayed at once. It does this as to not overload the user with information and to save server resources. This presents a problem for test suites. Typically when a user wants to see a test suite as opposed to a gallery then he is interested in an overview of the entire suite. He is also more interested in the validity of the documents than in the results themselves.

Michiel Leenaars has created a mock-up of an alternative page layout for test suite galleries that addresses these concerns. See figure \ref{fig:testsuite-mockup}.

I propose to implement this new layout. Aside from changes to the gallery layout themselves this will require several other changes. All the existing application data will need to be updated so that office suites can be properly grouped together. I.e. instead of individually referring to OpenOffice.org Writer, Calc and Impress only OpenOffice.org should be shown. This will require changes to all running factory clients as well. Implementing this new view will also require a view caching system to speed up rendering. Currently it takes over 10 seconds to render 200 documents with 5 results each on a quad-core server. This is far too slow. With view caching the pages will only need to be regenerated when the results have been modified.

\begin{figure}
  \begin{center}
    \includegraphics{testsuite-mockup.jpg}
  \end{center}
  \caption{Test suite mock-up view}
  \label{fig:testsuite-mockup}
\end{figure}

\begin{description}
  \item[Time estimate:] 40 hours
  \item[Deadline:] April 2010
\end{description}

\subsection{Adding results manually}

Currently not all applications are supported in Officeshots. Due to technical issues it may well be that some applications are only supported at a far later date or not at all. For certain important test suites it may be desirable to still show rendering and validation results for these applications.

I propose to extend the requests module to make it possible to manually add results. This way it is possible to manually create results for applications that are hard to support but which should be displayed in a test suite. This also makes it possible to include results for fast-moving applications (e.g. development builds) during plugfests.

\begin{description}
  \item[Time estimate:] 8 hours
  \item[Deadline:] March 2010
\end{description}

\subsection{Opt-in system for test suites}

The current batch processing system takes a list of documents and generates results for all stable applications in all supported versions and all supported output formats (round-trip, PDF and screenshots). A stable application is an application that has not been marked as a development version and which has been running continuously for 24 hours. This produces a large number of results per document. Some of these results are not interesting for test suites, such as older stable versions. Some results are still missing, such as Release Candidate versions or temporary factories during ODF plugfests. Some factory owners may not want to receive a thousand test suite documents even though their application is running stable due to bandwidth or resource limitations.

I propose to add functionality to Officeshots that will allow factory owners to indicate whether they want to participate in test suites or not. This will be an opt-in system that can be set per application.

\begin{description}
  \item[Time estimate:] 6 hours
  \item[Deadline:] March 2010
\end{description}

\subsection{Manual verification of results}

Officeshots can currently validate ODF round-trip documents in various ODF validators, but a valid document is not necessarily correct. PDF output and screenshots can not be automatically verified at all. Verification of correctness is still a manual process.

I propose to add a flag to the results where users can manually mark a conversion result as valid or invalid. In order to let users track why a document is valid or invalid I will also add the same description UI to the results that currently exists for galleries and requests. The state of the manual verification will be included on the normal gallery page and on the new test suite page I proposed in section \ref{sec:new-gallery}.

\begin{description}
  \item[Time estimate:] 8 hours
  \item[Deadline:] April 2010
\end{description}

\section{Resubmitting results}

During the ODF Plugfest in The Hague several people have expressed interest in being able to chain requests. That is, to take the ODF round-trip result of a request and re-submit it back to Officeshots as a new request.

I propose to build a simple version of request chaining. The result page will get a button that resubmits the result as a new request back to the front page. From there the user can select applications and other request options as usual. This allows users to resubmit a single result at a time back as a new request. It does not allow users to automatically resubmit more that one result.

\begin{description}
  \item[Time estimate:] 12 hours
  \item[Deadline:] April 2010
\end{description}

\section{Administrator documentation}

Officeshots is currently missing documentation for the system administrators of the central server. There is ample developer documentation in the form of source code comments and a manual for factory owners on the wiki, but documentation about configuring and running the central server is missing. During the course of the previous extension proposal Officeshots has evolved from a simple PHP application that needs only a webserver into a more complex system with many moving parts. This is largely due to multiple sorts of background processing such as validators, virus scanners, the ODF Anonymiser and inclusion of batch processing.

I propose to write a Systems Administration manual alongside the Factory Owners manual that documents how to configure and run the central Officeshots server. This manual will be hosted on the wiki just like the Factory Owners manual is. In the course of writing this manual I will also add extra administration tools to Officeshots to make certain tasks easier that currently require a developer. These include administrator access to other people's factories, better administrator access to other people's requests and results and easier addition of new test suites.

\begin{description}
  \item[Time estimate:] 32 hours
  \item[Deadline:] April 2010
\end{description}

\section{Systems management}

Officeshots is currently run on a small cloud of servers provided by The Opendoc Society. These servers need ongoing maintenance. OpenIT Netherlands who currently host these servers have indicated that they would prefer it if Officeshots were transferred to the new server running in Norway.

\subsection{Moving Officeshots}

The previous extension proposal included configuring and installing Officeshots clients on the new server in Norway but it did not include moving the central Officeshots server. At that time it was anticipated that Officeshots would continue to run on the Sparc machine hosted by OpenIT. The new server will need to be configured with the appropriate software (webserver, validation software, message queues, etcetera) and Officeshots itself will need to be moved and reconfigured. This will also involve moving the other websites currently hosted on the old server, such as the Trac server\footnote{http://code.officeshots.org} and the Pootle server\footnote{http://lang.officeshots.org}. Both Trac and Pootle will be updated to the latest version during the server move. The development server is running on a different machine and does not need to be moved.

\subsection{Total}

For the coming phase of Officeshots I estimate a grand total of 40 hours work for systems management, including the above improvements. This is based on my experiences during the previous phases.

\begin{description}
  \item[Time estimate:] 40 hours
  \item[Deadline:] May 2010
\end{description}

\end{document}
