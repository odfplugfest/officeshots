Officeshots.org is an open source project as well as a pre-competitive 
effort and online service available at http://officeshots.org which was 
initiated by [OpenDoc Society](http://opendocsociety.org) with funding from the Dutch government and 
[NLnet foundation](http://nlnet.nl). 

Officeshots.org is a distributed web application that makes it easy for users 
and developers to see how office applications handle their files in 
OpenDocumentFormat. The goal is to be able to compare productivity 
solutions on the market, as well as provide access to legacy versions 
without the need to install. Officeshots provides automated tooling
for comparing and testing the quality of the output (on screen, in 
print/PDF and as input to other ODF applications).

Officeshots.org helps users test the maturity of the market in the 
context of their own documents. Especially at purchase/procurement time, 
such a service is expected to be of use. Of course, internally 
within all those application vendors it is very helpful for debugging 
and convergence as well - imagine a customer calling 'my document looks
funny' and you can check directly test in the right version on the 
right platform. 

Also check out the [http://autotests.opendocumentformat.org](ODF Autotests).

There is some useful tooling to automate access to office applications,
including [https://gitlab.com/odfplugfest/officeshots/tree/master/trunk/OfficeConvert](OfficeConvert), a tool to use Microsoft Office for 
converting legacy formats to OpenDocument Format and PDF.


How does it work
----------------

Users can submit documents and select which applications they want to see their 
document in. The server then distributes these documents to client factories 
running everywhere on the internet. They process the document and submit the 
result back.

A huge thank you to
-------------------

The original project lead was Sander Marechal and the graphical design (including 
the logo) was created by designer Nuno Pinheiro. Since then other significant 
contributions were made by:

- dr. Ben Martin 
- Jos van den Oever
- Mike Fisher, Dirk Vollmar (OfficeConvert)
- Michiel Leenaars
- Joor Loohuis 
- Aad van der Klaauw 
- David Ibáñez (ODF greeking)
- Kázmér Koleszár, Ferenc Havasi, Gábor Pálfi, Tamas Toth

Officeshots owes a lot to the developers of the various ODF validators:

- Svante Schubert (ODF Toolkit validator, https://github.com/apache/odftoolkit)
- Jakub Ondrusek (Cyclone ODF Validator, http://odfvalidator.cyclone3.org)
- dr. Alex Brown (Office-o-tron Validator, https://code.google.com/p/officeotron)

Officeshots uses the [http://cakephp.org/](Cake PHP framework).

Thanks to OpenDoc Society, NLnet Foundation, Nokia, Open-IT, the Netherlands 
overnment, Aad van der KLaauw and Petter Reinholdsen for the canonical instance 
running at: http://officeshots.org

Thanks everyone that is running or ever ran a factory, to our translators and
to the users. Making more interoperable applications starts with analysing and
understanding, thank you for contributing to a better world.