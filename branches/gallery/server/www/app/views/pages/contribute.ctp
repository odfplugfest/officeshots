<h2>How to contribute</h2>

<p>The Officeshots.org project is a pre-competitive effort started by OpenDoc Society with funding from the Dutch government
and NLnet foundation to be able to compare the quality of the output of all office suites on the market today. We provide the
software infrastructure and development support, but you can help us in a number of ways - as an individual, an organisation
or a government.

<h3>By donating or adding new office packages</h3>

<p>There is a massive amount of software out there relevant to Officeshots.org, more than can be maintained centrally in an
orderly fashion. Also, we want to offer the latest developer builds &mdash; software that is not yet released publicly &mdash; so that
people that have issues can test themselves whether a certain implementation issue or software error has been fixed. For that
we need companies and communities to add their software to Officeshots.org, which in most cases will be fairly easy as we
already have <a href="http://docs.officeshots.org/factory-guide/factory.html">a number of different backends</a>. If you want to use or write your own <a href="/xmlrpc">XML-RPC code</a>, that is
fine as well. We prefer companies to host and manage it yourself, simply so that your future customers can sample your product
through Officeshots.org in the best possible way &mdash; at its place of birth. Nobody knows your code better than you. It is
appreciated but not mandatory if you send one or more registered copies to the developers as well, so that we can test stuff
or add emergency nodes if your machines have some issue.</p>

<p>Some vendors have potentially very restrictive licensing with regards to services like this, which is solved when the software
is donated by that same company or is run by someone who has the right to do so. If you run a web or cloud office suite, everything
is already online but you can help us by providing accounts, access to the right API&#8217;s etc.</p> 

<p>Please <a href="http://lists.opendocsociety.org/mailman/listinfo/officeshots">contact us</a> if you are considering donating or
adding new office packages. We are ready to help you get started and help you find your way around the (fairly low) requirements.
And that for free.</p>

<h3>By running a factory</h3>

<p>That sounds fancy, but it is very simple and non-intrusive. Officeshots.org is distributed, meaning that you provide what we run.
If you have a machine that has some software that would be useful in Officeshots (even if some version is already in there, for instance
OpenOffice.org, Go-OO or Koffice) and if you are willing to download some simple scripts and register your machine then you can help us
provide snappier results to the users. Your computer won&#8217;t be visible from the outside, all documents are routed through our central
servers from and to the user - meaning your computer is well protected at all times.</p>

<h3>By translating Officeshots to your language</h3>

<p>We want Officeshots to be available in many different languages. We have set up Pootle, an application that makes translating
easy. You can <a href="http://lang.officeshots.org">use our Pootle server</a> to translate Officeshots in your language. You will need
to register an account there before you can start transtaling. If your language is not listed on the Pootle server yet, then
<a href="http://lists.opendocsociety.org/mailman/listinfo/officeshots">let us know</a> and we will add your language.</p>

<h3>Hardware and software donations</h3>

<p>We are very happy to receive hardware and software donations or even more convienently (funding for) hosting support - meaning you
run or donate one or more real or virtual machines, on which we can run jobs for Officeshots.org. This will allow us to add more juice
to the rendering cloud, meaning faster rendering and more different (historical) versions of software. We welcome older software as well
 - we want to offer as wide as possible an overview of the practical interoperability at the document level.</p>

