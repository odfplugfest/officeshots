<div class="groups form">
<?php echo $form->create('Group');?>
	<fieldset>
		<legend>
		<?php if ($this->action == 'admin_add') {
			__('Add Group');
		} else {
			__('Edit Group');
		}
		?>
		</legend>
	<?php
		echo $form->input('id');
		echo $form->input('name');
		echo $form->input('request_limit');
		echo $form->input('default_memberlist');
		echo $form->input('default', array('label' => __('Automatically add all registered users to this group', true)));
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
