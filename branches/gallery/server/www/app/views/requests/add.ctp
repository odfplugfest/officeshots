<script type="text/javascript">
	$(function() {
		sets = <?php echo $javascript->object($sets);?>;
		set_ext = sets['all'];
		set_output = sets['all'];

		update_apps = function() {
			set_active = $.richArray.intersect(set_ext, set_output);
			$('#applications input[type=checkbox]').each(function(i) {
				if (jQuery.inArray(this.id, set_active) > -1) {
					$(this).removeAttr('disabled');
					$('label[for=' + this.id + ']').toggleClass('disabled', false);
				} else {
					$(this).removeAttr('checked').attr('disabled', 'disabled');
					$('label[for=' + this.id + ']').toggleClass('disabled', true);
				}
			});
		}

		select_apps = function(type) {
			switch (type) {
			case 'all':
			case 'latest':
			case 'development':
				set_apps = sets[type];
				break;
			case 'none':
				set_apps = [];
				break;
			default:
				set_apps = sets['platform'][type];
				break;
			}
			
			$('#applications input[type=checkbox]:enabled').each(function(i) {
				if ($.inArray(this.id, set_apps) > -1) {
					$(this).attr('checked', 'checked');
				} else {
					$(this).removeAttr('checked');
				}
			});
		}

		$('#applications').accordion({
			header: 'h3',
			autoHeight: true
		});

		$('#RequestFileUpload').change(function() {
			ext = $('#RequestFileUpload').val();
			ext = ext.substr(ext.lastIndexOf('.') + 1);

			if (ext == '') {
				set_ext = sets['all'];
			} else {
				$('#applications').accordion('activate', 'h3.' + ext);
				set_ext = sets['extension'][ext];
			}

			update_apps();
		});

		$("input[type=radio]").click(function() {
			var id = $("input[type=radio]:checked").val();

			if (id == '') {
				set_output = sets['all'];
			} else {
				set_output = sets['format'][id];
			}

			update_apps();
		});

		select_apps('latest');
	});
</script>

<?php
	$javascript->link('jquery-1.3.2.min.js', false);
	$javascript->link('jquery-ui-accordion-1.6rc6.min.js', false);
	$javascript->link('jquery.rich-array-min.js', false);
?>

<div id="request-add">
	<?php echo $form->create('Request', array('type' => 'file')); ?>
	<fieldset id="upload">
 		<legend><?php __('Upload your documents!');?></legend>
		<?php
			if ($can_submit_requests) {
				echo $form->file('Request.FileUpload');
				echo $form->button(__('Upload', true), array('type' => 'submit', 'id' => 'RequestFileSubmit'));
			} else {
				echo '<p>' . __('Officeshots is in open beta. You need to <a href="/users/login">login</a> or <a href="/users/register">register</a> before you can upload documents.', true) . '</p>';
				echo $form->file('Request.FileUpload', array('disabled' => true));
				echo $form->button(__('Upload', true), array('type' => 'submit', 'disabled' => true, 'id' => 'RequestFileSubmit'));
			}
		?>
		<div class="input radio" id="RequestFormatId">
			<label><?php __('Output format:'); ?></label>
			<ul id="RequestFormatIdList">
				<li>
				<input type="radio" name="data[Request][format_id]" id="RequestFormatIdNone" value="" checked="checked" />
				<label for="RequestFormatIdNone"><?php __('Does not matter'); ?></label>
				</li>
			<?php
				foreach ($formats as $id => $name) {
					echo '<li><input type="radio" name="data[Request][format_id]" id="RequestFormatId' . $id . '" value="' . $id . '" />';
					echo ' <label for="RequestFormatId' . $id . '">' . $name . '</label></li>';
				}
			?>
			</ul>
		</div>
		<?php
			if ($can_have_factories) {
				echo $form->input('own_factory', array('label' => __('Only use my own factories to process this request.', true)));
			}
		?>
	</fieldset>
	<fieldset id="applications">
		<legend><?php __('Choose applications'); ?></legend>
		<div id="applications-container">

		<span id="select">
			<?php __('Select:')?> <a href="javascript:select_apps('all')"><?php __('All');?></a> -
			<a href="javascript:select_apps('none')"><?php __('None');?></a> -
			<a href="javascript:select_apps('latest')"><?php __('Latest');?></a> -
			<a href="javascript:select_apps('development')"><?php __('Upcoming');?></a>
			<?php foreach ($platforms as $platform): ?>
				- <a href="javascript:select_apps('<?php echo $platform['Platform']['id'];?>')"><?php echo $platform['Platform']['name'];?></a>
			<?php endforeach; ?>
		</span>
		<?php foreach ($doctypes as $doctype): ?>
		<div>
			<h3 class="<?php echo implode(' ', Set::extract('/Mimetype[doctype_id='.$doctype['Doctype']['id'].']/extension', $mimetypes)); ?>">
				<?php echo $doctype['Doctype']['name'];?>
			</h3>
			<div style="margin-bottom: 0;">
			<table>
				<tr>
					<?php
					$width = (int) 100 / sizeof($platforms);
					foreach ($platforms as $platform) {
						echo '<th style="width: '.$width.'%"><span class="platform">' . $platform['Platform']['name'] . '</span></th>';
					}
					?>
				</tr>
				<tr>
					<?php foreach ($platforms as $platform): ?>
					<td>
						<?php 
							foreach($workers as $worker) {
								if ($worker['Doctype']['code'] == $doctype['Doctype']['code'] && $worker['Platform']['name'] == $platform['Platform']['name']) {
									echo "\n" . '<input type="checkbox" name="data[Request][App][]" id="' . $worker['short_id'] . '" value="' . $worker['id'] . '" />'
										. "\n" . '<label for="' . $worker['short_id'] . '">'
										. $worker['Application']['name'] . ' '
										. $worker['Worker']['version']
										. "</label>\n";
								}
							} 
						?>
					</td>
					<?php endforeach;?>
				</tr>
			</table>
			</div>
		</div>
		<?php endforeach; ?>

		</div>
	</fieldset>
	<?php echo $form->end(); ?>
</div>

<p><?php __('You want to try which Office software is any good for you or your organisation? It&#8217;s very easy. Five steps:');?></p>
<ol>
	<li><?php __('Submit a document that contains features you typically require.');?></li>
	<li><?php __('Take a pick from the supported word processors, spreadsheet and presentation packages.');?></li>
	<li><?php __('Select what you would like to see: PDF exports, screen output or ODF roundtrips.');?></li>
	<li><?php __('Wait a bit while we work our magic...');?></li>
	<li><?php __('Grab the results and compare them!');?></li>
</ol>
