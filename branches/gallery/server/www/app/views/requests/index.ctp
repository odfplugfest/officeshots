<div class="requests index">
<h2><?php __('Your requests');?></h2>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('filename');?></th>
	<th><?php echo $paginator->sort('Uploaded', 'created');?></th>
	<th><?php echo $paginator->sort('Status', 'state');?></th>
	<th><?php __('Completed'); ?></th>
</tr>
<?php
$i = 0;
foreach ($requests as $request):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $html->link($request['Request']['filename'], array('action'=>'view', $request['Request']['id'])); ?>
		</td>
		<td>
			<?php echo $request['Request']['created']; ?>
		</td>
		<td>
			<?php echo $requestModel->getState($request); ?>
		</td>
		<td>
			<?php
				printf(__('%d/%d jobs', true), $request['Request']['result_count'], $request['Request']['job_count']);
			?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Create a new request', true), array('action'=>'add')); ?></li>
	</ul>
</div>
