<div class="factories view">
<h2><?php  printf(__('Factory "%s"', true), $factory['Factory']['name']);?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Owner'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $html->link($factory['User']['name'], array('controller'=> 'users', 'action'=>'view', $factory['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Operatingsystem'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $factory['Operatingsystem']['name']; ?>
			<?php echo $factory['Operatingsystem']['version']; ?>
			<?php if ($factory['Operatingsystem']['codename']): ?>
				(<?php echo $factory['Operatingsystem']['codename']; ?>)
			<?php endif; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Hardware'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $factory['Factory']['hardware']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Last activity'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $factory['Factory']['last_poll']; ?>
			&nbsp;
		</dd>
	</dl>
	<?php if ($isOwner): ?>
		<p><?php echo $html->link(__('Edit this factory', true), array('controller'=> 'factories', 'action'=>'edit', $factory['Factory']['id']));?> </li>
	<?php endif; ?>
</div>
<div class="related">
	<h3><?php __('Installed office applications');?></h3>
	<?php if (!empty($factory['Worker'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Application'); ?></th>
		<th><?php __('Version'); ?></th>
		<th><?php __('Development'); ?></th>
		<th><?php __('Output formats'); ?></th>
		<?php if ($isOwner): ?>
			<th class="actions"><?php __('Actions');?></th>
		<?php endif; ?>
	</tr>
	<?php
		$i = 0;
		foreach ($factory['Worker'] as $worker):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $worker['Application']['name'];?></td>
			<td><?php echo $worker['version'];?></td>
			<td><?php if ($worker['development']) { __('Unstable'); } else { __('Stable'); } ?></td>
			<td><?php echo implode(', ', Set::extract('/Format/name', $worker));?></td>
			<?php if ($isOwner): ?>
				<td class="actions">
					<?php echo $html->link(__('Edit', true), array('controller'=> 'workers', 'action'=>'edit', $worker['id'])); ?>
					<?php echo $html->link(__('Delete', true), array('controller'=> 'workers', 'action'=>'delete', $worker['id']), null, sprintf(__('Are you sure you want to delete this instance of %s from this factory?', true), $worker['Application']['name'])); ?>
				</td>
			<?php endif; ?>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<?php if ($isOwner): ?>
		<p><?php echo $html->link(__('Add an application', true), array('controller'=> 'workers', 'action'=>'add', $factory['Factory']['id']));?> </li>
	<?php endif; ?>
</div>
