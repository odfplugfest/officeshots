<div class="factories index">
<h2><?php __('Factories');?></h2>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('user_id');?></th>
	<th><?php echo $paginator->sort('operatingsystem_id');?></th>
	<th><?php echo $paginator->sort('name');?></th>
	<th><?php echo $paginator->sort('hardware');?></th>
	<th><?php echo $paginator->sort('last_poll');?></th>
	<th><?php echo $paginator->sort('created');?></th>
	<th><?php echo $paginator->sort('modified');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($factories as $factory):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $factory['Factory']['id']; ?>
		</td>
		<td>
			<?php echo $html->link($factory['User']['email_address'], array('controller'=> 'users', 'action'=>'view', $factory['User']['id'])); ?>
		</td>
		<td>
			<?php echo $html->link($factory['Operatingsystem']['name'], array('controller'=> 'operatingsystems', 'action'=>'view', $factory['Operatingsystem']['id'])); ?>
		</td>
		<td>
			<?php echo $factory['Factory']['name']; ?>
		</td>
		<td>
			<?php echo $factory['Factory']['hardware']; ?>
		</td>
		<td>
			<?php echo $factory['Factory']['last_poll']; ?>
		</td>
		<td>
			<?php echo $factory['Factory']['created']; ?>
		</td>
		<td>
			<?php echo $factory['Factory']['modified']; ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('View', true), array('action'=>'view', $factory['Factory']['id'])); ?>
			<?php echo $html->link(__('Edit', true), array('action'=>'edit', $factory['Factory']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action'=>'delete', $factory['Factory']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $factory['Factory']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New Factory', true), array('action'=>'add')); ?></li>
		<li><?php echo $html->link(__('List Users', true), array('controller'=> 'users', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New User', true), array('controller'=> 'users', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Operatingsystems', true), array('controller'=> 'operatingsystems', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Operatingsystem', true), array('controller'=> 'operatingsystems', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Workers', true), array('controller'=> 'workers', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Worker', true), array('controller'=> 'workers', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Jobs', true), array('controller'=> 'jobs', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Job', true), array('controller'=> 'jobs', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Results', true), array('controller'=> 'results', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Result', true), array('controller'=> 'results', 'action'=>'add')); ?> </li>
	</ul>
</div>
