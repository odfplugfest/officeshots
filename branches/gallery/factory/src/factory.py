#!/usr/bin/env python
# Officeshots.org - Test your office documents in different applications
# Copyright (C) 2009 Stichting Lone Wolves
# Written by Sander Marechal <s.marechal@jejik.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
This is the core Factory class and main method of the Officeshots
factory. Execute it with -h or --help to see the available options
"""

import os
import sys
import time
import socket
import logging
import ConfigParser

from optparse import OptionParser
from backends import BackendException
from xmlrpclib import ServerProxy, Error, Fault

LOGLEVELS = {'debug': logging.DEBUG,
             'info': logging.INFO,
	     'warning': logging.WARNING,
	     'error': logging.ERROR,
	     'critical': logging.CRITICAL}

class Factory:
	"""
	The core factory class communicates with the Officeshots server and passes
	requests on to any of the available workers
	"""

	def configure(self, options):
		self.options = options
		self.config = ConfigParser.RawConfigParser()
		self.config.read(os.path.abspath(self.options.config_file))

		# Configure logging
		if self.options.debug:
			logging.basicConfig(format = self.config.get('global', 'log_format'), level = logging.DEBUG)
		else:
			level = LOGLEVELS.get(self.config.get('global', 'log_level'), logging.NOTSET)
			try:
				logging.basicConfig(
						format = self.config.get('global', 'log_format'),
						filename = self.config.get('global', 'log_file'),
						filemode = 'a',
						level = level
				)
			except IOError, e:
				print "Logfile IO error. Please make sure that the log_file setting is correct in config.ini"
				sys.exit(1)

		# Load factory name
		self.name = self.config.get('global', 'factory_name')

		# Configure the XMLRPC proxy
		transport_name = self.config.get('global', 'transport')
		transport = self.load('transports.' + transport_name, 'SSLTransport')
		if transport is None:
			print "Transport %s could not be loaded" % transport_name
			sys.exit(1)

		transport = transport(
			self.config.get('global', 'tls_key_file'),
			self.config.get('global', 'tls_certificate_file')
		)
		self.proxy = ServerProxy(self.config.get('global', 'xmlrpc_endpoint'), transport=transport, verbose=self.options.debug)

		# Load all the backends
		self.backends = []
		sections = [s.strip() for s in self.config.get('global', 'backends').split(',')]

		for section in sections:
			backend_name = self.config.get(section, 'backend')
			if backend_name is None:
				continue

			backend = self.load('backends.' + backend_name.lower(), backend_name)
			if backend is None:
				continue

			backend = backend(self.options, self.config, section)
			try:
				backend.initialize()
			except BackendException, e:
				logging.warning('Error initializing backend %s for %s: ' + str(e), backend_name, section)
				continue

			self.backends.append(backend)

		if len(self.backends) == 0:
			logging.critical('No backends could be loaded')
			return False

		# Configuration succeeded
		return True

	def load(self, package, class_name):
		"""
		A convenience function to import class_name from package
		"""
		try:
			module = __import__(package, globals(), locals(), class_name)
		except ImportError, e:
			logging.warning('Error importing %s from %s. ' + str(e), class_name, package)
			print_stack()
			return None
		
		return getattr(module, class_name)



	def systemload(self):
		"""
		Return the average system load
		"""
		try:
			return max(os.getloadavg())
		except (AttributeError, OSError):
			return None

	def loop(self):
		"""
		A single iteration of the main loop.
		Return False to terminate the application
		"""
		# Keep an eye on system load
		load = self.systemload()
		maxload = self.config.getfloat('global', 'load_max')
		if load > maxload:
			logging.debug("Systemload %.2f exceeds limit %.2f. Sleeping." % (load, maxload))
			time.sleep(60)
			return True

		# Poll for a job. Sleep for a minute if there's no work
		try:
			job = self.proxy.jobs.poll(self.name)
		except socket.error, ex:
			logging.warning(ex)
			logging.warning("Cannot connect to server. Sleeping.")
			time.sleep(60)
			return True
		except Fault, ex:
			logging.error("XML-RPC fault. Poll failed. Sleeping.")
			time.sleep(60)
			return True

		if len(job) == 0:
			logging.debug('No jobs found. Sleeping.')
			time.sleep(60)
			return True

		# We have work. Find a backend to pass it off to
		for backend in self.backends:
			if backend.can_process(job):
				try:
					(format, document) = backend.process(job)
				except BackendException, ex:
					logging.warning(ex)
					if not ex.recoverable:
						logging.warning('Removing backend')
						self.backends.remove(backend)
						if len(self.backends) == 0:
							logging.critical('No more active backends.')
							return False
					return True

				try:
					self.proxy.jobs.finish(self.name, job['job'], format, document)
				except socket.error, ex:
					logging.warning("Cannot connect to server. Job cannot be finished. Sleeping.")
					time.sleep(60)
					return True
				except Fault, ex:
					logging.error("XML-RPC fault. Finishing failed. Sleeping.")
					time.sleep(60)
					return True

				logging.info('Processed job %s', job['job'])
				return True

		logging.warning('No suitable backend found for job')
		# TODO: Do something smart about that, like deactivating the related worker on the server
		return True


	def run(self):
		"""
		This is the main execution loop
		"""
		logging.info('Starting factory server.')
		while self.loop():
			pass



if __name__ == "__main__":
	parser = OptionParser(usage='Usage: %prog [options]')
	parser.add_option('-c', '--config-file', action='store', type='string', dest='config_file',
			default='../conf/config.ini', help='Full path to the configuration file to read.')
	parser.add_option('-d', '--debug', action='store_true', dest='debug',
			help='When in debug mode all errors will be written to the console and logging will be set to debug.')
	(options, args) = parser.parse_args()

	factory = Factory()
	if factory.configure(options):
		factory.run()
