# Officeshots.org - Test your office documents in different applications
# Copyright (C) 2009 Stichting Lone Wolves
# Written by Sander Marechal <s.marechal@jejik.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
This module provides SSLTransport in order to have SSL client authentication
with the standard xmlrpclib. It is based on the M2Crypto library
"""

from M2Crypto import m2xmlrpclib, SSL

class SSLTransport(m2xmlrpclib.SSL_Transport):
	"""
	An SSL-capable transport based off M2Crypto's SSL_Transport
	"""

	def __init__(self, key_file, cert_file):
		ctx = SSL.Context()
		ctx.load_cert(key_file, cert_file)
		m2xmlrpclib.SSL_Transport.__init__(self, ctx)
	
	def request(self, host, handler, request_body, verbose=0):
		"""
		Override m2xmlrpclib.request() because it cannot handle an URL
		without a port number
		"""
		if host.find(':') == -1:
			host = host + ':443'

		return m2xmlrpclib.SSL_Transport.request(self, host, handler, request_body, verbose)
