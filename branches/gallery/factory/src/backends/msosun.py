# Officeshots.org - Test your office documents in different applications
# Copyright (C) 2009 Stichting Lone Wolves
# Written by Sander Marechal <s.marechal@jejik.com>

"""
This module is a back-end for the Microsoft Office 2000, 2003 and 2007
using COM and the Sun plugin on Windows.
"""

import os
import time
import logging
import subprocess
import win32com.client
import pythoncom

from backends import Backend, BackendException

class MSOSunException(BackendException):
	def __str__(self):
		return 'MSOSunException: ' + BackendException.__str__(self)

class MSOWord:
	"""
	Save or print a document with MS-Word
	"""

	Word = None
	Document = None

	def open(self, file):
		pythoncom.CoInitializeEx(pythoncom.COINIT_APARTMENTTHREADED)
		self.Word = win32com.client.DispatchEx('Word.Application')
		self.Document = self.Word.Documents.Open(file, False, False, False)
	
	def saveAs(self, file):
		self.Document.SaveAs(file)
		self.quit()
	
	def printOut(self, printer, file):
		self.Document.Saved = 1
		self.Word.Application.NormalTemplate.Saved = 1
		self.Word.Application.ActivePrinter = printer
		
		self.Word.PrintOut(True, False, 0, file)
		while self.Word.BackgroundPrintingStatus > 0:
			time.sleep(0.1)

		self.quit()
	
	def quit(self):
		self.Document.Close()
		self.Word.Quit()
		del self.Document
		del self.Word
		pythoncom.CoUninitialize()

class MSOExcel:
	"""
	Save or print a document with MS-Excel
	"""

	Excel = None
	Document = None

	def open(self, file):
		pythoncom.CoInitializeEx(pythoncom.COINIT_APARTMENTTHREADED)
		self.Excel = win32com.client.DispatchEx('Excel.Application')
		self.Excel.AskToUpdateLinks = 0
		self.Document = self.Excel.Workbooks.Open(file, 0, False, 2)
	
	def saveAs(self, file):
		self.Document.SaveAs(file)
		self.quit()
	
	def printOut(self, printer, file):
		self.Document.Saved = 1
		self.Excel.PrintOut(1, 5000, 1, False, printer, True, False, file)
		self.quit()
	
	def quit(self):
		self.Document.Close()
		self.Excel.Quit()
		del self.Document
		del self.Excel
		pythoncom.CoUninitialize()

class MSOPowerpoint:
	"""
	Save or print a document with MS-Powerpoint
	"""

	Powerpoint = None
	Document = None

	def open(self, file):
		pythoncom.CoInitializeEx(pythoncom.COINIT_APARTMENTTHREADED)
		self.Powerpoint = win32com.client.DispatchEx('Powerpoint.Application')
		self.Document = self.Powerpoint.Presentations.Open(file, False, False, False)
	
	def saveAs(self, file):
		self.Document.SaveAs(file)
		self.quit()
	
	def printOut(self, printer, file):
		self.Document.PrintOptions.PrintInBackground = 0
		self.Document.PrintOptions.ActivePrinter = printer
		self.Document.Saved = 1
		self.Excel.PrintOut(1, 5000, file, 0, False)
		self.quit()
	
	def quit(self):
		self.Document.Close()
		self.Powerpoint.Quit()
		del self.Document
		del self.Powerpoint
		pythoncom.CoUninitialize()

APP_BY_DOCTYPE = {
	"odt": MSOWord,
	"ods": MSOExcel,
	"odp": MSOPowerpoint
}

class MSOSun(Backend):
	printer = None

	def initialize(self):
		if self.config.has_option(self.section, 'printer'):
			self.printer = self.config.get(self.section, 'printer')

	def process(self, job):
		if (job['format'] == ''):
			job['format'] = 'odf'

		src_file = self.save_document(job)
		(root, ext) = os.path.splitext(src_file)

		if job['format'] == 'pdf':
			ext = '.ps'

		dst_file = root + '_result' + ext

		try:
			App = APP_BY_DOCTYPE[job['doctype']]()
		except KeyError:
			raise MSOSunException("Unknown input doctype: '%s'" % doctype, True)

		App.open(src_file)
		if job['format'] == 'odf':
			App.saveAs(dst_file)
		else:
			if not self.printer:
				raise MSOSunException("No PDF printer specified", True)
			App.printOut(self.printer, dst_file)
			dst_file = self.ps2pdf(dst_file)

		logging.info('MSOSun converted %s to %s' % (src_file, dst_file))
		contents = self.load_document(dst_file)
		os.unlink(src_file)
		os.unlink(dst_file)

		return (job['format'], contents)

	def ps2pdf(self, ps_file):
		(root, ext) = os.path.splitext(ps_file)
		pdf_file = root + '.pdf'

		command = 'ps2pdf.bat' + ' "' + ps_file + '" "' + pdf_file + '"'
		try:
			p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
			stdout, stderr = p.communicate()
		except OSError, ex:
			raise MSOSunException('The command `%s` generated an OSError (%s).' % (command, str(ex)), True)

		if p.returncode > 0:
			raise MSOSunException('The command `%s` returned with a non-zero exit status (%s, %s).' % (command, stdout, stderr), True)
		os.unlink(ps_file)
		return pdf_file
	
		
