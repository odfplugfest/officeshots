<div class="requests form">
<?php echo $form->create('Request');?>
	<fieldset>
 		<legend><?php __('Edit Request');?></legend>
	<?php
		echo $form->input('id');
		echo $form->input('user_id');
		echo $form->input('ip_address');
		echo $form->input('format_id', array('empty' => true));
		echo $form->input('filename');
		echo $form->input('path');
		echo $form->input('mimetype_id');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action'=>'delete', $form->value('Request.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('Request.id'))); ?></li>
		<li><?php echo $html->link(__('List Requests', true), array('action'=>'index'));?></li>
		<li><?php echo $html->link(__('List Users', true), array('controller'=> 'users', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New User', true), array('controller'=> 'users', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Doctypes', true), array('controller'=> 'doctypes', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Doctype', true), array('controller'=> 'doctypes', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Jobs', true), array('controller'=> 'jobs', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Job', true), array('controller'=> 'jobs', 'action'=>'add')); ?> </li>
	</ul>
</div>
