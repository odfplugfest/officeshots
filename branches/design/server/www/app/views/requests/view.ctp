<?php
	if ($request['Request']['state'] == Request::STATE_SCAN_QUEUED || $request['Request']['state'] == Request::STATE_QUEUED) {
		$html->meta(null, null, array( 'http-equiv' => 'refresh', 'content' => Configure::read('Request.meta_refresh')), false);
	}
?>
<div class="requests view">
<h2><?php printf(__('Filename "%s"', true), $request['Request']['filename']);?></h2>
	<?php if ($request['Request']['state'] == Request::STATE_SCAN_FOUND): ?>
		<img src="/img/icons/virus.png" alt="" style="float: left;" />
	<?php else: ?>
		<img src="/img/icons/<?php echo $request['Mimetype']['icon'];?>" alt="" style="float: left;" />
	<?php endif; ?>
	<dl style="margin-left: 10em;"><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Uploaded'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $request['Request']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Status'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $requestModel->getState($request); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Document type'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $request['Mimetype']['Doctype']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Actions'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php
				if ($request['Request']['state'] == Request::STATE_SCAN_FOUND) {
					echo $html->link(
						__('Download all results', true),
						array('action' => 'download', $request['Request']['id']),
						array(),
						sprintf(__('This document contains the "%s" virus. Are you sure?', true), $request['Request']['state_info'])
					);
				} else {
					echo $html->link(__('Download all results', true), array('action' => 'download', $request['Request']['id']));
				}
			?>
			&nbsp;
		</dd>
	</dl>
</div>

<?php if (!empty($request['Job'])):?>
<div class="related">
	<h3><?php __('Results');?></h3>
	<?php foreach ($request['Job'] as $job): ?>
		<div style="width: 200px; height: 250px; float: left; text-align: center; border: 1px solid #ccc; margin-right: 1em;">
			<?php echo $jobModel->getIcon($job, $request['Request']['state']); ?><br />
			<?php echo $jobModel->getDescription($job, $request['Request']['state']); ?><br />
			<p>
				<?php echo $job['Application']['name'] . ' ' . $job['version'];?>
				(<?php echo $job['Platform']['name'];?>)
			</p>
			<?php if (!empty($job['Result']) && $canDeleteResults) {
				echo '<p>' . $html->link(__('Delete', true), array('controller' => 'results', 'action' => 'delete', $job['Result']['id'])) . '</p>';
			}?>
		</div>
	<?php endforeach; ?>
</div>
<?php endif; ?>
