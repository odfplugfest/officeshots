<?php

/**
 * The global AppController.
 *
 * Takes care of authentication for all controllers
 *
 * The authentication model comes from Studio Canaria:
 * http://www.studiocanaria.com/articles/cakephp_auth_component_users_groups_permissions_revisited
 */
class AppController extends Controller
{
	/**
	 * @var $components array Array of components to load for every controller in the application
	 */
	public $components = array('AuthCert', 'Session', 'Cookie');

	/**
	 * Application hook which runs prior to each controller action
	 */
	public function beforeFilter()
	{
		//Override default fields used by Auth component
		$this->AuthCert->fields = array('username'=>'email_address','password'=>'password');
		//Set application wide actions which do not require authentication
		$this->AuthCert->allow(array());
		//Set the default redirect for users who logout
		$this->AuthCert->logoutRedirect = '/';
		//Set the default redirect for users who login
		$this->AuthCert->loginRedirect = '/';
		//The error displayed when a login error occurs
		$this->AuthCert->loginError = __('Login failed. Wrong e-mail address or password.', true);
		//Extend auth component to include authorisation via isAuthorized action
		$this->AuthCert->authorize = 'controller';
		//Restrict access to only users with an active account
		$this->AuthCert->userScope = array('User.active = 1');
		//Pass auth component data over to view files
		$this->set('Auth',$this->AuthCert->user());
	}

	/**
	 * Application hook which runs after each action but, before the view file is 
	 * rendered
	 */
	public function beforeRender()
	{
		//If we have an authorised user logged then pass over an array of controllers
		//to which they have index action permission
		$topControllers = array();
		$bottomControllers = array(
			__('Factories', true) => '/factories',
		);

		if ($this->AuthCert->user()) {
			$bottomControllers[__('Requests', true)] = '/requests';
			$bottomControllers[__('Your account', true)] = '/users/view';
		}

		$bottomControllers[__('Manuals', true)] = 'http://code.officeshots.org/trac/officeshots/wiki/Documentation';

		if ($this->__permitted('users', 'admin_index')) {
			$bottomControllers[__('Users', true)] = '/admin/users';
			$bottomControllers[__('Groups', true)] = '/admin/groups';
		}

		if (!$this->AuthCert->hasCert) {
			if ($this->AuthCert->user()) {
				$topControllers[__('Logout', true)] = '/users/logout';
			} else {
				$topControllers[__('Login', true)] = '/users/login';
			}
		}

		// The currently set language
		$language = $this->__getLanguage();


		$this->set(compact('topControllers', 'bottomControllers', 'language'));
	}

	/**
	 * Called by Auth component for establishing whether the current authenticated 
	 * user has authorization to access the current controller:action
	 * 
	 * @return true if authorised/false if not authorized
	 */
	public function isAuthorized()
	{
		return $this->__permitted($this->name, $this->action);
	}

	/**
	 * Helper function returns true if the currently authenticated user has permission 
	 * to access the controller:action specified by $controllerName:$actionName
	 *
	 * @param $controllerName Object
	 * @param $actionName Object
	 * @return 
	 */
	public function __permitted($controllerName, $actionName)
	{
		if (!$user_id = $this->AuthCert->user('id')) {
			return false;
		}

		//Ensure checks are all made lower case
		$controllerName = low($controllerName);
		$actionName = low($actionName);
		
		//If permissions have not been cached to session...
		if (!$this->Session->check('Permissions')) {
			//...then build permissions array and cache it
			
			// Set the global permissions for all users that are logged in
			$permissions = array(
				'users:logout',
				'users:index',
				'users:view',
				'users:edit',
				'requests:index',
				'requests:view',
				'requests:add',
				'requests:download',
				'results:view',
				'results:download',
			);

			//Import the User Model so we can build up the permission cache
			App::import('Model', 'User');
			$thisUser = new User();

			//Now bring in the current users full record along with groups
			$thisGroups = $thisUser->find(array('User.id' => $user_id));
			$thisGroups = $thisGroups['Group'];
			foreach ($thisGroups as $thisGroup) {
				$thisPermissions = $thisUser->Group->find(array('Group.id' => $thisGroup['id']));
				$thisPermissions = $thisPermissions['Permission'];
				foreach ($thisPermissions as $thisPermission) {
					$permissions[] = $thisPermission['name'];
				}
			}

			//write the permissions array to session
			$this->Session->write('Permissions', $permissions);
		} else {
			//...they have been cached already, so retrieve them
			$permissions = $this->Session->read('Permissions');
		}

		//Now iterate through permissions for a positive match
		foreach ($permissions as $permission) {
			if ($permission == '*') {
				return true; //Super Admin Bypass Found
			}
			if ($permission == $controllerName.':*') {
				return true; //Controller Wide Bypass Found
			}
			if ($permission == $controllerName.':'.$actionName) {
				return true; //Specific permission found
			}
		}
		return false;
	}

	/**
	 * Set the language based on the session or a cookie
	 */
	private function __setLanguage()
	{
		if ($this->Cookie->read('lang') && !$this->Session->check('Config.language')) {
			$this->Session->write('Config.language', $this->Cookie->read('lang'));
		}
	}

	/**
	 * Get the language based on the session or a cookie
	 */
	private function __getLanguage()
	{
		if ($language = $this->Cookie->read('lang')) {
			return $language;
		}
		
		if ($language = $this->Session->read('Config.language')) {
			return $language;
		}

		return Configure::read('Config.language');
	}
}

?>
