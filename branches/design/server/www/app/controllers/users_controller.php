<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Controller for the User model
 */
class UsersController extends AppController
{
	/** @var array The components this controller uses */
	public $components = array('Email', 'AuthCert');
	
	/** @var array The helpers that will be available on the view */
	public $helpers = array('Html', 'Form');

	/** @var array The models that this controller uses */
	public $uses = array('User', 'Group');

	/**
	 * Set the auth permissions for this controller
	 * @return void
	 */
	function beforeFilter()
	{
		parent::beforeFilter();
		$this->AuthCert->allow('login', 'recover', 'reset');

		if (Configure::read('Auth.allowRegister')) {
			$this->AuthCert->allow('register', 'activate');
		}
	}

	/**
	 * Login for the user. Empty because it is automagically handled by the Auth class.
	 * @return void
	 */
	public function login() {}

	/**
	 * Remove session permission data and logout
	 * @return void
	 */
	public function logout()
	{
		$this->Session->del('Permissions');
		$this->redirect($this->AuthCert->logout());
	}

	/**
	 * Display a login/register page or redirect to view() for logged in users
	 * @return void
	 */
	public function index()
	{
		if ($this->AuthCert->user('id')) {
			$this->redirect(array('action' => 'view'));
		}

		$this->redirect(array('action' => 'login'));
	}

	/**
	 * Display the user's own account page
	 * @return void
	 */
	public function view($id = null)
	{
		if ($id == null) {
			$id = $this->AuthCert->user('id');
		}

		$isSelf = ($id == $this->AuthCert->user('id'));
		$this->set(array(
			'isSelf' => $isSelf,
			'canAddFactories' => ($isSelf && $this->__permitted('factories', 'add')),
			'user' => $this->User->read(null, $id),
		));
	}

	/**
	 * Register for a new account
	 * @return void
	 */
	public function register()
	{
		if (empty($this->data)) {
			return;
		}

		if ($this->User->find('first', array('conditions' => array('User.email_address' => $this->data['User']['email_address'])))) {
			$this->Session->setFlash(__('An account already exists with that e-mail address.', true));
			return;
		}

		if ($file = Configure::read('Auth.limitRegister')) {
			$addresses = file($file, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

			if (!is_array($addresses)) {
				$this->Session->setFlash(__('Could not verify your e-mail address. Please contact the administrators.', true));
				return;
			}

			if (!in_array($this->data['User']['email_address'], $addresses)) {
				$this->Session->setFlash(__('Sorry, but your e-mail address does not appear on the memberlist of the OpenDoc Society. Only OpenDoc Society members can participate in the private beta phase.', true));
				return;
			}
		}

		$this->User->create();
		if ($this->User->save($this->data)) {
			$user_id = $this->User->getLastInsertID();

			$this->Group->add_default_member($user_id);
			$this->_sendActivationEmail($user_id);

			$this->Session->setFlash(__('A registration verification e-mail has been sent. After your account has been activated you can login.', true));
			$this->redirect(array('action' => 'login'));
		} else {
			$this->Session->setFlash(__('The account could not be registered. Please, try again.', true));
		}
	}

	/**
	 * Account activation from the link in the e-mail
	 *
	 * @param string $id The user ID to activate
	 * @param string $hash The user activation hash
	 * @return void
	 */
	public function activate($id = null, $hash = null)
	{
		if ($id && $hash) {
			$this->User->id = $id;
			if ($this->User->activate($hash)) {
				$this->Session->setFlash(__('Your account has been activated. You can now login.', true));
				$this->redirect(array('action' => 'login'));
			}
		}

		$this->Session->setFlash(__('Your account was *not* activated. Please, try again or re-register.', true));
		$this->redirect(array('action' => 'register'));
	}

	/**
	 * Send an activation e-mail to the specified user.
	 *
	 * @param string $id The ID of the user to send the e-mail to
	 * @return boolean
	 */
	private function _sendActivationEmail($id = null)
	{
		if (!$id) {
			return false;
		}

		$this->User->id = $id;
		$this->User->read();

		$this->set('username', $this->User->field('name'));
		$this->set('activation_url', Router::url(array(
			'controller' => 'users',
			'action' => 'activate',
			'id' => $this->User->field('id'),
			$this->User->getActivationHash()
		), true));
		$this->set('base_url', Router::url('/', true));

		$this->Email->to = $this->User->field('email_address');
		$this->Email->subject = __('Please confirm your registration', true);
		$this->Email->from = Configure::read('Email.admin');
		$this->Email->template = 'activate';
		$this->Email->sendAs = 'text';
		$this->Email->delivery = 'mail'; // debug or mail
		return $this->Email->send();
	}

	/**
	 * Password recovery form
	 */
	public function recover()
	{
		if ($this->AuthCert->user('id')) {
			$this->redirect(array('action' => 'edit'));
		}

		if (!empty($this->data)) {
			$user = $this->User->find('first', array('conditions' => array('User.email_address' => $this->data['User']['email_address'])));
			if (isset($user['User']['id'])) {
				$this->_sendRecoveryEmail($user['User']['id']);
				$this->Session->setFlash(__('An e-mail has been sent containing instructions to reset your password.', true));
				return;
			}

			$this->Session->setFlash(__('That e-mail address does not belong to a registered user.', true));
		}
	}

	/**
	 * Send a password recovery e-mail to the specified user.
	 *
	 * @param string $id The ID of the user to send the e-mail to
	 * @return boolean
	 */
	private function _sendRecoveryEmail($id = null)
	{
		if (!$id) {
			return false;
		}

		$this->User->id = $id;
		$this->User->read();

		$this->set('username', $this->User->field('name'));
		$this->set('reset_url', Router::url(array(
			'controller' => 'users',
			'action' => 'reset',
			'id' => $this->User->field('id'),
			$this->User->getRecoveryHash()
		), true));
		$this->set('base_url', Router::url('/', true));

		$this->Email->to = $this->User->field('email_address');
		$this->Email->subject = __('Your password recovery request', true);
		$this->Email->from = Configure::read('Email.admin');
		$this->Email->template = 'recover';
		$this->Email->sendAs = 'text';
		$this->Email->delivery = 'mail'; // debug or mail
		return $this->Email->send();
	}

	/**
	 * Reset a user's password from the e-mail link in the password recovery e-mail
	 *
	 * @param string $id The user ID
	 * @param string $hash The user password recovery hash
	 * @return void
	 */
	public function reset($id = null, $hash = null)
	{
		if (!empty($this->data)) {
			$this->User->id = $this->data['User']['id'];
			if ($this->User->getRecoveryHash() == $this->data['User']['hash']) {

				// Check the new passwords match
				if (empty($this->data['User']['new_password']) || $this->data['User']['new_password'] !== $this->data['User']['new_password_confirm']) {
					$this->Session->setFlash(__('Your new password did not match the confirmation', true));
					$this->set('hash', $this->data['User']['hash']);
					return;
				}

				$this->data['User']['password'] = $this->AuthCert->password($this->data['User']['new_password']);

				if ($this->User->save($this->data)) {
					$this->Session->setFlash(__('Your password has been updated. You can now log in.', true));
					$this->redirect(array('action' => 'login'));
				} else {
					$this->Session->setFlash(__('Your changes could not be saved. Please, try again.', true));
					$this->redirect(array('action' => 'recover'));
				}
			}
		}

		if ($id && $hash) {
			$this->User->id = $id;
			if ($this->User->getRecoveryHash() == $hash) {
				$this->data = $this->User->read(null, $id);
				$this->set('hash', $hash);
				return;
			}
		}

		$this->Session->setFlash(__('Invalid password recovery URL.', true));
		$this->redirect(array('action' => 'recover'));
	}

	/**
	 * A user can edit his own account information
	 * TODO: Changing passwords
	 *
	 * @return void
	 */
	public function edit()
	{
		$this->User->id = $this->AuthCert->user('id');
		
		if (empty($this->data)) {
			$this->data = $this->User->read();
			return;
		}

		// Check the new passwords match
		if ($this->data['User']['new_password'] || $this->data['User']['new_password_confirm']) {
			if ($this->data['User']['new_password'] !== $this->data['User']['new_password_confirm']) {
				$this->Session->setFlash(__('Your new password did not match the confirmation', true));
				return;
			}

			$this->data['User']['password'] = $this->AuthCert->password($this->data['User']['new_password']);
		}

		// When the e-mail address changes, make sure it doesn't exist yet
		if ($this->data['User']['email_address'] !== $this->AuthCert->user('email_address')) {
			if ($this->User->find('first', array('conditions' => array('User.email_address' => $this->data['User']['email_address'])))) {
				$this->Session->setFlash(__('An different account already exists with that e-mail address.', true));
				return;
			}
		}

		// Save the User
		if ($this->User->save($this->data)) {
			$this->Session->setFlash(__('Your changes has been saved', true));
			$this->redirect(array('action' => 'view'));
		} else {
			$this->Session->setFlash(__('Your changes could not be saved. Please, try again.', true));
		}
	}

	/**
	 * List all user accounts
	 * @return void
	 */
	public function admin_index()
	{
		$this->paginate = array(
			'contain' => array('Group'),
			'order' => array('User.name' => 'asc'),
		);
		$this->set('users', $this->paginate());
	}

	/**
	 * View all information about a user
	 *
	 * @param string $id The ID of the user
	 * @return void
	 */
	public function admin_view($id = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid User.', true));
			$this->redirect(array('action'=>'index'));
		}

		$this->set(array(
			'isSelf' => false,
			'canAddFactories' => false,
			'user' => $this->User->read(null, $id),
		));

		$this->render('view');
	}

	/**
	 * Edit any user
	 *
	 * @param string $id The ID of the user
	 * @return void
	 */
	public function admin_edit($id = null)
	{
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid User', true));
			$this->redirect(array('action'=>'index'));
		}
		
		$groups = $this->User->Group->find('list');
		$this->set(compact('groups'));

		// Check the new passwords match
		if ($this->data['User']['new_password'] || $this->data['User']['new_password_confirm']) {
			if ($this->data['User']['new_password'] !== $this->data['User']['new_password_confirm']) {
				$this->Session->setFlash(__('The new password did not match the confirmation', true));
				return;
			}

			$this->data['User']['password'] = $this->AuthCert->password($this->data['User']['new_password']);
		}

		// When the e-mail address changes, make sure it doesn't exist yet
		$this->User->id = $id;
		if ($this->data['User']['email_address'] !== $this->User->field('email_address')) {
			if ($this->User->find('first', array('conditions' => array('User.email_address' => $this->data['User']['email_address'])))) {
				$this->Session->setFlash(__('An different account already exists with that e-mail address.', true));
				return;
			}
		}

		if (!empty($this->data)) {
			if ($this->User->save($this->data)) {
				$this->Session->setFlash(__('The User has been saved', true));
				$this->redirect(array('action'=>'view', $id));
			} else {
				$this->Session->setFlash(__('The User could not be saved. Please, try again.', true));
			}
		} else {
			$this->data = $this->User->read(null, $id);
		}
	}

	/**
	 * Delete a user
	 *
	 * @param string $id The ID of the user
	 * @return void
	 */
	public function admin_delete($id = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for User', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->User->del($id)) {
			$this->Session->setFlash(__('User deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

}

?>
